@echo off

md "${job.jobMifHiddenDir}"

rem Logging
echo ##### Preparation ##### >> "${job.jobMifHiddenDir}\MIF.stdout"
echo Created directory "${job.jobMifHiddenDir}" >> "${job.jobMifHiddenDir}\MIF.stdout"
echo: >> "${job.jobMifHiddenDir}\MIF.stdout"

exit /b %ERRORLEVEL%