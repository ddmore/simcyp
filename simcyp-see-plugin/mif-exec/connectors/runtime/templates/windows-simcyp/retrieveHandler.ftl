@echo off

set EXECUTABLE=${job.commandExecutionTarget.customScriptsDirectory}\simcypOutputs2SO.jar
set PARAMETERS=${job.executionFile}
set LOCATION=${job.jobDirectory}

rem Logging
echo ##### Retrieval ##### >> "${job.jobMifHiddenDir}\MIF.stdout"
echo Executable: %EXECUTABLE% >> "${job.jobMifHiddenDir}\MIF.stdout"
echo Parameters: %PARAMETERS% >> "${job.jobMifHiddenDir}\MIF.stdout"
echo Location: %LOCATION% >> "${job.jobMifHiddenDir}\MIF.stdout"

cd "%LOCATION%"

java.exe -jar "%EXECUTABLE%" "%PARAMETERS%" 1>> "${job.jobMifHiddenDir}\MIF.stdout" 2>> "${job.jobMifHiddenDir}\MIF.stderr"

echo: >> "${job.jobMifHiddenDir}\MIF.stdout"