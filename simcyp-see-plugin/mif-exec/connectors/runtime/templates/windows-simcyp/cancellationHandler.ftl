@echo off

set CONTROL_FILE=${job.jobDirectory}\simcyp_control.txt

rem Logging
echo ##### Cancellation ##### >> "${job.jobMifHiddenDir}\MIF.stdout"
echo Writing Simcyp cancellation message to %CONTROL_FILE% >> "${job.jobMifHiddenDir}\MIF.stdout

rem Write a cancel message to the signal file to be picked up by the Simcyp Console
echo CANCEL > "%CONTROL_FILE%"

echo: >> "${job.jobMifHiddenDir}\MIF.stdout"

exit /b %ERRORLEVEL%