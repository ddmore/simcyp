@echo off

set EXECUTABLE=${job.commandExecutionTarget.toolExecutablePath}
set PARAMETERS=${job.executionRequest.executionParameters}
set LOCATION=${job.jobDirectory}

rem Logging
echo ##### Processing ##### >> "${job.jobMifHiddenDir}\MIF.stdout"
echo Executable: %EXECUTABLE% >> "${job.jobMifHiddenDir}\MIF.stdout"
echo Parameters: %PARAMETERS% >> "${job.jobMifHiddenDir}\MIF.stdout"
echo Location: %LOCATION% >> "${job.jobMifHiddenDir}\MIF.stdout"

rem Log the start time
for /f "tokens=2 delims==" %%G in ('wmic os get localdatetime /value') do ( set DATETIME=%%G )
echo START=%DATETIME:~0,4%-%DATETIME:~4,2%-%DATETIME:~6,2%T%DATETIME:~8,2%:%DATETIME:~10,2%:%DATETIME:~12,2%Z > "${job.jobDirectory}\duration.tmp"

cd "%LOCATION%"

call "%EXECUTABLE%" %PARAMETERS% 1>> "${job.jobMifHiddenDir}\MIF.stdout" 2>> "${job.jobMifHiddenDir}\MIF.stderr"

rem Log the end time
for /f "tokens=2 delims==" %%G in ('wmic os get localdatetime /value') do ( set DATETIME=%%G )
echo END=%DATETIME:~0,4%-%DATETIME:~4,2%-%DATETIME:~6,2%T%DATETIME:~8,2%:%DATETIME:~10,2%:%DATETIME:~12,2%Z >> "${job.jobDirectory}\duration.tmp"

echo: >> "${job.jobMifHiddenDir}\MIF.stdout"

exit /b %ERRORLEVEL%
