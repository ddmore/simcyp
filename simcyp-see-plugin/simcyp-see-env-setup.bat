@echo off
Setlocal EnableDelayedExpansion

set SIMCYP_EXECUTABLE="%PROGRAMFILES%\Simcyp\Simcyp Simulator V15\Console\simcypc.exe"

if exist !SIMCYP_EXECUTABLE! (
    echo Simcyp Console executable found at !SIMCYP_EXECUTABLE!
) else (
	echo Simcyp Console executable not found at !SIMCYP_EXECUTABLE!
	
	set SIMCYP_EXECUTABLE="%PROGRAMFILES(x86)%\Simcyp\Simcyp Simulator V15\Console\simcypc.exe"
	
	if exist !SIMCYP_EXECUTABLE! (
		echo Simcyp Console executable found at !SIMCYP_EXECUTABLE!
	) else (
		echo Simcyp Console executable not found at !SIMCYP_EXECUTABLE!
		echo Please ensure Simcyp V15 is installed and / or correct the path to the executable in simcyp-see-env-setup.bat
	)
)

set MIF_CONNECTORS_ENV_PARAMS=%MIF_CONNECTORS_ENV_PARAMS% -Dwindows-simcyp.executable=!SIMCYP_EXECUTABLE!