package com.simcyp.simcyp2PharmMLSO;

import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestCsvMessageConverter 
{
	
	String[] testInputs = new String[] {"Info, Title1, Simple Message , 3, 6",
			
			                            "Warn, Title2,  \"Content may have \n new line, comma and \"\"quotes\"\" \", 4, 1 ",
			
			                            "Info, Title1, Simple Message , 3, 6, \n Warn, Title2,  \"Content may have \n new line, comma and \"\"quotes\"\" \", 4, 1 "}; 


    Vector< Vector< Vector<String> > > answers = new Vector <Vector <Vector<String> > >(); 
    
    @BeforeClass
	public static void setUpBeforeClass() throws Exception 
    {
	
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{	
		
	}

	@Before
	public void setUp() throws Exception 
	{
		
		for(int i = 0; i < 3; i++)
		{
			answers.addElement(new Vector<Vector<String> >());
		}
		Vector<String> line = new Vector<String>();
		line.add("Info");
		line.add("Title1");		
		line.add("Simple Message ");
		line.add("3");
		line.add("6");
		answers.elementAt(0).add(line);
		
		Vector<String> anotherLine = new Vector<String>();
		anotherLine.add("Warn");
		anotherLine.add("Title2");		
		anotherLine.add("Content may have \n new line, comma and \"quotes\" ");
		anotherLine.add("4");
		anotherLine.add("1 ");	
		answers.elementAt(1).add(anotherLine);	
		
		answers.elementAt(2).add(line);
		answers.elementAt(2).add(anotherLine);
	}

	@After
	public void tearDown() throws Exception 
	{
	}

	@Test
	public final void test() 
	{
		
		
		for(int i = 0; i < answers.size(); i++)
		{
			Vector<Vector<String> > actual = CsvMessageConverter.parseCsv(testInputs[i]);
			
			assertEquals(actual.size(), answers.elementAt(i).size() );
			
			for(int j = 0; j < answers.elementAt(i).size(); j++)				
			{
				assertEquals(actual.elementAt(j).size(), answers.elementAt(i).elementAt(j).size() );
				
				for(int k = 0; k < answers.elementAt(i).elementAt(j).size(); k++)
				{
				   //assertTrue(actual.elementAt(j).elementAt(k).equals(answers.elementAt(i).elementAt(j).elementAt(k))); 
				   assertEquals(answers.elementAt(i).elementAt(j).elementAt(k), actual.elementAt(j).elementAt(k));
				}
			}
		}
	}

}
