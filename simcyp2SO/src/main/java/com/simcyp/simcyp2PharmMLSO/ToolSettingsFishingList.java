package com.simcyp.simcyp2PharmMLSO;

/**
 * Header fields to fish out from Simcyp csv files for tool settings if represented as marked up SO table(s). 
 * Actually the toolSetttings element itself does not provide a an SO dataset, just an external file reference , so the information here 
 * would not be needed for that. One option is to create a populationParameter table under the simulation element with the fished headers. 
 */
public interface ToolSettingsFishingList {
	

	String[] toolSettingsFishingListTD = {   
	    	"Random Generator",
	    	"Seed",
	    	"Seed Value",
	    	"Solver sampling interval",
	    	"Memory Size",
	    	"Number of time samples (solver output)",
	    	"Differential Solver",
	    	"Maximum number of steps",
	    	"Relative Tolerance",
	    	"Relative Tolerance when ADAM is used",
	    	"Integration error tolerance",
	    	"Paediatric Module",
	    	"No. Differential Equations",
	    	"Study Duration",
	    	
	    	"Simulation Duration",
	    	"Windows Version",
	    	"Simcyp.exe",
	    	"File Version",
	    	"Date Modified",	
	    	"File Size",	
	    	"Login name	",
	    	"Computer name",	
	    	"Licence type",
	    	"Key",	
	    	"Expiry"	    	
	   }; 

}
