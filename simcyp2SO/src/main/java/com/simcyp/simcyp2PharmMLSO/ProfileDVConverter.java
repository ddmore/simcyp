package com.simcyp.simcyp2PharmMLSO;

import java.util.TreeMap;
import java.util.Vector;

/**
 *  This class is the delegate for (now probably optional) things concerned with generating SO simulation profile tables with a single dependent variable (DV)  
 *  row-indexed by a DVID column which identifies different DV variables, from an alternative format often output by Simcyp, where various dependent variables 
 *  with different names are tabulated in columns side by side against the same set of times .
 */
public class ProfileDVConverter
{
	
    /**
     * Structure used to keep track of columns during conversion to DVID format 
     */
    static class TableDVIDInfo
    {    	
	    int     nDVIDCol        = -1;
    	int     nDVCol          = -1;
    	int     nTimeCol        = -1;
    	int     nIndexCol       = -1;
    	boolean rawFileUsesDVID = false;
    	int     nColumnsInput   =  0;  //remember how many fields we started with  
    }    
    
	
	/**
     * For a table where the column header info and values have been already mapped into candidateColumns and candidateValues, but where the dependent variables are 
     * side-by-side, this method converts the mapping to one representing the SO profiles format with dependent variables in a single DV column and a DVID 
     * column representing the identifier of the DV type. The DVID is created here as a string identifier from the column names of each dependent variable 
     * with any internal whitespace replaced by "_". Columns recognized as Population, Trial , Index/ID, Time are not treated as DVs; any others are.
     * @param fNameOnly            filename without extension identifying the original csv file source
     * @param tableInfo            information on table structure 
     * @param indivProfiles        map of filename to headers for individual profiles table
     * @param candidateColumns     map of filename to (map of column-name:column-number)  - is updated by the conversion
     * @param candidateValues      map of filename to (map of column-number:column-values) - is updated by the conversion
     */
    void convertProfilesToDVIDBased(String fNameOnly,
    		                        TreeMap<String, Vector<ColumnHeader> > indivProfiles,
    		                        TableDVIDInfo tableInfo,
    		                        TreeMap<String, TreeMap<String, Integer>> candidateColumns, 
    		                        TreeMap<String, TreeMap<Integer, Vector<String>>> candidateValues)
    {
    	Vector<String> dvids       = new Vector<String>();
    	Vector<String> dvs         = new Vector<String>();
    	Vector<String> times       = new Vector<String>();
    	Vector<String> populations = new Vector<String>();
    	Vector<String> ids         = new Vector<String>();
    	Vector<String> trials      = new Vector<String>();
    	
    	
    	TreeMap<Integer, Vector<String>> values = candidateValues.get(fNameOnly);
    	TreeMap<String,Integer>         colInfo = candidateColumns.get(fNameOnly); 
    	
    	//These values can be repeated for each DVID 
		Vector<String> timeValues = null, idValues=null, trialValues= null, popValues = null;   //check for case issues
	
    	if (values != null)
    	{
    		Integer colNo = colInfo.get("Time");
    		if(colNo != null)
			    timeValues  = values.get(colNo);   
    		colNo = colInfo.get("Index");  
    		if(colNo != null)
			    idValues    = values.get(colNo);
			
			colNo = colInfo.get("Trial");       
	    	if(colNo != null)
			    trialValues = values.get(colNo);
			    
			colNo = colInfo.get("Population");   
			if(colNo != null)
			    popValues   = values.get(colNo);
    	}		
		
    	//do conversions
    	for(ColumnHeader item : indivProfiles.get(fNameOnly))
    	{
    		Integer nameColNo = colInfo.get(item.getName());
    		if(nameColNo != null)
    		{
    			Vector<String> nameColValues  = values.get(nameColNo);
    			
    			if(nameColValues != null)
    			{
      				//fill in the repeating values for this dvid 
    				if(idValues != null)
    					ids.addAll(idValues);
    				if(timeValues != null)
    				    times.addAll(timeValues);
    				if(trialValues != null)
    				    trials.addAll(trialValues);
    				if(popValues != null)
    				    populations.addAll(popValues); 
    				
    				//dvid itself is constant for this block
    				int size = nameColValues.size();
    				String dvid = item.getName().trim().replaceAll("\\s", "_"); //replace any internal whitespace with _, no external whitespace;
    				for(int i = 0; i < size ; i++ )
    				{		
    					dvids.add(dvid);
    				}  
    				
    				//dvs are values from this column 
    				dvs.addAll(nameColValues);
    				
    				//don't want these columns any more 
    				colInfo.remove(item.getName());
    				values.remove(nameColNo);
    			}
    		}    		
    	}
    	
        //write back the new values onto the candidate maps 
    	
     	TreeMap<Integer, Vector<String>>  newValues  = new TreeMap<Integer, Vector<String>>();
     	TreeMap<String, Integer>          newColInfo = new TreeMap<String, Integer>(); 
 	    
     	newColInfo.put("ID", newColInfo.size() + 1);
 	    newValues.put(newColInfo.size(), ids);    	
 	    newColInfo.put("DVID", newColInfo.size() + 1);
 	    newValues.put(newColInfo.size(), dvids);
 	    newColInfo.put("DV",  newColInfo.size() + 1);
 	    newValues.put(newColInfo.size(), dvs);    
 	    newColInfo.put("Time", newColInfo.size() + 1);
 	    newValues.put(newColInfo.size(), times);
 	    newColInfo.put("Population",  newColInfo.size() + 1);
 	    newValues.put(newColInfo.size(), populations); 
 	    newColInfo.put("Trial",  newColInfo.size() + 1);
 	    newValues.put(newColInfo.size(), trials); 
 	    
 	    tableInfo.nIndexCol   = 1;
 	    tableInfo.nDVIDCol    = 2;
 	    tableInfo.nDVCol      = 3;
 	    tableInfo.nTimeCol    = 4;
 	    
 	    candidateColumns.put(fNameOnly, newColInfo);
 	    candidateValues.put(fNameOnly, newValues);
 	    
     }

}
