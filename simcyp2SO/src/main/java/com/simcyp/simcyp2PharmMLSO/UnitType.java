package com.simcyp.simcyp2PharmMLSO;

import eu.ddmore.libpharmml.dom.commontypes.SymbolType;

/**
 * Represents units and data types for a variable (typically for a table column header)  
 * @author Duncan Edwards
 *
 */
public class UnitType 
{
	
	//static final String[] realUnits = {"s", "h","w", "y", "1/h","mg", "mg/L", "Years",	"kg","cm","m2","g",	"kg/m2","L/h","g/L","umol/L","mL/min", "%","L", "L/kg",
	//		"million cells / g liver", "mg protein / g liver", "mg protein whole gut SI","mg protein whole gut SI","mg protein whole gut SI",
	//		"millions of  cells/g kidney","mg protein / g kidney","mg protein / g kidney","mg protein / g kidney", "s", "byte", "Mb",
	//		"mL"};
	
    //static Set<String> realUnitsSet = new TreeSet<String>(Arrays.asList(realUnits)); 

	/**
	 * 
	 */
    public static String [] unitNames ={ "",     "",       "mg",        "mg/kg",          "mg/m2",       "kg",       "m2",  "...", ""};
    
    public enum UnitID                {UNSET,   BLANK,     UNIT_MG,     UNIT_MG_PER_KG,   UNIT_MG_PER_M2, UNIT_KG, UNIT_M2, OTHER, CUSTOM_STRING};
    
    public enum TypeID                {UNSET, REAL, INTEGER, BOOLEAN, STRING, ID}  //ID  added as special case to match PharmML SymbolTypeType 
    
        
    private UnitID unit = UnitID.BLANK;
    
    private TypeID type = TypeID.UNSET;

    private String customUnitName;

	
	public UnitType(UnitID unit)
	{
		this.unit = unit;		
	}
	
    public UnitType(String customUnitName)
    {
    	if(customUnitName != null)
    	{
    	    unit = UnitID.CUSTOM_STRING;    	
    	    this.customUnitName = customUnitName;
    	}
    } 	
	
	public UnitType(UnitID unit, TypeID type)
	{
		this.unit = unit;
		this.type = type;
	}

	public UnitID getUnit()
	{
		return unit;
	}
	
	/**
	 * Gets the name of the unit as a string: empty string if not set
	 */
	public String getUnitName()
	{
		if(unit == UnitID.CUSTOM_STRING && customUnitName != null)
			return customUnitName;
		return unitNames[unit.ordinal()];
	}
	
	/**
	 * Gets the datatype as enumerated in TypeID, including the possibility of UNSET
	 * @return type value
	 */
	public TypeID getType()
	{
		return type;
	}
	
	/**
	 * Gets the datatype as a PharmML SymbolType with UNSET defaulting to Symboltype.STRING 
	 * @return eu.ddmore.libpharmml.dom.commontypes.SymbolType value 
	 */
	public SymbolType getSymbolType()
	{
		switch (type)
		{
			case REAL:    return SymbolType.REAL;
			case INTEGER: return SymbolType.INT;
			case BOOLEAN: return SymbolType.BOOLEAN;
			case STRING:  return SymbolType.STRING;
			case ID:      return SymbolType.ID;
			default:      return SymbolType.STRING;
		}
	}
	
	public void setType(TypeID type)
	{
		this.type = type;
	}
	
	/**
	 * Sets the custom unit to the supplied 
	 * @param unit type as enumerated in UnitID
	 */
	public void setUnit(UnitID unit)
	{
		this.unit = unit;
		if(unit  != UnitID.CUSTOM_STRING)
		    this.customUnitName = null;
	}
	
	/**
	 * Sets the custom unit to custom string with the supplied name string
	 * @param customName
	 */
	public void setCustomUnit(String customName)
	{
		this.unit = UnitID.CUSTOM_STRING;
		if(customName != null)
	        customUnitName = customName;
	}    
	
}
