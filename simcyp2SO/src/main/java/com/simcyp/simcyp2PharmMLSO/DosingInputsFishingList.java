package com.simcyp.simcyp2PharmMLSO;


/**
 * Header fields to fish out from Simcyp csv files for doing inputs conversion
 * @author Duncan Edwards
 *
 */
public interface DosingInputsFishingList {
	
	String[] dosingFishingListTD = {   	    	
	        "Fasted/Fed",
			//"Simulation Duration",
            "Fluid intake with dose",	
    		"Fluid intake with dose CV (%)",
    		"Start Day/Time",	
    		"End Day/Time",	
    		//"Study Duration",
    		
    		//WWERE IS THE INDIV VALUE?
    		//Fluid intake with dose,
            ///Fluid intake with dose CV (%) 
    };  
	
	//This list is expected to be found as whitespace-removed end-label following character ":"; eg look for in "Sub : Route" 
	String[] dosingFishingListTD2 = {
    		"Route",	                     //eg for Sub:Route  Inh 1:Route
    		"Dose",	
    		"Dosing", //Regimen	
    		"Dose Interval",	
    		"NoDoses"
    }; 
	
	String[] dosingFishingListCmpd = { 
 	    	"Input Compartment",
	    	"Input Model",
	    	"Input Duration",
 	    	"Input Compartment",  
 	    	"Interpolation Method",
 	    	"Lag time",      //mean and CV values eventually to rbe replaced by individual values
 	    	"Lag time CV (%)",  
 	    	"fa",  
 	    	"fa CV (%)",
 	    	"ka",  
 	    	"ka CV (%)"
 	};

	
    //Custom dosing - can get the whole table   
    /*
    Dose Number	            
    Day
    Time                             //(hh:mm)
    Offset                           //(h)
    Dose
    Dose Unit
    Route of Administration
    Duration
    */
}
