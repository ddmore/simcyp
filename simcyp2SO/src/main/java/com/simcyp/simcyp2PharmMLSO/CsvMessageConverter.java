package com.simcyp.simcyp2PharmMLSO;

import java.util.Vector;


/**
 * Support for converting standard messages expected to be output by Simcyp console in file Messages.tmp to PharmMLSO message content.
 * Messages output by Simcyp are expected to be on a single line, mostly with simple fields. However the content field may include 
 * commas, newlines or quotes and so needs careful handling..
 * @author Duncan Edwards
 */
public class CsvMessageConverter 
{
	
	
	/**
	 * Takes input string and creates lines of strings with each parsed element. Takes account of standard which requires 
	 * csv elements to be in double quotes if containing (CRLF) (currently assuming \n on Windows) , comma or ", 
	 * and for enclosed " to be doubled as "". Any whitespace after last comma on a line is dropped. 
	 * Currently this implementation drops leading whitespace but only drops trailing after end of final enclosing double quote.  
	 * @param inString
	 */
	public static Vector< Vector <String> > parseCsv(String inString)
	{
		
		boolean bInQuotes      = false;
		boolean bTokenIsQuoted = false;
		boolean bTokenComplete = false;
		boolean bLineComplete  = false;
		boolean bLeading       = true;
		//boolean bTrailing      = false; 
		
		Vector< Vector<String> > lines = new Vector< Vector <String> > ();
		Vector<String> currentLine = new Vector<String>();
		
		int nPosition = 0;
		int length = inString.length();
		StringBuffer token = new StringBuffer();
		
		while (nPosition < inString.length())
		{
			char c = inString.charAt(nPosition);
			
			if(!bInQuotes)
			{
				switch(c)
				{
				    case '"': 
				    	if(!bTokenIsQuoted && bLeading) 
				    	{
				    		bInQuotes = true;
				    		bTokenIsQuoted = true;
				    		bLeading = false;				    		
				    	}
				    	else if (bTokenIsQuoted)
				    	{
				    		assert(false); //shouldn't encounter " when bInQuotes false and bTokenIsQuoted 
				    		token.append(c);				    		
				    	}
				    	else //bLeading				    	
				    	{	 
				    		System.err.println("csv parse error: encountered \" within unquoted string");
				    		token.append(c);
				    	}				    	
				    	break;
				    	
				    case '\n': 
				    	bLineComplete = true;
			    		//fall through

				    case ',':
				    	bTokenComplete = true;				    	
				        break;
				        
				    default:
				    {
				    	if(!(Character.isWhitespace(c) && bLeading))
				    	{
				    	    token.append(c);
				    	    if(bLeading)
				    	    	bLeading = false;
				    	}
				    }
				}
				
			}
			else  //bInquotes
			{
				switch(c)
				{
				    case '"': 
				    	if(nPosition + 1 < length) 
				    	{
				    	    if(inString.charAt(nPosition+1) == '"') // case of internal " doubled as  "", just use one of them
				    	    {
				    	    	nPosition++;
				    	    	token.append('"');
				    	   	}
				    	    else
				    	    {
					    		bInQuotes = false; // should be final quote
					    		//bTrailing = true;
				    	    }
				    	}
				    	else                                       
				    	{
				    		bInQuotes = false; // can get here if final quote is at end of inString                             
				    	} 
				    	break;
				    	
				    default:
				    {
				    	token.append(c);
				    }				
				}
				
			}
			
			if(nPosition >= length -1 )
			{
				bTokenComplete = true;
				bLineComplete  = true;
			}
			if(bTokenComplete)
			{

				if(bLineComplete)  //need to avoid outputting whitespace token after last comma on line 
				{		
					boolean isAllWhiteSpace = true;  
					for(int i = 0; i < token.length(); i++)
					{
						if (!Character.isWhitespace(token.charAt(i)))
							isAllWhiteSpace = false;
					}
					
					if(!isAllWhiteSpace)
						currentLine.add(token.toString());
				}
				else
					currentLine.add(token.toString());
				if(nPosition < length - 1)
				{
				    token = new StringBuffer();
				    bTokenIsQuoted = false;
				    bInQuotes = false;
				    bTokenComplete = false;
				    bLeading = true;
				}
			}			
			if(bLineComplete)
			{
				lines.add(currentLine);
				if(nPosition < length -2)
				{
					currentLine = new Vector<String>();
					bLineComplete = false;
					bTokenComplete = false;
				}
			}	
			
			nPosition++;
		}

		return lines;
	}
	

}
