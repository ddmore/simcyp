/**
 * 
 */
package com.simcyp.simcyp2PharmMLSO;

import com.simcyp.simcyp2PharmMLSO.UnitType;
import eu.ddmore.libpharmml.dom.commontypes.SymbolType;

/**
 * Represents table column header information (such as name, data-type and units) 
 * @author Duncan Edwards
 */
public class ColumnHeader 
{	
	private String    name;
	
	private UnitType  unitTypeInfo;

	/**
	 * Constructs a column header with supplied column name
	 * @param name
	 */
	public ColumnHeader(String name)
	{
		this.name = name;
	}

	/**
	 * Indicates whether this header has a non-null UnitTypeInfo object  
	 */
	public boolean hasUnitTypeInfo()
	{
		return unitTypeInfo != null;
	}
	
	/**
	 * Sets unit to the supplied type 
	 * @param unit
	 */
	public void setUnit(UnitType.UnitID unit)
	{
		if(unitTypeInfo == null)
			unitTypeInfo = new UnitType(unit);
		else
			unitTypeInfo.setUnit(unit);		
	}
	
	/**
	 * Sets unit to a custom string 
	 * @param customNameString
	 */
	public void setCustomUnit(String customNameString)
	{
		if(unitTypeInfo == null)
			unitTypeInfo = new UnitType(customNameString);
		else
			unitTypeInfo.setCustomUnit(customNameString);	
	}
	
	/**
	 * Sets datatype to the supplied type
	 * @param type
	 */
	public void setDataType(UnitType.TypeID type)
	{
		if(unitTypeInfo == null)
			unitTypeInfo = new UnitType(UnitType.UnitID.UNSET, type);
		else
			unitTypeInfo.setType(type);			
	}
	
	/**
	 * Gets the header column name 
	 */
	public String getName()
	{
		return name;
	}
	
	public UnitType.UnitID  getUnit()
	{
		if (unitTypeInfo != null) 
			return unitTypeInfo.getUnit(); 
		else 
			return UnitType.UnitID.UNSET;
	}
	
	/**
	 * Gets the name of the units
	 */
	public String  getUnitName()
	{
		if (unitTypeInfo != null) 
			return unitTypeInfo.getUnitName(); 
		else return "";
	}
	

	/**
	 * Gets the datatype for the column  
	 */
	public UnitType.TypeID getDataType()
	{
		if (unitTypeInfo != null) 
			return unitTypeInfo.getType(); 
		else 
			return UnitType.TypeID.UNSET;
	}

	/**
	 * Gets the datatype for the column as a PharmML SymbolType with UNSET defaulting to Symboltype.STRING 
	 * @return eu.ddmore.libpharmml.dom.commontypes.SymbolType value 
	 */
	public SymbolType getSymbolType()
	{
		if (unitTypeInfo != null) 
			return unitTypeInfo.getSymbolType(); 
		else 
		   return SymbolType.STRING;
	}
	
}
