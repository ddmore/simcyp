package com.simcyp.simcyp2PharmMLSO;

/**
 * Repository of reusable constants and associate enumeration values, particularly for constants used in Simcyp and/or which possibly feature for the SO converter 
 * as keywords for special treatment  
 * @author dedwards
 */
public interface SimcypConstants{
	
	String[] strCompoundShortNames       = {"Sub", "Inh 1", "Inh 2", "Inh 3", "Sub Pri Met1", "Inh 1 Met", "Sub Pri Met2", "Sub Sec Met", "Inh 1 Met"};
	String[] strCompoundReallyShortNames = {"Sub", "Inh 1", "Inh 2", "Inh 3", "Sub PM1",      "Inh 1 M",   "Sub PM2",       "Sub SM",     "Inh 1 M"};
	
	/**
	 * Corresponds to compound roles in a simulation parent/primary-metabolite/secondary-metabolite, substrate/inhibitor
	 */
	enum Compound                           {SUB,   INH1,    INH2,  INH3,  SUB_PM1,  INH1_M,  SUBP_PM2, SUB_SM}
	
	/**
	 * Compound values beyond this are metabolites
	 */
	int LAST_INDEX_DOSED_CMPD = Compound.INH3.ordinal();
	

	String[] strDosingRegimen = {"Single Dose", "Multiple Dose"};
	
	/**
	 * May not be required as number of doses can be used instead  
	 */
	enum DosingRegimen           {SINGLE_DOSE,   MULTIPLE_DOSE, UNSET};
	                                 //private int value; private DosingRegimen(int value) {this.value = value;};  public int value(){return value;}}               
	/**
	 * Corresponds to dose individualisation scaling options based on body-size or weight.   
	 */
	enum DoseScalingType {UNSCALED, PER_KG_BWT, PER_M2_BSA, UNKNOWN};		                     


	String[] strNonValues = {"n/a", "NaN", "null"};
	
	/**
	 * Various ways a value may be declared as a non-value or not a number or not applicable in Simcyp outputs.
	 */	
	enum NonValues          {NA, NAN, NULL};
	
	/**
	 * This puts together various admin routes and sites used by Simcyp so that the ordinal provides an enum which can distinguish them via a distinct  
	 * numeric pseudo-ADM data code in standard output dosing table
	 */
	enum InputSiteRoute {UNSET, ORAL, IV_BOLUS, IV_INFUSION, INHALED, 
		                 OTHER_SITE_GENERIC, VENOUS_BLOOD, CSF, SPINAL_CSF, STOMACH, COLON, ADDITIONAL_ORGAN, SUBCUTANEOUS, INTRAMUSCULAR, CUSTOM,
                         DERMAL_GENERIC, DERMAL_FOREARM, DERMAL_UPPER_ARM, DERMAL_LOWER_LEG, DERMAL_THIGH, DERMAL_FACE};
      
	/**
	 * Input/site names expected from a Simcyp Inputs_<Cmpd> file  "Input Compartment";  includes expected values from CustomDosing<Cmpd> file  "Route of Administration" (case-insensitively , "Oral","oral")   
	 */
    String[]     strInputRoutes = {"Venous Blood","CSF","Spinal CSF","Stomach", 
    		                        "Colon", "Additional organ","Subcutaneous", 
	                                "Oral", "iv", "Infusion", "Inhaled","Dermal"};	
    /**
     * Numerical identifiers for strInputRoutes  
     */
	InputSiteRoute[] inputRoutes = {InputSiteRoute.VENOUS_BLOOD, InputSiteRoute.CSF, InputSiteRoute.SPINAL_CSF, InputSiteRoute.STOMACH ,
                                    InputSiteRoute.COLON, InputSiteRoute.ADDITIONAL_ORGAN, InputSiteRoute.SUBCUTANEOUS, 
                                    InputSiteRoute.ORAL, InputSiteRoute.IV_BOLUS, InputSiteRoute.IV_INFUSION, InputSiteRoute.INHALED, InputSiteRoute.DERMAL_GENERIC};
	
	
	/**
	 * Input/site names expected from a Simcyp Inputs_TrialDesign file "<Cmpd> : Route",
	 */
    String[]  strInputRoutes2     = {"Oral", "i.v. bolus", "i.v. infusion", "Dermal", 
    		                         "Inhaled","Custom", "Subcutaneous", "Intramuscular", "Other Site"};   
    
    /**
     * Numerical identifier corresponding to strInputRoutes2
     */
    InputSiteRoute[] inputRoutes2 =  {InputSiteRoute.ORAL,    InputSiteRoute.IV_BOLUS, InputSiteRoute.IV_INFUSION,  InputSiteRoute.DERMAL_GENERIC, 
    		                          InputSiteRoute.INHALED, InputSiteRoute.CUSTOM,   InputSiteRoute.SUBCUTANEOUS, InputSiteRoute.INTRAMUSCULAR, InputSiteRoute.OTHER_SITE_GENERIC};
   

	String[]      strSkinLocations  = {"Forearm", "Upper Arm ", "Lower Leg", "Thigh", "Face"};
	
	/**
	 *  Dermal route sublocation specifications.Can potentially be used to make finer compartment separation in SO ADM code, but not yet in use.      
	 */	
	InputSiteRoute[] skinLocations  = {InputSiteRoute.DERMAL_FOREARM, InputSiteRoute.DERMAL_UPPER_ARM, InputSiteRoute.DERMAL_LOWER_LEG, InputSiteRoute.DERMAL_THIGH, InputSiteRoute.DERMAL_FACE};
		

    /**
     * Input models expected for Simcyp "Other Site" dosing  in Inputs_<Cmpd> file
     */
	String[] strInputModels = {"Bolus", "First Order", "Zero Order", "User Profile"};
	
	/**
	 * Input models as specifically for Simcyp Other-Site Dosing outputs in   
	 */
	enum InputModel {BOLUS,  FIRST_ORDER, ZERO_ORDER, USER_PROFILE}; 
	
	//String[] strDemographicKeywords = {"Weight", "BSA"};
    //enum demographicKeywordIds        {WEIGHT,    BSA};

	String[] strTissueNames = {"Adipose", "Bone", "Brain","Gut", "Heart", "Kidney", "Liver", "Lung", "Muscle", "Skin", "Spleen", "Plasma", "RBC", "AddOrgan", "BrainCSF", "FetoPlacenta", "Pancreas"};
	
	enum Tissues              {ADIPOSE,    BONE,   BRAIN,  GUT,   HEART,   KIDNEY,   LIVER,   LUNG,   MUSCLE,   SKIN,   SPLEEN,   PLASMA,   RBC,   ADDITIONAL_ORGAN,   BRAIN_CSF,  FETOPLACENTA,  PANCREAS};
		
	String[] strGISegmentNames = {"Stomach", "Duodenum", "JejunumI", "JejunumII", "IleumI", "IleumII", "IleumIII", "IleumIV", "Colon", "Faeces"};

	enum GISegment                {STOMACH,   DUODENUM,    JEJUNUM_I, JEJUNUM_II,  ILEUM_I, ILEUM_II, ILEUM_III, ILEUM_IV, COLON, FAECES};
	




}

