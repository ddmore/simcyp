package com.simcyp.simcyp2PharmMLSO;

import com.simcyp.simcyp2PharmMLSO.ProfileDVConverter.TableDVIDInfo;
import com.simcyp.simcyp2PharmMLSO.TableXReferencer.TableType;
import com.simcyp.simcyp2PharmMLSO.TableXReferencer.CsvTables;
import com.simcyp.simcyp2PharmMLSO.TableXReferencer.TableTypeCheckMode;

import eu.ddmore.libpharmml.dom.commontypes.AnnotationType;
import eu.ddmore.libpharmml.dom.commontypes.IntValue;
import eu.ddmore.libpharmml.dom.commontypes.RealValue;
import eu.ddmore.libpharmml.dom.commontypes.Rhs;
import eu.ddmore.libpharmml.dom.commontypes.StringValue;
import eu.ddmore.libpharmml.dom.commontypes.SymbolType;
//import eu.ddmore.libpharmml.dom.dataset.DatasetRow;
import eu.ddmore.libpharmml.dom.dataset.HeaderColumnsDefinition;
import eu.ddmore.libpharmml.dom.dataset.ObjectFactory;
import eu.ddmore.libpharmml.dom.dataset.ColumnDefinition;
import eu.ddmore.libpharmml.dom.dataset.ColumnType;
import eu.ddmore.libpharmml.dom.dataset.ExternalFile;
import eu.ddmore.libpharmml.impl.PharmMLVersion;
import eu.ddmore.libpharmml.so.*;
import eu.ddmore.libpharmml.so.impl.*;
import eu.ddmore.libpharmml.so.dom.*;
//import eu.ddmore.libpharmml.util.WrappedList;
import de.unirostock.sems.cbarchive.ArchiveEntry;
import de.unirostock.sems.cbarchive.CombineArchive;
import de.unirostock.sems.cbarchive.CombineArchiveException;

import java.net.URI;
import java.net.URISyntaxException;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import javax.xml.bind.JAXBElement;
import javax.xml.transform.TransformerException;

import org.jdom2.JDOMException;
import org.junit.Assert;




/** 
 * Converts Simcyp csv outputs (together with Lua scripts and messages) to DDMoRe standardized outputs.  
 *                          
 * @version     0.3 (prototype) 
 * @author      Duncan Edwards
 */
public class ToPharmMLSO 
{
    /**
     * When false, profiles with multiple dependent variable (DV)  columns against a single time column are left as is. When true, the DVs are 
     * put in a single column time and a DVID created from the DB column names.
     */
    public boolean bMakeProfilesWithDVID = false;
    
   
    /**
     * Helper for cross referencing table types
     */
    TableXReferencer       xReft;
    
    /**
     * An inferred table type. The PharmMLSO generator aims to process many Simcyp output csv files into appropriate tables. In order to streamline the process it tries to work 
     * out what sort of table it is seeing and apply rules for that type. Actually the table type can also be hard coded, and the converter can use such hard coding to check what it infers , or other options. 
     * For explicit table types expected based on csv filename, see the TableXReferencer
     */
    TreeMap<String, TableType> tTypeInferred;

    //This block of maps will typically be used to store headers for setting up external file ref column definitions for a particular type of SO table 
    //More specific collections of headers with cross-referenced values for detailed markup are handled by the "candidate..." maps. 
    //Note that not all maps are continually held full. The keying on filename
    //allows for certain cross-file references to be held where necessary, but most map branches are  currently cleaned down after a each file is processed.  
    
    /**
     *  Generic map of csv filename (without extension) to a collection of headers destined for an SO table currently being prepared 
     *  (used for individual and population parameters and profiles individual parameters table).  
     */
    TreeMap<String, Vector<ColumnHeader> > headerMap   = new TreeMap<String, Vector<ColumnHeader> >(String.CASE_INSENSITIVE_ORDER);
    
    /**
     *  Map of csv filename (without extension) to a collection of headers destined for an SO dosing table. 
     *  Originally was used to treat dosing table like other external file tables: usage may be reassiged as dosing table now 
     *  has specific handling for NONMEM-like format conversion  
     */
    TreeMap<String, Vector<ColumnHeader> > dosingInfo        = new TreeMap<String, Vector<ColumnHeader> >(String.CASE_INSENSITIVE_ORDER);
    
    /**
     *  Map of csv filename (without extension) to a collection of headers destined for an SO tool settsings table.
     */
    TreeMap<String, Vector<ColumnHeader> > toolSettings      = new TreeMap<String, Vector<ColumnHeader> >(String.CASE_INSENSITIVE_ORDER);
    
    /* 
     * Note Candidate columns have not as yet been migrated to use the columnHeader structure for historical reasons - a separate units map was already in use 
     */
  
    /**
     * Candidate column information or markup mapped by table-filename (without csv extension).For each filename there is a map of column name to column number 
     */
    TreeMap<String, TreeMap<String, Integer>> candidateColumns = new TreeMap<String, TreeMap<String, Integer>>(String.CASE_INSENSITIVE_ORDER);    
    
    /**
     * Candidate data values in a column mapped by table-filename: for each filename there is a map of column number to a vector of column values in row order
     */
    TreeMap<String, TreeMap<Integer, Vector<String>>> candidateValues = new TreeMap<String, TreeMap<Integer, Vector<String>>>(String.CASE_INSENSITIVE_ORDER); 
    

    /**
     * Candidate data values in a column mapped by table-filename: for each filename there is a map of column name to Units
     */
    TreeMap<String, TreeMap<Integer, UnitType> > candidateUnits = new TreeMap<String, TreeMap<Integer, UnitType>>(String.CASE_INSENSITIVE_ORDER); 

    
    /**
     * Maps to the JAXB external file elements for csv tables from the file name INCLUDING extension 
     */
    TreeMap<String, JAXBElement<ExternalFile> >  mapExternalTableFiles = new TreeMap<String, JAXBElement<ExternalFile> >(String.CASE_INSENSITIVE_ORDER);
   
    
    /**
     * Maps to the JAXB external file element for lua script files from the file name INCLUDING extension 
     */
    TreeMap<String, JAXBElement<ExternalFile> >  mapExternalScriptFiles = new TreeMap<String, JAXBElement<ExternalFile> >(String.CASE_INSENSITIVE_ORDER);
    
    
    /**
     * Name of subfolder for data in Combine archive 
     */
    final String DATA_FOLDER_NAME    = "data";
    
    /**
     * Name of subfolder for Lua scripts in Combine archive 
     */
    final String SCRIPT_FOLDER_NAME  = "scripts";
    
    /**
     * URI used to identify the Lua script content type for Combine Archive
     */
    final String LUA_URI             = "http://purl.org/NET/mediatypes.application/x.lua"; //provisional invalid persistent URL pending something better
    
    /**
     * URI used to identify PharmML-Standard Output content type 
     */
    final String SO_URI              = "http://www.ddmore.eu/pharmmlso";

    /**
     * File extension used for Combine Archive 
     */ 
    final String OMEX_EXTENSION      = ".omex";  //might otherwise be .phmex for SO?
    
    
    private LibSO libSO;
    
    private StandardisedOutputResource soResource;
    	    
    private boolean bDosingTemplateCreated = false;
    
    
    /**
     * Entry point for SO Converter.  Converter should be run in the directory where the Simcyp csv files are to be found. 
     * @param args - Currently command line arguments are ignored.
     */
    public static void main(String args[])
    {
    	int completionCode = 0;
        System.out.println("Simcyp to SO conversion");

        ToPharmMLSO converter = new ToPharmMLSO();

        converter.createSOResource();
        
        Path dir = Paths.get(".");   //csv files to be read from here      
        
        completionCode = converter.doSO(dir);
        
        //converter.tryMoreSOStuff();
        
        String outSOName      = "so_out.xml";
        String outCombineName = "SimcypSimOutput.omex";
        
        completionCode += converter.saveSO(outSOName);      
        
        completionCode += converter.createArchive(outCombineName, outSOName);
        
        System.exit(completionCode);
    }
    
    

    /**
     * Creates the SO source and it's DOM as indicated in libPharmMLSO.  
     * "SO resource is the object that contains the DOM. One must specify the version of the SO specification while creating the resource. 
     * This version corresponds to the version of the schema that will be used for validate the content of the DOM."
     */
    public void createSOResource()
    {
        SOFactory soFactory = SOFactory.getInstance();
        libSO = soFactory.createLibSO();

        soResource = libSO.createDom(SOVersion.DEFAULT);        
    }
    
    /**
     * Saves the created PharmMLSO serialized to xml in the named file. 
     * @param name
     */
    public int saveSO(String name)
    {    
        try
        {
            libSO.save(new FileOutputStream(name), soResource);
            return 0;
        }
        catch(FileNotFoundException e)
        {
            System.err.println(e);
            return 100;
        }
    }
      
	/**
	 * Sets up an SO block with component elements to be filled by external csv data from Path dir.
	 * Then tries to evaluate and load that data via a call to loadTables.
	 * @param dir
	 */
    public int doSO(Path dir)
    {
    	//Set up the SO block  elements where the Simcyp info will be put
        StandardisedOutput so = soResource.getDom();	        
        SOBlock sob = so.createSoBlock(); 
        AnnotationType anv =  new AnnotationType();
        anv.setValue("Standardized Output based on Simcyp outputs (csv table and Lua script files)");
        sob.setDescription(anv);
        
        RawResults rrr = sob.createRawResults(); 
        rrr.setId("SimcypOutputFiles");
        anv =  new AnnotationType();
        anv.setValue("Currently this ADDITIONAL list shows relative file paths as packed in data and script subfolders within Combine archive."
        		    + "External File elements elsewhere in this document show relative path to the external file relative to this SO file");
        rrr.setDescription(anv);      
        
        Simulation sim = sob.createSimulation();
                
        ToolSettings ts = sob.createToolSettings();
        ts.setId("SimcypSettings");    
        anv  = new AnnotationType();
        String note = "See also markup of relevant columns in external files under Simulation PopulationParameters";
        anv.setValue(note);	
        ts.setDescription(anv);           	
      
         //create TaskInformation element
        TaskInformation ti = sob.createTaskInformation();
        int ret = loadMessagesAndCreateMarkup(dir, ti);   //currently not using this return code
        
        //create table datasets under simulation
        sim.createIndivParameters();
        sim.createPopulationParameters();
        sim.createDosing();
        sim.createSimulatedProfiles();
                
        //ExternalFile rawResFile  = sim.createRawResultsFile(); //currently only one external file directly under <Simulation> allowed by the schema we are cuirrently not using this         
        
        //Now look at and evaluate the external data for import
        
        xReft = new TableXReferencer(TableTypeCheckMode.INFER_WITH_WARNINGS);
        
        int completionCode = loadTablesAndCreateMarkup(dir, sob, xReft, DATA_FOLDER_NAME ); //returns 0-5
       
        int returnCode     = loadScriptsAndCreateMarkup(dir, sob, SCRIPT_FOLDER_NAME);      //returns 0-3	
        
        completionCode += 10 * returnCode;
        
        return completionCode;
    }


    /**
     * Provides a space to play with libPharmMLSO features. 
     */
    public void tryMoreSOStuff()
    {
        StandardisedOutput so = soResource.getDom();	
        PharmMLVersion pmlv = so.getUnmarshalVersion();		
        System.out.println("using PharmML Version: " + pmlv);
        
        //PharmMLRef myRef = 
        so.createPharmMLRef("dummyRefToAssociatedPharmML");
        
        //System.out.println(" PharmML reference name: " + myRef.getName());
        
    }

	/**
	 * Looks for script files with .lua extension in the current directory and creates an external file reference mapped by filename;
	 * also adds a raw data reference at relative address under the supplied subFolder name.
	 * @param workDir
	 * @param sob
	 * @param subFolder
	 * @return completionCode in {0,1,2,3}
	 */    
    int loadScriptsAndCreateMarkup(Path workDir, SOBlock sob, String subFolder)
    {
    	int completionCode = 0;
    	
        try (DirectoryStream<Path> stream =  Files.newDirectoryStream(workDir, "*.lua"))
        {
            for (Path entry: stream) 
            {
                //get name of next file 
                String fileName = entry.getFileName().toString();                
                
                //remove extension (and any whitespace just in case)
                assert(fileName.toLowerCase().endsWith(".lua"));
                String fNameOnly = fileName.substring(0, fileName.lastIndexOf('.')).replaceAll("\\s", "");
                
                ObjectFactory dsOf = new  ObjectFactory(); 
                
                JAXBElement<ExternalFile> jaxF = createExternalFileReference(fileName, fNameOnly, dsOf, null);  
                mapExternalScriptFiles.put(fileName, jaxF);
                
                JAXBElement<ExternalFile> jaxF2 = createExternalFileReference(fileName, fNameOnly, dsOf, subFolder);  
                RawResults rrr = sob.getRawResults();
                if(rrr !=null)
                    rrr.getDataFilesAndGraphicsFiles().add(jaxF2);
            }
        }
        catch (IOException e)
        {
        	e.printStackTrace();
        	completionCode = 1;
        }
        catch(Exception e)
        {
        	System.err.println("SO error when importing lua script");
        	e.printStackTrace();
        	completionCode += 2;
        }
        return completionCode;
    }

	/**
	 * Attempts to open tables each of *.csv files in the workDir and create for each corresponding SO tables and markup.
	 * @param workDir
	 * @param sob
	 * @param xReft
	 * @param destinationSubFolder
	 * @return completionCode in {0,1,2,3,4,5}
	 */
    int loadTablesAndCreateMarkup(Path workDir, SOBlock sob, TableXReferencer xReft, String destinationSubFolder)
    {
    	int completionCode = 0;
    	DosingInputsConverter dInputsConverter = new DosingInputsConverter();
    	
        try (DirectoryStream<Path> stream =  Files.newDirectoryStream(workDir, "*.csv"))
        {
            for (Path entry: stream) 
            {
                //get name of next csv file 
                String fileName = entry.getFileName().toString();                
                //remove extension (and any whitespace just in case)
                assert(fileName.toLowerCase().endsWith(".csv"));
                String fNameOnly = fileName.substring(0, fileName.lastIndexOf('.')).replaceAll("\\s", "");
           
                //sort out external file references and mark up corresponding raw result 
                ObjectFactory dsOf = new  ObjectFactory();
                JAXBElement<ExternalFile> jaxF = createExternalFileReference(fileName, fNameOnly, dsOf, null);                 
                mapExternalTableFiles.put(fileName, jaxF);                
                JAXBElement<ExternalFile>jaxF2 = createExternalFileReference(fileName, fNameOnly, dsOf, destinationSubFolder);     
                RawResults rrr = sob.getRawResults();
                if(rrr !=null)
                    rrr.getDataFilesAndGraphicsFiles().add(jaxF2);
      
                TableType tType = TableType.UNSET;
                try(BufferedReader in = Files.newBufferedReader(entry, Charset.defaultCharset()))
                {
                   String line = in.readLine();
                   
                   TableDVIDInfo tableInfo = new TableDVIDInfo(); //this structure collects info for optional DVID format conversion 
                   
                   //decide on table type and glean some preliminary table information               
                   switch(xReft.tableTypeCheckMode)
                   {
	                   case INFER_WITH_WARNINGS:
 	                	   tType = passToInferTableType(line, tableInfo);
	                       if(tType != xReft.getExpectedTableType(fNameOnly))                    	
	                    	   System.err.println("Warning: inferred table type for file " + fileName + " is not as expected");
	                       break;
	                       
	                   case INFER_EXCLUDING_UNEXPECTEDS:
	                   {
	                	   tType = passToInferTableType(line, tableInfo);
	                       if(tType != xReft.getExpectedTableType(fNameOnly))
	                       {	  
	                    	   tType = TableType.UNSET;
	                           System.err.println("Error: unable to set expected table for table " + fileName);
	                       }
	                       break;
	                   }
	                   case USE_EXPECTED_ONLY:
	                   {
	                	   tType = xReft.getExpectedTableType(fNameOnly);
	                	   break;
	                   }
	                		   
                   }

                   
                   if(tType == TableType.UNSET || tType == TableType.OLD_INPUTS || tType == TableType.PROPERTY_TRIPLE)
                        continue;	  
                	   
                   passToMapColumnHeaders(line, fNameOnly, tType, tableInfo, dInputsConverter, jaxF); //map structures currently class member scope
                  
                   passToMapTypesUnits(in, fNameOnly, tType, tableInfo);
                   
                   passToMapElementValues(in, fNameOnly, tType);
                   
                   markTableHeadersAndConvert2SO(fNameOnly,  tType, tableInfo, dInputsConverter, sob, jaxF);  //sets up external column definitions and inline rows 
                   
                   markMappedElements(fNameOnly,  tType, tableInfo, sob);
                   
                   markOtherElements(workDir, dsOf,  sob);  
                   
                   //only need to keep in memory those files that need to be cross-referenced
                   //So: need to keep various dosing info and BWT, BSA  to create dosing table
                   if(   !fNameOnly.equalsIgnoreCase(xReft.csvNames[CsvTables.DEMOGRAPHIC.ordinal()]) 
                	  && !fNameOnly.equalsIgnoreCase(xReft.csvNames[CsvTables.INPUTS_TRIAL_DESIGN.ordinal()]) 
                	  && !fNameOnly.toLowerCase().startsWith(xReft.csvNames[CsvTables.INPUTS_.ordinal()].toLowerCase()) //actually don't need Inputs_SoftwareVersions here
                	  && tType != TableType.CUSTOM_DOSING
                      && tType != TableType.INDIVIDUAL_PROFILE_INPUT)
                   {
                   	   cleanDownMaps(fNameOnly); 
                   }
                }
                catch (Exception e)
                {
                	System.err.println("SO creation error at fileName " + fileName);
                	completionCode = 1;
                	throw e;
                }
            }
        }  
        catch (IOException e)
        {
        	e.printStackTrace();
        	completionCode = 2;
        }
        
        try
        {
        	dInputsConverter =  new DosingInputsConverter();
        	dInputsConverter.markDosingTableFromMultipleMappings(sob, xReft, candidateColumns, candidateValues, candidateUnits);        	
        }
        catch(Exception e)
        {
        	System.err.println("SO error when creating dosing table");
        	e.printStackTrace();
        	completionCode+=3;
        }        

        return completionCode;
    }
    
    /**
     * Clears maps entries for the given filename without extension (does not affect mapExternalTableFiles, mapExternalScriptFiles) 
     * @param fNameOnly
     */
    void cleanDownMaps(String fNameOnly)
    {
    	if(fNameOnly == null)
    	{
            System.err.println("Warning: null filename encountered when trying to to clean down in-memory data maps");
            return;
    	}
    		
    	Vector<ColumnHeader> toClear = headerMap.get(fNameOnly);
    	if(toClear != null)
	       toClear.clear();
    	
    	toClear = dosingInfo.get(fNameOnly);
    	if(toClear != null)
 	       toClear.clear();
    	
    	toClear = toolSettings.get(fNameOnly);
    	if(toClear != null)
 	       toClear.clear();
    	
    	TreeMap<String, Integer>  toClear2 = candidateColumns.get(fNameOnly);
    	if(toClear2 != null)
 	       toClear2.clear();
    	
    	TreeMap<Integer, Vector<String> > toClear3 = candidateValues.get(fNameOnly);
    	if(toClear3 != null)
 	       toClear3.clear();
    	
    	TreeMap<Integer, UnitType > toClear4 = candidateUnits.get(fNameOnly);
    	if(toClear4 != null)
 	       toClear4.clear();
    }
    
    /**
     * Handles the details of setting up a JAXB reference to an external file. 
     * @param fileName name of the file with extension
     * @param nameOnly name of the file without extension
     * @param dsOf  data set object factory used type create the external file reference
     * @param folderName  name of folder to be added as a relative address
     * @return reference to JAXBElement for the ExternalFile object created. 
     */
    JAXBElement<ExternalFile> createExternalFileReference(String fileName, String nameOnly, ObjectFactory dsOf, String folderName)
    {
        ExternalFile ef = dsOf.createImportDataType();   
        if(folderName != null)        
        {
        	Path path = Paths.get(folderName + "\\" + fileName);
        	ef.setPath(".\\" + path.normalize().toString());
        }
        else
        	ef.setPath(".\\" + fileName);
        
        //ef.setFormat("0000.00");
        ef.setOid(nameOnly);
 
        JAXBElement<ExternalFile> jaxF = dsOf.createImportData(ef);
        return jaxF;
    }
    
    /**
     * Parses the supplied header-line to infer table type; also populates a TableDVIDInfo structure for the current table identifying column placement of certain keywords
     * @param line
     * @param tableInfo to  receive information 
     * @return the inferred type of table 
     */
    TableType passToInferTableType(String line, TableDVIDInfo tableInfo)
    {
   	    TableType tType = TableType.UNSET;

    	if (line != null)
        {
            //Inspect first line ..initially to work out type of table	
            StringTokenizer st = new StringTokenizer(line, "," );
            
            {					    	
                boolean bHaveIndex = false, bHaveTime = false, bHaveDVID =false, bHaveCumulativeInput = false;
                int tokenIndex= 0;
                
                final int SIGNATURE_LENGTH = 3;
                
                String[] tokens = new String[SIGNATURE_LENGTH];
                while (st.hasMoreTokens() && ((!bHaveTime || !bHaveIndex)  || (bHaveTime && !bHaveDVID)))
                {
                    String token = st.nextToken();
                    
                    if(tokenIndex == 0 && token.equalsIgnoreCase("Substrate"))
                    {
                        tType = TableType.OLD_INPUTS;
                        break;
                    }
                    else if (tokenIndex == 0 && token.length() >= 19 &&  token.substring(0, 19).equalsIgnoreCase("Custom Model Script"))
                    {
                        tType = TableType.SCRIPT;
                    }
                    else if (tokenIndex == 0 && token.length() >= 17 && token.substring(0, 17).equalsIgnoreCase("State variable Id"))
                    {
                        tType = TableType.ODE_STATE_INFO;
                    }
                    else if(tokenIndex == 0 && token.length()  >= 11 && token.substring(0, 11).equalsIgnoreCase("Dose Number"))
                    {
                    	tType = TableType.CUSTOM_DOSING;
                    }                 
                    else if (tokenIndex == SIGNATURE_LENGTH && tokens[0]!=null && tokens[0].equalsIgnoreCase("Name")  
                                                            && tokens[0]!=null && tokens[1].equalsIgnoreCase("Value")
                                                            && tokens[0]!=null && tokens[2].equalsIgnoreCase("Type"))
                    {
                        tType = TableType.PROPERTY_TRIPLE;
                        break;
                    }
                    
                    if (tokenIndex > 0  && token.length()  >= 16 && token.substring(0, 16).equalsIgnoreCase("Cumulative Input"))
                    {
                        bHaveCumulativeInput= true;
                    }
                    if(token.equalsIgnoreCase("Time"))
                    {
                        bHaveTime= true;
                        tableInfo.nTimeCol = tokenIndex + 1;
                        
                    }
                    if(token.equalsIgnoreCase("DVID"))
                    {
                        bHaveDVID = true;
                        tableInfo.nDVIDCol = tokenIndex + 1;
                        tableInfo.rawFileUsesDVID = true;
                        
                    }
                    else if(token.equals("Index"))
                    {
                        bHaveIndex = true;
                        tableInfo.nIndexCol = tokenIndex + 1;
                    }
                    tokenIndex++;				      		
                }
            
                if(tType == TableType.UNSET)
                {				      	
                    if(bHaveIndex)
                    {
                        if(bHaveTime)
                        {                         
                        	if(!bHaveCumulativeInput)
                        		tType = TableType.INDIVIDUAL_PROFILE;
                        	else
                        		tType = TableType.INDIVIDUAL_PROFILE_INPUT;
                        }
                        else
                            tType = TableType.INDIVIDUAL_PARAMETER;
                    }
                    else
                    {
                        if(bHaveTime)
                            tType = TableType.POPULATION_PROFILE;
                        else
                            tType = TableType.POPULATION_PARAMETER;
                    }
                }
            }
        }
        return tType;      
    }
    
    /**
     * Runs a single-pass parsing of the current (assumed header) line to put header labels into memory mapped according to table type,
     * possibly adding some table information (TableDVIDInfo data collection started in passToInferTableType). Decisions are made here also as to which 
     * headers are put into candidate lists - these will later have corresponding values pulled in so the list of candidate columns is restricted to save memory    
     * @param line
     * @param fNameOnly
     * @param tType
     * @param tableInfo       - may be updated with dependent variable DV column position, where relevant
     * @param dinvConverter   - supplied to indicate what columns it needs stored as candidates for conversion to dosing table
     * @param jaxF
     */
    void passToMapColumnHeaders(String line, String fNameOnly, TableType tType, TableDVIDInfo tableInfo, DosingInputsConverter dinvConverter, JAXBElement<ExternalFile> jaxF)
    {
        StringTokenizer st = new StringTokenizer(line, "," );  //note that this will not count blanks - to make this more robust consider refactoring to split( line, ",", -1) 
    	tableInfo.nColumnsInput = st.countTokens();
        int nColumn = 0;
        switch(tType)
        {
            case INDIVIDUAL_PARAMETER:
            {
                headerMap.put(fNameOnly, new Vector<ColumnHeader>());	
                
                if(candidateColumns.get(fNameOnly) == null)
            	    candidateColumns.put(fNameOnly, new TreeMap<String,Integer>());
                
        	    boolean  isDemographic = (fNameOnly.equalsIgnoreCase(xReft.csvNames[CsvTables.DEMOGRAPHIC.ordinal()]))
        	    		                 || (fNameOnly.equalsIgnoreCase(xReft.csvNames[CsvTables.DEMOGRAPHIC2.ordinal()]));

                while (st.hasMoreTokens())
                {
                     String token = st.nextToken();
                     nColumn++;
                     //if(!token.equalsIgnoreCase("Index"))
                     headerMap.get(fNameOnly).add(new ColumnHeader(token));	
                     
                     //flag these demographic columns as possibly needed for dose scaling  
                     if(isDemographic && (token.equalsIgnoreCase("Index") || token.equalsIgnoreCase("Weight") || token.equalsIgnoreCase("BSA") ) )
                    	 candidateColumns.get(fNameOnly).put(token, nColumn);
                }
                break;
            }
            
            case POPULATION_PARAMETER:
            {            	
            	headerMap.put(fNameOnly, new Vector<ColumnHeader>());
                toolSettings.put(fNameOnly, new Vector<ColumnHeader>());
            	
            	if(dosingInfo.get(fNameOnly) ==  null)
                    dosingInfo.put(fNameOnly, new Vector<ColumnHeader>());        
                if(candidateColumns.get(fNameOnly) == null)
            	    candidateColumns.put(fNameOnly, new TreeMap<String,Integer>());
            	
 
                String fNameOnlyLC = fNameOnly.toLowerCase();
                boolean  isInputsParameters = fNameOnlyLC.startsWith(xReft.csvNames[CsvTables.INPUTS_.ordinal()].toLowerCase()); 
                boolean  isInputsTD = fNameOnlyLC.endsWith("trialdesign"); //lower case comparator
                boolean  isInputsSVer = fNameOnlyLC.endsWith("softwareversions"); //lower case comparator       
                boolean  isInputsCmpd = isInputsParameters && !isInputsSVer && !isInputsTD;
                		
    		    Set<String> toolSettingsFishingSetTD = new TreeSet<String>(Arrays.asList(ToolSettingsFishingList.toolSettingsFishingListTD));  
   		    
    		    String token = null;
    		    String correctionString = null;
    		    while (st.hasMoreTokens())
	            {
    		    	nColumn++;  
    		    	
    		    	//FIX HEADER ERRORS: PART1: recognize some erroneously nonunique "End" headers in trial design input by finding precediing
    		    	//"Bin Start " values and grasp the correction to prepend such as "Bin Start 1 "      
	                if(isInputsTD && token != null)
	                {
	                	if(token.startsWith("Bin ") && token.endsWith(" start"))
	                	    correctionString = token.replace(" start", " ");
	                } 
    		    	
	               	token = st.nextToken();
	               	
	                //FIX HEADER ERRORS: PART2: prepend the corection prefix to the following "End" field 
	                if(isInputsTD && token != null && correctionString != null && token.equals("end"))
	                {
	                	token = correctionString + token;
	                }
	                else
	                	correctionString = null;
	                
	               	//also consider end-section of token which has colon separator
	                String endToken = "";
	                int colonIndex =  token.indexOf(':');
	                if(token.contains(":") && token.length() > colonIndex + 1)
	                	endToken = token.substring(colonIndex+1).trim();

	                
	                if(isInputsCmpd && dinvConverter.dosingFishingSetCmpd.contains(token))
	                {
	                	toolSettings.get(fNameOnly).add(null); 
		                dosingInfo.get(fNameOnly).add(new ColumnHeader(token));       //obsolete?
		                headerMap.get(fNameOnly).add(null);                           //add null to preserve record of column indexes 
		                candidateColumns.get(fNameOnly).put(token, nColumn);          //mark column as candidates for conversion to dosing table content              	
	                }
		            else if(isInputsTD && (dinvConverter.dosingFishingSetTD.contains(token) || dinvConverter.dosingFishingSetTD2.contains(endToken)))    
		            {
		            	toolSettings.get(fNameOnly).add(null); 
		                dosingInfo.get(fNameOnly).add(new ColumnHeader(token));       //obsolete?
		                headerMap.get(fNameOnly).add(null);                       //add null to preserve record of column indexes 
		                candidateColumns.get(fNameOnly).put(token, nColumn);          //mark column as candidates for conversion to dosing table content  
		            }      	
		            else if((isInputsTD || isInputsSVer) && toolSettingsFishingSetTD.contains(token)) 
		            {
		            	toolSettings.get(fNameOnly).add(new ColumnHeader(token));     //option : represent as header for raw data column  
		                headerMap.get(fNameOnly).add(new ColumnHeader(token));        //option : represent as popparams table non-null values here carry the index with them
		            }
		            else  //leave as reference to population parameter table
		            {
		            	toolSettings.get(fNameOnly).add(null); 
		                headerMap.get(fNameOnly).add(new ColumnHeader(token));    //header for raw data column		                
		            }	
	            }
	            break;                                	
	        }
            
            case CUSTOM_DOSING:
            {
	            //FROM CUSTOM DOSING for the specified compound  so CustomDosingSub or CustomDosiingInh1 etc  
	            /*
	            Dose Number	            
	            Day
	            Time                             //(hh:mm)
	            Offset                           //(h)
	            Dose
	            Dose Unit
	            Route of Administration
	            Duration
	            */
            	
            	if(dosingInfo.get(fNameOnly) ==  null)
            		 dosingInfo.put(fNameOnly, new Vector<ColumnHeader>());
                if(candidateColumns.get(fNameOnly) == null)
            	    candidateColumns.put(fNameOnly, new TreeMap<String,Integer>());
                
            	while (st.hasMoreTokens())
	            {
            		nColumn++;
	               	String token = st.nextToken();
            	    dosingInfo.get(fNameOnly).add(new ColumnHeader(token));
            	    candidateColumns.get(fNameOnly).put(token, nColumn);
	            }
            	break;
            }
                
                
            case ODE_STATE_INFO:
            {
           	    //use a RawResultsFile as a default file format ...varIndex?, nameNoDDI, nameDDI try to link the variable names to ct:DerivativeVariable items?
                break;
            }
                
            case INDIVIDUAL_PROFILE:
            {
            	//If we don't have a DVID type profile but want one, we will need to generate converted values so use candidateColumns for that
            	if(bMakeProfilesWithDVID && !tableInfo.rawFileUsesDVID)
            	    candidateColumns.put(fNameOnly, new TreeMap<String,Integer>());
            	
            	//If bMakeProfilesWithDVID is true, this will collect just the potential DV column names (anything not Trial, Time Population, Index or ID
            	//otherwise collects all except Index 
            	headerMap.put(fNameOnly, new Vector<ColumnHeader>());	
                while (st.hasMoreTokens())
                {
                     String token = st.nextToken();
                     nColumn++;
                     
                     //find any explicitly labelled DV column for use in cases where we have DVID 
                	 if(token.equalsIgnoreCase("DV"))      
                	 {                		 
                		 if(tableInfo.nDVIDCol > 0)
                		 {
                			 if(tableInfo.nDVCol < 0)
                			 {
                		         tableInfo.nDVCol = nColumn;
                			 }
                			 else
                			 {
                		         System.err.println("WARNING: more than one column found with DV label in " + fNameOnly.toString() 
                    					 + "\nDV mapping already assigned to column " + tableInfo.nDVCol 
                    					 + "\nDV mapping not assigned to column " +  nColumn);
                			 }
                		 }
                	 }
                	 
                	 //for DVID format, consider any other non-keyword names as DV columns to be combined for different DVIDs  
                	 if(bMakeProfilesWithDVID && (!(token.equalsIgnoreCase("Index") || token.equalsIgnoreCase("ID")) 
                			                   && !token.equalsIgnoreCase("Trial") 
                			                   && !token.equalsIgnoreCase("Population")
                			                   && !token.equalsIgnoreCase("DVID") 
                			                   && !token.equalsIgnoreCase("Time") ) )           		 
                     {
                		 headerMap.get(fNameOnly).add(new ColumnHeader(token));
                     }
                	 
                	 //for no DVID format or preexisting DVID format, consider all INCLUDING INDEX !!!  upadtae others similarly? 
                	 if(!bMakeProfilesWithDVID)// && !token.equalsIgnoreCase("Index"))
                     {
                         headerMap.get(fNameOnly).add(new ColumnHeader(token));
                     }
                	 
                	 if(bMakeProfilesWithDVID && !tableInfo.rawFileUsesDVID)
                	 {
                		 candidateColumns.get(fNameOnly).put(token, nColumn);
                	 }
                	 
                }
            }
            break;
                
            case INDIVIDUAL_PROFILE_INPUT:
            {
                	//This is destined to be converted for splicing into dosing table so go direct to candidates, don't need dosingInfo directly?
                	//if(dosingInfo.get(fNameOnly) ==  null)
               		//   dosingInfo.put(fNameOnly, new Vector<String>());
                    if(candidateColumns.get(fNameOnly) == null)
               	       candidateColumns.put(fNameOnly, new TreeMap<String,Integer>());
                	
                    while (st.hasMoreTokens())
                    {
                		nColumn++;
    	               	String token = st.nextToken();
	                	candidateColumns.get(fNameOnly).put(token, nColumn);
	            	    //dosingInfo.get(fNameOnly).add(token);
                    }
            }
            //other implementations pending
            default:
        }
    }
    //do we need to remove units rows from tables? get type info from units table lookup?

    /**
     * Passes through the units line, creating units and inferring some data types from the information available.
     * In some cases data types are not inferred but left for explicit markup based on table type and name 
     * when the markup is created.  
     * @param in              BufferedReader assumed set up at the units subheader line to be read
     * @param fNameOnly       name of the csv table file without extension
     * @param type            type of table      
     * @param tableInfo       used to check how to process profile tables
     */
    void passToMapTypesUnits(BufferedReader in, String fNameOnly, TableType type, TableDVIDInfo tableInfo) throws IOException
    {

   	    String line =  in.readLine();
   	    //don't use tokenizer as we need blanks to be included
   	    String[] unitFields = line.split(",", -1); 
   	    
   	    Vector<ColumnHeader> columnHeaders = null;
   	    int nExpectedUnitTokens = tableInfo.nColumnsInput;
   	    int nStartHeaderColumn  = 0;

   	       	    
   	    switch(type)
   	    {
	        case INDIVIDUAL_PARAMETER:
	       	{
	       		columnHeaders = headerMap.get(fNameOnly);
	       		break;
	       	}	
	        	
	        case INDIVIDUAL_PROFILE:
	       	{
	       		columnHeaders = headerMap.get(fNameOnly);
	       		nStartHeaderColumn  = nExpectedUnitTokens - columnHeaders.size();  //any non DVs with no headers currently assumed at left       	
	       		break;
	       	}
	       	
	        case POPULATION_PARAMETER:
	        {
	        	columnHeaders = headerMap.get(fNameOnly);
	        	break;       	
	        }
    	
	        default:  
	        //case POPULATION_PROFILE                   //currently not processed - no tables of this taype  so far
   	        //case INDIVIDUAL_PROFILE_INPUT:            //data passed to dosing table as candidates so don't need to process here as indivProfiles;    
   	        //case CUSTOM_DOSING:                       //data passed to dosing table as candidates so don't need to process here as indivProfiles;    
   	        //case TableType.ODE_STATE_INFO):           //no units
   	       	        	
   	    }     	    

   	    
   	    if(nExpectedUnitTokens > 0 && columnHeaders != null && unitFields.length == nExpectedUnitTokens)
   	    {
   	    	assert(nStartHeaderColumn + columnHeaders.size() == unitFields.length);
   	    	
	   	    for(int nField = nStartHeaderColumn; nField < unitFields.length; nField++)
	        {
	             ColumnHeader currentHeader = columnHeaders.elementAt(nField - nStartHeaderColumn);
	             String field = unitFields[nField];
	             if(currentHeader != null && field != null & !field.isEmpty())
	             {
                     currentHeader.setCustomUnit(field);
	            	 
	            	 //Rule : if we have units then assume REAL 
                     currentHeader.setDataType(UnitType.TypeID.REAL);
	             }                
	        }
   	    }
 	    
   	    
   	    //Store units from Inputs_TrialDesign table as the units for dose column are needed to infer individual dose-scaling for the dosing table   
   	    if(fNameOnly.equalsIgnoreCase(xReft.csvNames[CsvTables.INPUTS_TRIAL_DESIGN.ordinal()]))
   	    {
   	    	candidateUnits.put(fNameOnly, new TreeMap<Integer, UnitType>());	
   	    	TreeMap<Integer, UnitType> utMap = candidateUnits.get(fNameOnly);
            
	    	//Find target columns with Dose after colon (compound identifier will be before) 
   	    	TreeSet<Integer> targetColumnIds = new TreeSet<Integer>(); //sorted
   	    	TreeMap<String, Integer> cols = candidateColumns.get(fNameOnly);     	    	
   	    	for(String colName : cols.keySet())
   	    	{
   	    		
   	    		int colonIndex =  colName.indexOf(':');
   	    		if(colonIndex > 0 &&  colName.substring(colonIndex + 1).trim().equalsIgnoreCase("Dose"))
   	    		{
   	    			if(cols.get(colName) != null)
   	    			    targetColumnIds.add(cols.get(colName));
   	    		}  	
   	    		
   	    	}
   	    	
   	    	//Try to match dosing unit type at the target columns; other columns leave as unset, or blank if found blank   
   	   	    String[] sts = line.split(","); 
   	    	Iterator<Integer> iit = targetColumnIds.iterator();   	    	
   	    	int targetColId;
   	    	if(iit.hasNext())
   	    	{
   	    		targetColId = iit.next();
	   	    	int nCol = 0;
		   	    for(int nToken=0; nToken < sts.length; nToken++)
		   	    {
		   	    	nCol++;
		   	    	String token = sts[nToken];
		   	    	UnitType ut = new UnitType(UnitType.UnitID.UNSET);
		   	    	if(nCol == targetColId)
		   	    	{	
		   	    		if(token.equalsIgnoreCase("mg/kg"))
		   	    			ut.setUnit(UnitType.UnitID.UNIT_MG_PER_KG);
		   	    		else if (token.equalsIgnoreCase("mg/m2"))
		   	    			ut.setUnit(UnitType.UnitID.UNIT_MG_PER_M2);	
		   	    		else if (token.equalsIgnoreCase("mg"))
		   	    			ut.setUnit(UnitType.UnitID.UNIT_MG);	
		   	    		targetColId++;
	   	    		}
		   	    	else if(token.equals(""))
		   	    	{
		   	    		ut.setUnit(UnitType.UnitID.BLANK);	   	    		
		   	    	}
		   	    	utMap.put(nCol, ut);
	   	    	}
   	    	}
   	    }   	    
    }
    
     /**
     * Runs a possibly multi-line pass through the stream represented by the BufferedReader, to pick out features flagged as candidates for mark up. 
     * The flagged columns are identified from existing headers in the member candidateColumns map. The results are written to the member candidateValues map.
     * @param  in         reader assumed already open at first non-header line (of the underlying csv file); the reader will typically end at 
     *                    end of file or after the expected number of lines for table type has been reached. 
     * @param  fNameOnly  name of csv file without extension 
     * @param  tType      table type represented by the file  
     * @throws IOException 
     */
    void passToMapElementValues(BufferedReader in, String fNameOnly, TableType tType) throws IOException
    {
    	if(candidateColumns.get(fNameOnly) == null  || candidateColumns.get(fNameOnly).isEmpty())
    		 return;
    	
    	TreeSet<Integer> candidateColNos = new TreeSet<Integer>();
		for(Map.Entry<String,Integer> entry : candidateColumns.get(fNameOnly).entrySet()) 
		    candidateColNos.add(entry.getValue());
		
    	candidateValues.put(fNameOnly, new TreeMap<Integer, Vector<String>>());
    	
		String line = in.readLine();//should start at first non-header data values line
		
		while(line != null)
		{	
		    //iterate through tokens (values)  to find matching column numbers in the candidates and store the values for later markup 
		    StringTokenizer st = new StringTokenizer(line, ",");
			int nCol = 1;
			Iterator<Integer> it = candidateColNos.iterator();
			int nColCandidate = -1;
			if(it.hasNext())
			    nColCandidate = it.next();
			while (st.hasMoreTokens())
			{
	            String token = st.nextToken();
	                   
	            if(nCol == nColCandidate)
	            {   
	                Vector<String> values = candidateValues.get(fNameOnly).get(nCol);
	                if(values == null)
	                {
	                    candidateValues.get(fNameOnly).put(nCol, new Vector<String>());
	                	values = candidateValues.get(fNameOnly).get(nCol);
	                }
	                values.add(token);
	             	                    
	                if(it.hasNext())
	                    nColCandidate = it.next();
	                else
	                   break;
	            }
	            nCol++;          	   
			}
			
			if(tType == TableType.POPULATION_PARAMETER)
			   	break;
			else
			   	line = in.readLine();
		}
    }
    
   
    /**
     * Using previously constructed in-memory maps of column headers, creates the SO dataset and column definitions for the corresponding table according to table type/name.
     * Also collects settings columns into an additional set PopulationParameters of SimcypSettings population element with specific extFileNo values and 
     * @param nameOnly        originating csv file name without extension
     * @param tType           type of table to assume  for the current table/file
     * @param tableInfo       contains table information for DVID conversion if required (can be null if bMakeProfilesWithDVID is false).
     * @param dinConverter    converter object for creating dosing table (header here)   
     * @param jaxF            JAXB external file reference corresponding to the named csv file.
     */
    void markTableHeadersAndConvert2SO(String nameOnly, TableType tType, TableDVIDInfo tableInfo, DosingInputsConverter dinConverter, SOBlock sob, JAXBElement<ExternalFile> jaxF)
    {
        if(nameOnly == null)
            return;
         
        //create the common table elements here (however these may not be used if nothing found to fill them)
   		SimulationItem table = new SimulationItem();
      	table.setExternalFile(jaxF.getValue());
       	table.setId(nameOnly);  
       	table.setName(nameOnly);  //do we need both id and name?
       	AnnotationType an = new AnnotationType();
       	an.setValue("Currently using path to external file here, not necessarily that in archive");
       	table.getExternalFile().setDescription(an);
       	
       	//Later we shall need to flag the header and units rows as header types...are we waiting for lib update(s) with PharML0.7 support for this
       	//<HeaderTypeType>  mainHeder/subHeader/userDefined 
       	//WrappedList<DatasetRow> tableRows = table.getListOfRow();
       	//tableRows.get(0).setHeaderType(...);
       	
       	
       	if(tType == TableType.INDIVIDUAL_PARAMETER)
       	{
       		List<SimulationItem> indivParamsTableList = sob.getSimulation().getListOfIndivParameters(); 
       		
           	Vector<ColumnHeader> vs = headerMap.get(nameOnly); 
           	if(vs != null && !vs.isEmpty())
           	{           	
            	HeaderColumnsDefinition	hcsDef = table.getDefinition();
            	
            	if(hcsDef == null)
            		hcsDef = table.createDefinition();

            	for(int i=0; i < vs.size(); i++)
			    {
			       	ColumnHeader ch = vs.elementAt(i);
			       	String columnName = null;
			       	if(ch != null)
			       		columnName = ch.getName();
			    	   	
			       	if(columnName != null);
			       	{
			       		String colNameId =  columnName.trim().replaceAll("\\s", "_"); //eventually can add original colName as name attribute or in annotation 
			       		ColumnDefinition cdf = null;
			       		if(columnName.equalsIgnoreCase("Index") || columnName.equalsIgnoreCase("ID"))
			       			hcsDef.createColumnDefinition("Index", ColumnType.ID, SymbolType.INT, 1);
			       		else if(columnName.equalsIgnoreCase("Trial"))
			       			hcsDef.createColumnDefinition("Trial", ColumnType.REPLICATE, SymbolType.INT, 2);
			       		else if(columnName.equalsIgnoreCase("Population"))
			       			hcsDef.createColumnDefinition("Population", ColumnType.COVARIATE, SymbolType.INT, 3);
			       		else if(ch.hasUnitTypeInfo())
			       		{
			       			if(ch.getUnit() != UnitType.UnitID.UNSET)
			       			{
				         	    cdf = hcsDef.createColumnDefinition(colNameId, ColumnType.COVARIATE, ch.getSymbolType(), i+1);
				         	    an = new AnnotationType();
				         	    an.setValue("Unit: " + ch.getUnitName() );
				         	    cdf.setDescription(an);
			       			}
			       		}				          	   
			       		else
			       		{
			       			cdf = hcsDef.createColumnDefinition(colNameId, ColumnType.COVARIATE, SymbolType.REAL, i+1);			    	   			
			       		}
			       		
		        	    boolean  isDemographic = (nameOnly.equalsIgnoreCase(xReft.csvNames[CsvTables.DEMOGRAPHIC.ordinal()]))
	    		                              || (nameOnly.equalsIgnoreCase(xReft.csvNames[CsvTables.DEMOGRAPHIC2.ordinal()]));
			       		if(isDemographic)
			       		{
					        if(columnName.equalsIgnoreCase("Sex"))
					       		cdf.setValueType(SymbolType.STRING);
					       	else if (columnName.equalsIgnoreCase("Sex Code"))
					       		cdf.setValueType(SymbolType.INT);
			       		}
	            	}
		        }	
            	
	          	indivParamsTableList.add(table);
           	}
    	}
       	else if(tType == TableType.POPULATION_PARAMETER)
       	{
       		List<SimulationItem> popParamsTableList = sob.getSimulation().getListOfPopulationParameters(); 

       		//Some values will be candidates for the dosing table  so make sure that table has columns set up
           	if(!bDosingTemplateCreated)
           		bDosingTemplateCreated = dinConverter.createDosingTableTemplate(sob);


       		//do we have tool settings to process?
           	boolean bNeedToolSettings = toolSettings.get(nameOnly) != null && toolSettings.get(nameOnly).size() > 0;
           	if(bNeedToolSettings)
           	{
           		boolean bAllNull = true;
           		for(ColumnHeader ch : toolSettings.get(nameOnly))
           		{
           			bAllNull &= (ch == null);
           		}         
           		bNeedToolSettings &= !bAllNull; 
           	}
           	 
           	if(bNeedToolSettings)
	        {	
	           	//Anyway mark up columns for the parameters (currently held in PopParameters) for an external file
	            Vector<ColumnHeader> vs = toolSettings.get(nameOnly);  //using this for specific subset trial_design column, those excluded are null 
	            
	            if(vs != null && !vs.isEmpty())
	            {
	            	
	    	        //List the file in <ToolSettings Element>  (no column info here)
	               	sob.getToolSettings().getListOfFile().add(jaxF.getValue());
	               	
	               	//Mark up the corresponding table in <Simulation><PopulationParameters name=${nameOnly} ExFileNo=... >
	           		//create new table as below
	           		SimulationItem settingsTable = new SimulationItem();
	           		final String SETTINGS_TABLE_NAME = "SimcypSettingsTable";
	           		settingsTable.setName(SETTINGS_TABLE_NAME);
	               	settingsTable.setId(SETTINGS_TABLE_NAME + "from" + nameOnly);	               	
		           	settingsTable.setExtFileNo(sob.getToolSettings().getListOfFile().size());
		          	settingsTable.setExternalFile(jaxF.getValue());
		           	
		           	String note = "Settings treated here as quasi PopulationParameters; "
		                  + "also put in toolSettings element as external file but without column markup.";
		           	an = new AnnotationType();
		           	an.setValue(note);
		           	settingsTable.setDescription(an);
	            
	              	HeaderColumnsDefinition	hcsDef = settingsTable.getDefinition();
		           	if(hcsDef == null)
		           		hcsDef = settingsTable.createDefinition();
	    	        int i = 0;
	    	        for(ColumnHeader ch : vs)
	    	        {
	            		if(ch != null)
	            		{
				    	   	String element = ch.getName();
	            			String colNameId =  element.trim().replaceAll("\\s", "_");
           			
				       		if(ch.hasUnitTypeInfo())
				       		{
				       			if(ch.getUnit() != UnitType.UnitID.UNSET)
				       			{
					         	    ColumnDefinition cdf = hcsDef.createColumnDefinition(colNameId, ColumnType.UNDEFINED, ch.getSymbolType(), i+1);
					         	    an = new AnnotationType();
					         	    an.setValue("Unit: " + ch.getUnitName() );
					         	    cdf.setDescription(an);
				       			}
				       		}				          	   
				       		else
				       		{
				       			hcsDef.createColumnDefinition(colNameId, ColumnType.UNDEFINED, SymbolType.STRING, i+1);			    	   			
				       		}
	            		}
	            		i++;
	    	        }	
	    	        popParamsTableList.add(settingsTable);
	            }
	        }
	        
	        	          
	        {
	            Vector<ColumnHeader> vs = headerMap.get(nameOnly);  
	            if(vs != null && !vs.isEmpty())
	            {

	            	HeaderColumnsDefinition	hcsDef = table.getDefinition();
	            	if(hcsDef == null)
	            		hcsDef = table.createDefinition();
	            	
	            	int i = 0;
	            	for(ColumnHeader ch : vs)
	            	{
	            		if(ch != null)
	            		{
				    	   	String element = ch.getName();
	            			String colNameId =  element.trim().replaceAll("\\s", "_");
           			
				       		if(ch.hasUnitTypeInfo())
				       		{
				       			if(ch.getUnit() != UnitType.UnitID.UNSET)
				       			{
					         	    ColumnDefinition cdf = hcsDef.createColumnDefinition(colNameId, ColumnType.UNDEFINED, ch.getSymbolType(), i+1);
					         	    an = new AnnotationType();
					         	    an.setValue("Unit: " + ch.getUnitName() );
					         	    cdf.setDescription(an);
				       			}
				       		}				          	   
				       		else
				       		{
				       			hcsDef.createColumnDefinition(colNameId, ColumnType.UNDEFINED, SymbolType.STRING, i+1);			    	   			
				       		}
	            		}
	            		i++;
	            	}	
    	        
	            	popParamsTableList.add(table);
	            }
	        }

       	}
       	else if(tType == TableType.INDIVIDUAL_PROFILE)
       	{
       		List<SimulationItem> simProfilesList = sob.getSimulation().getListOfSimulatedProfiles(); 
           	
           	table.setId(nameOnly);
	            	
            if(bMakeProfilesWithDVID)
            {
    	       	if(tableInfo.rawFileUsesDVID)
    	       	{	    	       		    	        		
    	       		if(tableInfo.nDVCol >= 0 && tableInfo.nTimeCol > 0)
    	        	{
    	        		HeaderColumnsDefinition profileHeaders = table.getDefinition();
   			           	if(profileHeaders == null)
   			           		profileHeaders = table.createDefinition();

   			            profileHeaders.createColumnDefinition("Index", ColumnType.ID,   SymbolType.ID,   tableInfo.nDVCol);
    	        		profileHeaders.createColumnDefinition("DV",    ColumnType.DV,   SymbolType.REAL, tableInfo.nDVCol);
    	        		profileHeaders.createColumnDefinition("DVID",  ColumnType.DVID, SymbolType.INT,  tableInfo.nDVIDCol);
    	        		profileHeaders.createColumnDefinition("TIME",  ColumnType.TIME, SymbolType.REAL, tableInfo.nTimeCol);
	    		        		
    		        	int nCol = 0;
    		        	//currently allowing other columns alongside eg Population, Trial
    		        	Vector<ColumnHeader> vs = headerMap.get(nameOnly);
    		        	if(vs != null && !vs.isEmpty())
    		        	{
    				         for(ColumnHeader ch : vs)
    				         {
    					    	 String columnName = ch.getName();
    				        	 
    				           	 nCol++;
    				           	 if(nCol != tableInfo.nDVCol && nCol != tableInfo.nDVIDCol && nCol != tableInfo.nTimeCol)
    				           	 {
    				            	 String colNameId =  columnName.replaceAll("\\s", "_"); //eventually might add original colName as name atttribute or in annotation 
    				            	 
    				            	 if(ch.getUnit() != UnitType.UnitID.UNSET)
    					       		 {
        				           	  	 ColumnDefinition cdf = profileHeaders.createColumnDefinition(colNameId, ColumnType.UNDEFINED, SymbolType.STRING, nCol);
        				           	  	 
    						             an = new AnnotationType();
    						             an.setValue("Unit: " + ch.getUnitName() );
    						             cdf.setDescription(an);
    					       		 }
    				            	 else
    				            		 profileHeaders.createColumnDefinition(colNameId, ColumnType.UNDEFINED, ch.getSymbolType(), nCol);
    				           	  	 	 
    				           	 }
    				         }
    		        	}
    	        	}
    	        }
    		    else 
    		    {
    		       	new ProfileDVConverter().convertProfilesToDVIDBased(nameOnly, headerMap, tableInfo, candidateColumns, candidateValues);
	    		   
    		       	//for now generating the PharmML table internally..maybe later create new file for external referencing
    		        //here first the column headers
    		      	TreeMap <Integer, String> listSequence = new TreeMap<Integer, String>();
    		       	for (Entry<String, Integer> colInfo : candidateColumns.get(nameOnly).entrySet())
    		        {
    		       		listSequence.put(colInfo.getValue(), colInfo.getKey());
    		        }
	    		
    	            Vector<ColumnHeader> vs = headerMap.get(nameOnly);
    
    		       	for (Entry<Integer, String> colInfo2 : listSequence.entrySet())
    		       	{
    		 	   	    if(colInfo2 != null)
    			   	    {
    			   	       	Integer nCol    = colInfo2.getKey();
    			   	       	String  colName = colInfo2.getValue();
    			   	       	boolean doneID = false, doneDVID = false, doneTime = false, doneDV = false, doneTrial = false, donePop = false;
	    			    	       	
	    	       			HeaderColumnsDefinition profileHeaders = table.getDefinition();
    			           	if(profileHeaders == null)
    			           		profileHeaders = table.createDefinition();

    			   	       	if(colName != null && nCol != null )
    			           	{
    			           		if(!doneID && colName.equalsIgnoreCase("Index") || colName.equalsIgnoreCase("ID"))
    			           		{
    			           			profileHeaders.createColumnDefinition("ID", ColumnType.ID, SymbolType.INT, nCol);
    			           			doneID = true;
    			           		}
    			           		else if(!doneDVID && colName.equalsIgnoreCase("DVID"))
    			           		{
    			           			profileHeaders.createColumnDefinition("DVID", ColumnType.DVID, SymbolType.STRING, nCol);
    			           			doneDVID = true;
    			           		}
    			           		else if(!doneTime && colName.equalsIgnoreCase("Time"))
    			           		{
    			           			profileHeaders.createColumnDefinition("Time", ColumnType.TIME, SymbolType.REAL, nCol);
    			           			doneTime = true;
    			           		}
    			           		else if(!doneDV && colName.equalsIgnoreCase("DV"))
    			           		{
    			           			profileHeaders.createColumnDefinition("DV", ColumnType.DV, SymbolType.REAL, nCol);
    			           			doneDV = true;
    			           		}
    			           		else if(!donePop && colName.equalsIgnoreCase("Population"))
    			           		{
    			           			profileHeaders.createColumnDefinition("Population", ColumnType.UNDEFINED, SymbolType.INT, nCol);
    			           			donePop = true;
    			           		}
    			           		else if(!doneTrial && colName.equalsIgnoreCase("Trial"))
    			           		{
    			           			profileHeaders.createColumnDefinition("Trial", ColumnType.UNDEFINED, SymbolType.INT, nCol);
    			           			doneTrial = true;
    			           		}
    			           		else
    			           		{
    			           			if(vs != null && nCol < vs.size() )
    			           			{    			           				    			           				
    			           				ColumnHeader ch = vs.elementAt(nCol);
    			           				if(ch != null && ch.hasUnitTypeInfo())
    			           				{
    			           					ColumnDefinition cdf = profileHeaders.createColumnDefinition(colName, ColumnType.COVARIATE, ch.getSymbolType(), nCol);
    						       			
    			           					if(ch.getUnit() != UnitType.UnitID.UNSET)
    						       			{
    							         	    an = new AnnotationType();
    							         	    an.setValue("Unit: " + ch.getUnitName() );
    							         	    cdf.setDescription(an);
    						       			}
    			           				}
    			           			}
    			           			else
    			           			    profileHeaders.createColumnDefinition(colName, ColumnType.COVARIATE, SymbolType.STRING, nCol);
    			           		}
    			           	}
    			        }
    			    }
    	        }
            }
    	    else if(!tableInfo.rawFileUsesDVID)
    	    {
                int nCol = 0;
            	Vector<ColumnHeader> vs = headerMap.get(nameOnly);
            	if(vs != null  && !vs.isEmpty())
            	{
    		         for(ColumnHeader item : vs)
    		         {    		
                 		HeaderColumnsDefinition profileHeaders = table.getDefinition();
    			        if(profileHeaders == null)
    			            profileHeaders = table.createDefinition();
    		        	 
    		        	String columnName = item.getName();
    		        	
    		          	nCol++;		
    		          	boolean doneID = false, doneTime = false, doneTrial = false, donePop = false;
    		           	//pending better datatype info 
    		           	ColumnType colType = ColumnType.UNDEFINED;
    		           	SymbolType dataType = SymbolType.STRING; 
    		           	an = null;
    	           		if(!doneID && columnName.equalsIgnoreCase("Index") || columnName.equalsIgnoreCase("ID"))
    	           		{
    	           			colType  = ColumnType.ID;
    	           		    dataType = SymbolType.INT;
    	           			doneID = true;	    	            			
    	           		}
    	           		else if(!doneTime && columnName.equalsIgnoreCase("Time"))
    	           		{
    	           			colType  = ColumnType.TIME;
    	           		    dataType = SymbolType.REAL;
    	           			doneTime = true;
    	           		}
    	           		else if(!donePop && columnName.equalsIgnoreCase("Population"))
    	           		{
    	           			colType  = ColumnType.UNDEFINED;
    	           		    dataType = SymbolType.INT;
    	           		    donePop = true;
    	           		}
    	           		else if(!doneTrial && columnName.equalsIgnoreCase("Trial"))
    	           		{
    	           			colType  = ColumnType.UNDEFINED;
    	           		    dataType = SymbolType.INT;
    	           			doneTrial = true;
    	            	}
    	           		else
    	           		{
    	           			colType  = ColumnType.COVARIATE;
	           				ColumnHeader ch = vs.elementAt(nCol-1);
	           				if(ch != null && ch.hasUnitTypeInfo())
	           				{
	           					dataType = ch.getSymbolType();	           					
	           					if(ch.getUnit() != UnitType.UnitID.UNSET)
				       			{		
	           						an = new AnnotationType();
					         	    an.setValue("Unit: " + ch.getUnitName() );					         	   
				       			}	           					
	           				}
	           				else
	           				    dataType = SymbolType.STRING;	
    	           		}
                		String colNameId =  columnName.trim().replaceAll("\\s", "_"); //eventually can add original colName as name atytribute or in annotation 
                		//need to infer type from units or use csv data type  row , but for now 

                		ColumnDefinition cdf = profileHeaders.createColumnDefinition(colNameId, colType, dataType, nCol);
                		cdf.setDescription(an);
    		        }
    		        simProfilesList.add(table);
            	}	    	           
            }	            
       	}
        else if (tType == TableType.CUSTOM_DOSING || tType == TableType.INDIVIDUAL_PROFILE_INPUT)
        {
        	//Some values will be candidates for the dosing table so make sure that table has columns set up
        	if(!bDosingTemplateCreated)
        		 bDosingTemplateCreated = dinConverter.createDosingTableTemplate(sob);
        }
        //OdeStateInfo
        else if (tType == TableType.ODE_STATE_INFO)  
        {
        	//not sure where to put this, try it as a pop param, and maybe annotate the rest
       		List<SimulationItem> popParamsTableList = sob.getSimulation().getListOfPopulationParameters(); 
        	if(nameOnly.equalsIgnoreCase("OdeStateInfo"))
        	{
	           	table.setExternalFile(jaxF.getValue());
	           	
	           	HeaderColumnsDefinition odeStateHeaders = table.getDefinition();
            	if(odeStateHeaders == null)
            		odeStateHeaders = table.createDefinition();	           	
	           	odeStateHeaders.createColumnDefinition("State_Variable_Id",             ColumnType.UNDEFINED, SymbolType.INT,    0);
	           	odeStateHeaders.createColumnDefinition("State_Variable_Name_(NoPKDDI)", ColumnType.UNDEFINED, SymbolType.STRING, 1);
	           	odeStateHeaders.createColumnDefinition("State_Variable_Name_(PKDDI)",   ColumnType.UNDEFINED, SymbolType.STRING, 2);
        	}
        	else
        	{
        		Assert.assertTrue(false); //not expecting any other table here
        	}
        	popParamsTableList.add(table);  //TO COMPLETE
        } 
	}
    
    
    
    /**
     * Creates the markup for elements with values set in the candidateValues map, according to the table type 
     * @param fNameOnly   name of originating csv file without extension
     * @param tType       table type to be assumed
     * @param sob         SOBlock for access to the DOM elements
     */
    void markMappedElements(String fNameOnly, TableType tType, TableDVIDInfo tableInfo, SOBlock sob)
    {
    	if(candidateColumns.get(fNameOnly) == null)
    		return;
    	
	    switch(tType)
	    {
		    case POPULATION_PARAMETER:
		    {
		    	//<TaskInformation><RunTime> 
		    	//currently interpreted as Simulation Duration (will not count outputs generation time)
		    	String key = "Simulation Duration";
		    	if(fNameOnly.equalsIgnoreCase(xReft.csvNames[CsvTables.INPUTS_TRIAL_DESIGN.ordinal()]) &&
		    	   candidateColumns.get(fNameOnly).containsKey(key))
		    	{
					Integer col = (candidateColumns.get(fNameOnly)).get(key); 
					TreeMap<Integer, Vector<String>> fileCandidateValues = candidateValues.get(fNameOnly);
					if(col != null && fileCandidateValues.get(col) != null && fileCandidateValues.get(col).size() == 1)
					{
						String sValue = fileCandidateValues.get(col).elementAt(0);   //add in more null protections here
						double value = Double.NaN;
						if(sValue != null)
						{
							try
							{
								value = Double.parseDouble(sValue);
							}
							catch(NumberFormatException e)
							{
								//leave at NaN
								System.err.println(e.getMessage());
							}
						}
						sob.getTaskInformation().setRunTime(new Rhs(new RealValue(value)));
					}
		    	}
		        break;
	        }
		    
		    case INDIVIDUAL_PROFILE:
		    {
		    	if(tableInfo == null || tableInfo.rawFileUsesDVID)
		    		break;
		    	
		    	//temporary fix to find the relevant profile table -now there is potentially more than one 
		    	//- may move to supplying this info directly? 
		  		List<SimulationItem> simProfilesList = sob.getSimulation().getListOfSimulatedProfiles(); 
		  		SimulationItem profilesTable = null;
		  		for(SimulationItem profileTable : simProfilesList)
		    	{		    		
		 	        if(profileTable.getId().equals(fNameOnly))
		 	            profilesTable = profileTable;
		    	}
		    	
		    	int nDataRows = 0;
		    	Vector<String> rowVals  = new Vector<String>();
	 	        Vector<String> firstColValues = candidateValues.get(fNameOnly).firstEntry().getValue();
	 	        if(firstColValues != null)
	 	        {
	 	    	   nDataRows = firstColValues.size();
	 	        }
	 	       
	     
		 	    String[] row = null;
		 	        
		 	    for (int nDataRow = 0; nDataRow < nDataRows; nDataRow++)
		 	    {
		 	     	rowVals.clear();
				    for (Map.Entry<Integer, Vector<String> > valInfo : candidateValues.get(fNameOnly).entrySet())
				    { 
				        //assert tree is sorted on Integer values
				  	    //assert all value vectors are of same length
				    		  
				   	    if(valInfo != null)
				   	    {
				   	   	    //Integer nCol           = valInfo.getKey();  //1-based
				   	        Vector<String> values  = valInfo.getValue();			    	        
				   	        if(valInfo != null && values != null)
					        {
				    	        	
				   	        	String value = values.elementAt(nDataRow);
				   	        	//SymbolType varType = profilesTable.getListOfColumnDefinition().get(nCol-1).getValueType(); //need zero-based col index in list
				    	        	
				   	            //assert: nCol is contained as value in candidateColumns
				                rowVals.add(value);
					        }
				   	    }
				    }
			
			        //add the row to the table
			    	if(rowVals.size() > 0)
			    	{
			    	    	if(row == null)
			    	    		row = new String[rowVals.size()];
			    	        profilesTable.createRow(rowVals.toArray(row));
			    	}
		    	}
		    }

	 	       
		    //OTHERS?
		    //<TaskInformation><Message> [<ToolName>,<Name>,<Severity>, <Content>]
	        default:

	    }
    }

	/**
	 * Some additional experimental markup of of other elements, not necessarily dependent on csv file
	 * @param workDir
	 * @param dsOf
	 * @param sob
	 */
    void markOtherElements(Path workDir, ObjectFactory dsOf, SOBlock sob)
    {
    	//<TaskInformation><OutputFilePath>
        if(sob.getTaskInformation() != null)
        {
	        //What should this really point to? Here trying the local outputs directory absolute path for starters
	        ExternalFile ef = dsOf.createImportDataType();
	        ef.setPath(workDir.toAbsolutePath().toString());
	    	sob.getTaskInformation().setOutputFilePath(ef);   
        }
    }
    
    /**
     *  Creates a Combine archive around the data tables and scripts and SO xml 
     *  @param  omexName name for archive file to be produced
     *  @param  soName name of PharmMLSO file to be incorporated
     *  @return completionCode as a multiple of 1000
     */
    public int createArchive(String omexName, String soName)
    {
    	int completionCode = 0;
        try
        {

        	CombineArchive ca = new CombineArchive (new File (omexName));	
            
            Set<String> fileNames = mapExternalTableFiles.keySet();
            for(String fileName : fileNames)
            {	
                ca.addEntry ( new File (fileName), 
                              "/" + DATA_FOLDER_NAME + "/" + fileName ,   
                              new URI ("text/csv"));
            }
            
            fileNames = mapExternalScriptFiles.keySet();
            
            for(String fileName : fileNames)
            {	
            	//Path path =  mapExternalscriptFiles.get(fileName).getValue().getPath();
            		
                ca.addEntry ( new File (fileName), 
                		"/" + SCRIPT_FOLDER_NAME + "/" + fileName , new URI (LUA_URI)); 
            }
            
            ArchiveEntry soFile = ca.addEntry (
                       new File (soName),
                       soName, 
                       new URI ("http://www.ddmore.eu/pharmmlso"));	
        
            
            ca.setMainEntry (soFile);
            ca.pack ();
            ca.close ();            
        }
        catch(CombineArchiveException e)	
        {
        	e.printStackTrace();
        	completionCode = 1000;
        }
        catch(IOException e)		
        {
            e.printStackTrace();
            completionCode = 2000;
        }
        catch(JDOMException e)	
        {
            e.printStackTrace();
            completionCode = 3000;
        } 
        catch (ParseException e) 
        {
            e.printStackTrace();
            completionCode = 4000;
        } 
        catch (URISyntaxException e) 
        {
            e.printStackTrace();
            completionCode = 5000;
        }
        catch (TransformerException e) 
        {
            e.printStackTrace();
            completionCode = 6000;
        }	
        return completionCode;
    }

    
    
    
    /**
     *  Reads standard csv format messages from a special file in workDir called "Messages.tmp"
     *  if it exists.
     */
    public int loadMessagesAndCreateMarkup(Path dir, TaskInformation ti)
    {
    	final String MESSAGES_FILENAME = "Messages.tmp";
    	final int N_FIELDS_PER_LINE = 5;
    	
    	Path messagesPath = null;
    	try
    	{
    		messagesPath = Paths.get(dir.toString(), MESSAGES_FILENAME);
    	}
    	catch(InvalidPathException e)
    	{
    		System.err.println("Path to Simcyp messages file is invalid");
    		return 1;
    	}
    	if(!Files.exists(messagesPath))
    		return 0;
    	
        List<Message> messages = ti.getMessages();
        
        
        StringBuffer sb = new StringBuffer("");
        try(BufferedReader in = Files.newBufferedReader(messagesPath, Charset.defaultCharset()))
        {
        	String line = in.readLine();
        	while(line != null)
        	{
        		sb.append(line);	
    	        line = in.readLine();
        	}

		    Vector< Vector <String> > messagesToAdd =  CsvMessageConverter.parseCsv(sb.toString());
		    for (Vector<String> messageFields : messagesToAdd)
		    {
		    	if (messageFields.size() == N_FIELDS_PER_LINE)
		    	{
	    	        String messageType = messageFields.elementAt(0);
	    	        String toolName    = messageFields.elementAt(1);
	             	String name        = messageFields.elementAt(2);    	            
	    	        String content     = messageFields.elementAt(3);
	    	        String severity    = messageFields.elementAt(4);
	    	            
	    	        Message message = new Message();
	    	            
	    	        message.setType(messageType);
	     	        message.setToolname(new Rhs(new StringValue(toolName)));
	    	        message.setName(new Rhs(new StringValue(name)));
	    	        message.setContent(new Rhs(new StringValue(content)));  
	    	        
	    	        try
	    	        {
	    	         	IntValue nSeverity = new IntValue(Integer.parseInt(severity));   
	    	           	message.setSeverity(new Rhs(nSeverity));
	    	        }
	    	        catch (Exception e)
	    	        {
	    	          	System.err.println("Error: nonnumeric message severity code in " + messagesPath.toString());   	            	
	    	   		}
	    	        
	    	        messages.add(message);
		    	}
		       	else 
		       		throw new Exception("Error: message does not have the required number of fields (" + N_FIELDS_PER_LINE + ")");
		       	        
	        }
        }
        catch (Exception e)
        {
        	System.err.println("...error reading " + messagesPath.toString());
        	if(e.getMessage() != null)
        		System.err.println(e.getMessage());
        	return 1;
        }
        return 0;
    }
}


