package com.simcyp.simcyp2PharmMLSO;

import java.util.TreeMap;


/**
 * Contains information for evaluating types of tables as recognized in Simcyp csv outputs (as expected from filenames or inferred from content) 
 * and cross referencing such information. 
 * @author Duncan Edwards
 */
public class TableXReferencer implements SimcypConstants
{	
	/**
	 * Behaviours for checking of table type inferred from content vs from expected filename  
	 */
	enum TableTypeCheckMode {INFER_WITH_WARNINGS, INFER_EXCLUDING_UNEXPECTEDS, USE_EXPECTED_ONLY};

    /**
     * Types of table recognized in Simcyp csv files (to aid application of generic processing patterns)
     */
    enum TableType {PROPERTY_TRIPLE,        //NOT PROCESSED: a placeholder for an alternative column-wise name-unit-value format, effectively a transpose of POPULATION_PARAMETR 
    	            POPULATION_PARAMETER,     
    	            INDIVIDUAL_PARAMETER, 
    	            POPULATION_PROFILE, 
    	            INDIVIDUAL_PROFILE, 
    	            INDIVIDUAL_PROFILE_INPUT, //subtype of INDIVIDUAL_PROFILE  but flagged for treatment as dosing inputs (as in Simcyp 'Other Site Dosing' profiles)  
    	            SCRIPT,                   //NOT PROCESSED: scripts are currently expected directly as lua files not in csv tables 
    	            ODE_STATE_INFO, 
    	            CUSTOM_DOSING, 
    	            OLD_INPUTS,               //NOT PROCESSED Obsolete table format used once for limited inputs transfer to Phoenix  
    	            //OTHER,                  
    	            UNSET};
	
    /**
     *  The checking mode set up for this instance of TableXReferencer    	            
     */
    TableTypeCheckMode tableTypeCheckMode;    
    
    /**
     *  Catalog of expected table type keyed on filename (or then filename- root).    	            
     */
    private TreeMap<String, TableType> tTypeExpected;
 
    /**
     * Indicates whether a full dictionary of filenames to table types has been loaded or just a the directly enumerable core set of names and name-roots. 
     */
    private boolean bFullDictionary;

    
    /**
     * Names which may occur as the constant root of a filename (with a variable keyword ending like Sub, Inh1) 
     */
    public final String[] CSV_NAME_ROOTS = {"CustomDosing", 
    		                                "Inputs_", 
    		                                "OtherSiteProfile",
    		                                "SACConc",
    		                                "AbsorptionS", 
    		                                "AbsorptionC", 
    		                                "BrainConc",
    		                                "BrainTransCLProfiles",
    		                                "AddOrganProf",
    		                                "PDProfiles"}; 
    
    
    /**
     * An array of expected names of Simcyp output csv files: NOT a complete listing It contains a core set  
     * of simple names and a few recognizable starting-roots for names which are completed by a variable part.
     * It is in a sense legacy code from the prototype SOConverter.
     * These names are in effect indexed by an integer from enum csvTables values. A fuller set of tablenames
     * including resolved variable patterns is constructed in the tTypeExpected dictionary  
     */
    String[] csvNames={
    	"Absorption",
	    "BrainPhysiology",
	    "ConcProfiles",
	    "Demographic",
	    "DistributionVols",
	    "DrugPopParams",
	    "EndoIgGStatus",
	    "Enzymatic",
	    "GITractPhysiology",
	    "Inputs_SoftwareVersions",
	    "Inputs_TrialDesign",
	    "KidneyPhysiology",
	    "OdeStateInfo",
	    "OtherSiteInputParameters",
	    "SkinParameters",
	    "SkinDrugParameters",
	    "TargetStatus",
	    "TransporterStatus",
	    "Turnover",
	    
	    "Demographic2",
	    "DistributionVols2",
	    "DistributionKps",
	    "DistributionKps2",
	    "Enzymatic2",
	    
	    "LiverConcProfiles",
	    "DrugPopParams2",
	    "PDCustomXtra",
	    "PDCustomIndivXtra",
	    "PDCustomODE",	   
	    
	    CSV_NAME_ROOTS[0],
	    CSV_NAME_ROOTS[1],
	    CSV_NAME_ROOTS[2],
        CSV_NAME_ROOTS[3],
	    CSV_NAME_ROOTS[4],
        CSV_NAME_ROOTS[5],
	    CSV_NAME_ROOTS[6],
	    CSV_NAME_ROOTS[7],
	    CSV_NAME_ROOTS[8],
	    CSV_NAME_ROOTS[9],   
    };
    
    /**
     * An enumeration of a core set of Simcyp csv output tables: NOT a complete listing. May be used to index 
     * corresponding name or name roots in csvNames.  
     */
    enum CsvTables {
    	ABSORPTION_PARAMS,
        BRAIN_PHYSIOLOGY,
	    CONC_PROFILES,
	    DEMOGRAPHIC,
	    DISTRIBUTION_VOLS,
	    DRUG_POP_PARAMS,
	    ENDO_IGG_STATUS,
	    ENZYMATIC,
	    GI_TRACT_PHYSIOLOGY,
	    INPUTS_SOFTWARE_VERSIONS,
	    INPUTS_TRIAL_DESIGN,
	    KIDNEY_PHYSIOLOGY,
	    ODE_STATE_INFO,
	    OTHER_SITE_INPUT_PARAMETERS,
	    SKIN_PARAMETERS,
	    SKIN_DRUG_PARAMETERS,
	    TARGET_STATUS,
	    TRANSPORTER_STATUS,
	    TURNOVER,	 
	    
	    DEMOGRAPHIC2,
	    DISTRIBUTIONVOLS2,
	    DISTRIBUTIONKPS,
	    DISTRIBUTIONKPS2,
	    ENZYMATIC2,
	    
	    LIVER_CONC_PROFILES,
	    DRUG_POP_PARAMS2,
	    PD_CUSTOM_XTRA,
	    PD_CUSTOM_INDIVXTRA,
	    PD_CUSTOM_ODE,
	    
	    //Csv name roots indexed below 
	    CUSTOM_DOSING,
	    INPUTS_,
	    OTHER_SITE_PROFILE,
	    SAC_CONC,
	    ABSORPTION_PROFILES,	    
	    ABSORPTION_PROFILES_SINGLE_COMPOUND,
	    BRAIN_CONC,
	    BRAIN_CL_PROFILES,
	    ADDORGAN_PROFILES,
	    PDPROFILES,
	};

	CsvTables FIRST_NAME_ROOT = CsvTables.CUSTOM_DOSING;
	CsvTables LAST_NAME_ROOT  = CsvTables.ADDORGAN_PROFILES;  	
    
    TableType[] expectedTypes = { 
	   	TableType.INDIVIDUAL_PARAMETER,	
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PROFILE,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.POPULATION_PARAMETER,
	   	TableType.POPULATION_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.ODE_STATE_INFO,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	    	
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	
	   	TableType.INDIVIDUAL_PROFILE,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.POPULATION_PARAMETER,
	   	TableType.INDIVIDUAL_PARAMETER,
	   	TableType.INDIVIDUAL_PROFILE,
	
	   	TableType.CUSTOM_DOSING,
	   	TableType.POPULATION_PARAMETER,   	
	   	TableType.INDIVIDUAL_PROFILE_INPUT, 
	   	TableType.INDIVIDUAL_PROFILE,
	   	TableType.INDIVIDUAL_PROFILE, 
	   	TableType.INDIVIDUAL_PROFILE, 
	   	TableType.INDIVIDUAL_PROFILE, 
	   	TableType.INDIVIDUAL_PROFILE, 
	   	TableType.INDIVIDUAL_PROFILE, 
	   	TableType.INDIVIDUAL_PROFILE, 
    };
    
    
    /**
     * Constructs TableXReferencer with just a core dictionary of csvNames and expected table types
     * @param tCheckMode
     */
    TableXReferencer(TableTypeCheckMode tCheckMode)
    {
	    tTypeExpected= new TreeMap<String, TableType>(String.CASE_INSENSITIVE_ORDER);
	    for (int i = 0; i < csvNames.length; i++)
	    {
	    	tTypeExpected.put(csvNames[i], expectedTypes[i]);	    	
	    }
	    tableTypeCheckMode = tCheckMode;
	    bFullDictionary = false;
    }
    
    
    /**
     * This builds an extended dictionary of csv table names to table type in which tables not already represented by individual name keys 
     * from csvNames  (an individual name or name-root) are added in full by evaluating the variable name parts.
     * So for example ${GISegmentNames}${CmpShortName} is expanded with each specific GI Segment name combined with each specific 
     * compound short name (spaces removed)
     */
    public void loadTableNameTypeMap()
    {
    	
    	if(tTypeExpected == null)
    	{
   	         tTypeExpected= new TreeMap<String, TableType>(String.CASE_INSENSITIVE_ORDER);
    	
   	         for (int i = 0; i <=  LAST_NAME_ROOT.ordinal(); i++)
   	         {
   	        	 tTypeExpected.put(csvNames[i], expectedTypes[i]);	    	
   	         }    	
    	}
   	    
    	
    	for(int nCmpd = 0; nCmpd < Compound.values().length; nCmpd++)
   	    {  
    		
    		//ADAM model GI tract compartments 
   	    	//${GISegmentName}Profiles${CmpdShortName}
            for(int nSeg = 0; nSeg < GISegment.values().length; nSeg++)
            {
            	String name = strGISegmentNames[nSeg] + "Profiles(" + strCompoundShortNames[nCmpd] +")";
            	name = name.replaceAll("\\s", "");
            	tTypeExpected.put(name, TableType.INDIVIDUAL_PROFILE); 
            }
        
            //${GISegmentNames}${CmpShortName} with/without (Perm-Ltd)
            for(int nSeg = 0; nSeg < Tissues.values().length; nSeg++)
            {
            	String name1 = strTissueNames[nSeg] + "Conc";
            	if(nSeg == Tissues.GUT.ordinal())
            		name1 = strTissueNames[nSeg] + "TissueConc";
   
            	if(nCmpd != SimcypConstants.Compound.SUB.ordinal())  //a "feature": substrate is blank for Simcyp V15 
            	    name1 = name1 + strCompoundShortNames[nCmpd];
            	name1 = name1.replaceAll("\\s", "");              	
            	tTypeExpected.put(name1, TableType.INDIVIDUAL_PROFILE); 
            	
            	String name2 = strTissueNames[nSeg] + "Prof." +  strCompoundShortNames[nCmpd];
            	name2 = name2.replaceAll("\\s", "");           	
            	tTypeExpected.put(name2 + "(Perm-Ltd)", TableType.INDIVIDUAL_PROFILE); 
            }
            
            //${CmpdShortName}OtherSiteProfile.csv   	    
            String name = strCompoundShortNames[nCmpd] + "OtherSiteProfile";
            name = name.replaceAll("\\s", "");
        	tTypeExpected.put(name, TableType.INDIVIDUAL_PROFILE); 
   	    }
    	
    	bFullDictionary = true;
  
    }
    
    /**
     * Run this main to list expected tables, command line flag -L limits the by removing disallowed combinations of variables
     * @param args
     */
    public static void main(String args[])
    {
   		TableXReferencer tXR = new TableXReferencer(TableTypeCheckMode.USE_EXPECTED_ONLY);
    	if(args.length > 0 && args[0].equalsIgnoreCase("-L") )
    		tXR.listExpectedTables(true);
    	else
    		tXR.listExpectedTables(false);
    }
    
    
    /**
     * Prints out a list of individual tables expected, expanding all combinations of variables or, 
     * if bLimitCombinationsToSupport, tries to select only those combinations which are currently allowed by Simcyp.
     * @param bLimitCombinationsToSupport
     */
    public void listExpectedTables(boolean bLimitCombinationsToSupport)
    {
           	
       	for (int i = 0; i <  FIRST_NAME_ROOT.ordinal(); i++)
       	{
       	  	 System.out.println(csvNames[i]);	    	
       	}      	    
        
        //CAT model outputs - multiple compounds on one sheet
   	    //AbsorptionStomach.csv
   	    //AbsorptionComp{n}.csv
   	    System.out.println("AbsorptionStomach");
	    final int FIRST_CAT_GUTCMP = 1;
	    final int LAST_CAT_GUTCMP = 7;
   	    for (int i = FIRST_CAT_GUTCMP; i <= LAST_CAT_GUTCMP ; i++)
   	    {
   	    	System.out.println("AbsorptionComp" + i);
   	    }
       	
       	for(int nCmpd = 0; nCmpd < Compound.values().length; nCmpd++)
       	{  
        	//ADAM model GI tract compartments     
            //${CmpdShortName}OtherSiteProfile.csv   	    
            String sCmpName = strCompoundShortNames[nCmpd];
            sCmpName = sCmpName.replaceAll("\\s", "");
    
            //CustomDosing$CmpShortName}
            //Inputs_${CmpdShortName}.csv  
            //OtherSiteProfile$CmpShortName}
            //SACConc($CmpShortName})
            //BrainConc(${CmpdShortName})
            //BrainTransCLProfiles${CmpdShortName}.csv
   		    //PdProfiles(${CmpdShortName})
            System.out.println("CustomDosing" + sCmpName);  
            System.out.println("Inputs_" + sCmpName);
            System.out.println("OtherSiteProfile"+ sCmpName);  
            System.out.println("SACConc(" + sCmpName  + ")");
           	System.out.println("BrainConc(" + sCmpName  + ")");
            System.out.println("BrainTransCLProfiles" + sCmpName);  
            System.out.println("PDProfiles(" + sCmpName  + ")");
 
   	    	if(!bLimitCombinationsToSupport || (nCmpd <= LAST_INDEX_DOSED_CMPD))
   	    	{
   	    		//CAT model outputs - single compound named on sheet
   	    		//AbsorptionStomach{CmpdShortName}.csv
   	    		//AbsorptionSeg{n}${CmpdShortName}.csv
   	    		System.out.println("AbsorptionStomach(" + sCmpName + ")");
   	    		for (int i = FIRST_CAT_GUTCMP; i <= LAST_CAT_GUTCMP; i++)
   	    		{
   	    			System.out.println("AbsorptionSeg" + i + "(" + sCmpName + ")");
   	    		}
   	    	}
   	    	
   	   	    if(!bLimitCombinationsToSupport || (nCmpd == Compound.SUB.ordinal() || nCmpd == Compound.INH1.ordinal())) 
   	   	   	{
	   	    	//ADAM model GI tract compartments 
	   	    	//${GISegmentName}Profiles${CmpdShortName}
	            for(int nSeg = 0; nSeg < GISegment.values().length; nSeg++)
	            {
	                String name = strGISegmentNames[nSeg] + "Profiles(" + strCompoundShortNames[nCmpd] +")";
	                name = name.replaceAll("\\s", "");
	                System.out.println(name);
	            }
   	   	   	}
   	   	    
   	   	    //${TissueName}}${TissueLabel}${CmpShortname}Conc.csv     
   	   	    //${TissueName}Prof.${CmpShortName}(Perm-Ltd).csv
   	   	    //including: AddOrganProf.${CmpShortName}(Perm-Ltd).csv
            for(int nSeg = 0; nSeg < Tissues.values().length; nSeg++)
            {
            	String name1 = strTissueNames[nSeg] + "Conc";
            	if(nSeg == Tissues.GUT.ordinal())
            		name1 = strTissueNames[nSeg] + "TissueConc";

            	if(nCmpd != SimcypConstants.Compound.SUB.ordinal())  //a "feature": substrate is blank for Simcyp V15 
                     name1 = name1 + strCompoundShortNames[nCmpd];
                name1 = name1.replaceAll("\\s", "");   
                System.out.println(name1);
                if(!bLimitCombinationsToSupport || nSeg == Tissues.ADIPOSE.ordinal() || nSeg == Tissues.BONE.ordinal()
                		                        || nSeg == Tissues.HEART.ordinal()   || nSeg == Tissues.ADDITIONAL_ORGAN.ordinal()
                		                        || nSeg == Tissues.MUSCLE.ordinal()  || nSeg == Tissues.PANCREAS.ordinal()
                	                            || nSeg == Tissues.SKIN.ordinal()    || nSeg == Tissues.SPLEEN.ordinal()) 
                {
                	String name2 = strTissueNames[nSeg] + "Prof." + strCompoundShortNames[nCmpd];
                	name2 = name2.replaceAll("\\s", "");           	
                	System.out.println(name2 + "(Perm-Ltd)"); 
                }
            }
   	    }
    }

    
    /**
     * Looks up table type based on supplied file name (without extension); if not found tries 
     * to find a root in csvNameRoots witch matches the start of the name and uses that. So for example
     * a file name of "CustomDosingSub" will find the look up the table type of "CustomDosing".
     * If still no name match, it will ensure complex variable name ppatterns are added to the map
     * and try again for an exact name match (case-insensitive)  
     *  
     * @param fNameOnly  - file name without csv extension
     * @return expected  - table type 
     */
    public TableType getExpectedTableType(String fNameOnly)
    {
    	try
    	{
    		TableType t = tTypeExpected.get(fNameOnly);
    		if(t == null)
    		{ 
    			for(String root : CSV_NAME_ROOTS)
    			{
    				if(fNameOnly.startsWith(root))
    					t = tTypeExpected.get(root);   						
    			}
    		}
    			
    		if(t == null && bFullDictionary == false)
    		{
    			//we were too lazy
    			loadTableNameTypeMap();
    			t = tTypeExpected.get(fNameOnly);
    		}
    			
    		return t;
    	}
    	catch (Exception e)  
    	{
    		return null;
    	}
    } 	  
 
}
	

