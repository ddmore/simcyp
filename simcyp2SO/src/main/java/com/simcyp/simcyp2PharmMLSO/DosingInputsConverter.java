/**
 * Provides facilities for splicing in inputs from various Simcyp tables into a SO dosing table event record format  
 */
package com.simcyp.simcyp2PharmMLSO;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import com.simcyp.simcyp2PharmMLSO.SimcypConstants.Compound;
import com.simcyp.simcyp2PharmMLSO.SimcypConstants.InputSiteRoute;
import com.simcyp.simcyp2PharmMLSO.SimcypConstants.NonValues;

import eu.ddmore.libpharmml.dom.commontypes.AnnotationType;
import eu.ddmore.libpharmml.dom.commontypes.SymbolType;
import eu.ddmore.libpharmml.dom.dataset.ColumnDefinition;
import eu.ddmore.libpharmml.dom.dataset.ColumnType;
import eu.ddmore.libpharmml.dom.dataset.DatasetRow;
import eu.ddmore.libpharmml.dom.dataset.HeaderColumnsDefinition;
import eu.ddmore.libpharmml.so.dom.SOBlock;
import eu.ddmore.libpharmml.so.dom.SimulationItem;

/**
 * This class is the delegate for things concerned with generating a SO dosing table (which implies certain NONMEM/NMTRAN/PREDPP dialects with a nod to Monolix/MlxTran)
 * by piecing together information from various Simcyp output tables as and when they happen to be output according to the user-chosen model. 
 * @author Duncan Edwards
 */
public class DosingInputsConverter  implements DosingInputsFishingList{
	

    Set<String> dosingFishingSetTD    = new TreeSet<String>(Arrays.asList(dosingFishingListTD)); 
    Set<String> dosingFishingSetTD2   = new TreeSet<String>(Arrays.asList(dosingFishingListTD2));  
    Set<String> dosingFishingSetCmpd  = new TreeSet<String>(Arrays.asList(dosingFishingListCmpd)); 
	
	/*
	 * Maps Simcyp route/site labels to a corresponding numeric (enumeration) code 
	 */
	private java.util.HashMap<String, InputSiteRoute> inputSiteRouteIndex;

    
	/**
	 * Structure for carrying through row contents through an ordering process without losing annotations, and supporting  
	 * distinction of similar rows by a distinct key   
	 */
	private class RowInfo
	{
		RowInfo(Vector<String> contents, String annotation, int key)
		{
			this.contents   = contents;
			this.annotation = annotation;
			this.key        = key;
		}
		
		Vector<String> contents;
		String         annotation;

		int getKey() {return key;}
		private int  key;
	}
	
	/**
	 * Factory for creating  RowInfo with distinct key   
	 */
	private class RowInfoFactory
	{
		RowInfoFactory(int startKey)
		{
			nextKey = startKey;
		}
		
		/**
		 * Creates a new row info with contents annotation and an integer key which increments by one on each call
		 * @param contents
		 * @param annotation
		 * @return reference to the created row info object
		 */
		RowInfo createRowInfo(Vector<String> contents, String annotation)
		{
			return new RowInfo(contents, annotation, nextKey++);
		}
		
		private int nextKey;
	}
	

	
	/**
	 * Creates a map of site/route names to numerical index
	 */
	private void indexInputSiteRoutes()
	{		
		inputSiteRouteIndex = new java.util.HashMap<String, InputSiteRoute>();
		assert(SimcypConstants.strInputRoutes.length == InputSiteRoute.values().length);
		for (int i=0; i < SimcypConstants.strInputRoutes.length; i++)
		{			
			inputSiteRouteIndex.put(SimcypConstants.strInputRoutes[i], SimcypConstants.inputRoutes[i]);
		}
		for (int i=0; i < SimcypConstants.strInputRoutes2.length; i++)
		{	
			inputSiteRouteIndex.put(SimcypConstants.strInputRoutes2[i], SimcypConstants.inputRoutes2[i]);			
		}
		for (int i=0; i < SimcypConstants.strSkinLocations.length; i++)
		{	
			inputSiteRouteIndex.put(SimcypConstants.strSkinLocations[i], SimcypConstants.skinLocations[i]);			
		}
	}	
	
	/**
	 * Calculates a numerical admin compartment code which will be distinct for any combination of 
	 * compound, siteRoute, and different too to flag a depot compartment linked feeding in.   
	 * @param cmpd
	 * @param isDepot
	 * @param siteRoute
	 * @return ADM code for use in SO dosing table
	 */
	private int calculateADMCode(Compound cmpd, InputSiteRoute siteRoute, boolean isDepot)
	{
		final int ADM_SITE_SHIFT  = Compound.values().length;		
		final int ADM_DEPOT_SHIFT = Compound.values().length * InputSiteRoute.values().length;		
		return cmpd.ordinal() + ADM_SITE_SHIFT* siteRoute.ordinal() + ADM_DEPOT_SHIFT * (isDepot? 1: 2);
	}

	
	/**
     * A facility to help sort rows based on first four columns as RowInfo content, all collisions are then ordered by RowInfo.key interpreted as 
     *  0. int      typically   index/id
     *  1. double   typically   time
     *  2. String   typically   compound-type id based on short name
     *  3. int      typically   identifier for dosing csv table from which record is derived         
     * (typically for numeric index/id, compound name and time). Currently fails with exception if the number parsing fails. 
     * A more efficient approach can be coded later if necessary
     */
	private class DosingRowComparator implements java.util.Comparator<RowInfo> 
	{
		public int compare(RowInfo first, RowInfo second) throws NullPointerException, NumberFormatException
		{
			String keyF0 = first.contents.elementAt(0);
			String keyF1 = first.contents.elementAt(1);
			String keyF2 = first.contents.elementAt(2);
			String keyF3 = first.contents.elementAt(3);
			String keyS0 = second.contents.elementAt(0);
			String keyS1 = second.contents.elementAt(1);
			String keyS2 = second.contents.elementAt(2);
			String keyS3 = second.contents.elementAt(3);
			
			int intF0 = Integer.parseInt(keyF0);
			int intS0 = Integer.parseInt(keyS0);
			if(intF0 < intS0)
				return -1;
			else if (intF0 == intS0)
    		{				
				double dF1 = Double.parseDouble(keyF1);
    			double dS1 = Double.parseDouble(keyS1);
				if(dF1 < dS1)
				{
					return -1;
				}
				else if(dF1 == dS1) 
				{	
					int nComp = keyF2.compareToIgnoreCase(keyS2);
	    			if(nComp < 0)
	    				return -1; 
	    			else if (nComp == 0) 
	    			{
	    				int intF3 = Integer.parseInt(keyF3);
	    				int intS3 = Integer.parseInt(keyS3);
	    			    if(intF3 < intS3)
	    				   return -1;
	    				else if(intF3 == intS3)
	    				{
	    					if(first.key < second.key)
	    						return -1;
	    					else if (first.getKey() == second.getKey())
	    						return 0;
	    					else 
	    						return 1;
	    				}	    					
	    				else	    				
	    				   return 1;
	    			}
	    			else
	    				return 1;
				}
				else
					return 1;
    		}
			else
				return 1;   				
		}
	};
 
	/**
	 * Constructor sets up index of input site routes
	 */
	DosingInputsConverter()
	{
		indexInputSiteRoutes();
	}
	
	/**
     *  Sets up the basic column definitions for the SO NONMEM-like dosing table. Other columns may be added afterwards? 
     *  Code currently based on assumption of one (empty) dosing table with columns to be filled in (if that assumption fails, it creates a new table)  
     */    
    boolean createDosingTableTemplate(SOBlock sob)
    {
    	List<SimulationItem> dosingTableList = sob.getSimulation().getListOfDosing();
    	SimulationItem dosingTable = null;
    	if(dosingTableList.size() != 1)
    	    dosingTableList.add(sob.getSimulation().createDosing());
    	dosingTable = dosingTableList.get(dosingTableList.size() - 1);
    	
    	HeaderColumnsDefinition	dosingHeaders = dosingTable.getDefinition();
    	if(dosingHeaders == null)
            dosingHeaders = dosingTable.createDefinition();   	
    	dosingHeaders.createColumnDefinition("ID",          ColumnType.ID,          SymbolType.INT,     1);  
    	dosingHeaders.createColumnDefinition("TIME",        ColumnType.TIME,        SymbolType.REAL,    2); 
       	ColumnDefinition cmpdColumn = dosingHeaders.createColumnDefinition("CMPD",        ColumnType.UNDEFINED,   SymbolType.STRING,  3); //Simcyp compound role      	
        ColumnDefinition srcColumn  = dosingHeaders.createColumnDefinition("SOURCE",      ColumnType.UNDEFINED,   SymbolType.INT,     4); //Simcyp table source table type      	
        							  dosingHeaders.createColumnDefinition("AMT",         ColumnType.DOSE,        SymbolType.REAL,    5);            	
        							  dosingHeaders.createColumnDefinition("DURATION",    ColumnType.DURATION,    SymbolType.REAL,    6);          	
        ColumnDefinition admColumn  = dosingHeaders.createColumnDefinition("ADM",         ColumnType.ADM,         SymbolType.INT,     7); //numerical compartment encoding of CPD, SITE_ROUTE and depot or direct
        ColumnDefinition siteColumn = dosingHeaders.createColumnDefinition("SITE_ROUTE",  ColumnType.UNDEFINED,   SymbolType.STRING,  8); //Simcyp administration site/route
        
    	AnnotationType at = new AnnotationType();      
      	at.setValue("Simcyp compound role");
      	cmpdColumn.setDescription(at);
   
      	at = new AnnotationType();
      	at.setValue("Simcyp source csv table code");
      	srcColumn.setDescription(at); 
      	
    	at = new AnnotationType();
    	at.setValue("Numerical compartment encoding of CPD, SITE_ROUTE and depot/direct");
      	admColumn.setDescription(at);
      	
      	at = new AnnotationType();
      	at.setValue("Simcyp administration site/route name");
      	siteColumn.setDescription(at);  
      	
       	//dosingTable.createColumnDefinition("SS",          ColumnType.SS,          SymbolType.REAL, 6);   
       	//dosingTable.createColumnDefinition("II",          ColumnType.II,          SymbolType.REAL, 9);     	
       	//dosingTable.createColumnDefinition("SS_END_TIME", ColumnType.SS_END_TIME, SymbolType.REAL, 7);        	
       	//dosingTable.createColumnDefinition("SS_PERIOD",   ColumnType.SS_PERIOD,   SymbolType.REAL, 8);      	

       	//dosingTable.createColumnDefinition("DoseUnit",    ColumnType.UNDEFINED,   SymbolType.REAL, 11);           	
    	//dosingTable.createColumnDefinition("EVID",  ColumnType.EVID,        SymbolType.REAL, 12);  //not sure we need this	
       	//dosingTable.createColumnDefinition("MDV",       ColumnType.MDV,         SymbolType.REAL, 13);           	
       	
       	return true;
    }
    
	
	 /**
      * Fills in the values of the dosing table. Currently this involves splicing together dosing information from the 
      * Inputs_TrialDesign "<Cmpd> : <DoseInfromation>" columns and the CustomDosing<Cmpd> tables, with dose amounts scaled for any 
      * body-weight or surface area dependency using individual body weight and BSA from the Demographic table.    
      * @param sob
     */
    void markDosingTableFromMultipleMappings(SOBlock sob, TableXReferencer xReft,
    		                                 TreeMap<String, TreeMap<String, Integer> > candidateColumns,    
                                             TreeMap<String, TreeMap<Integer, Vector<String> > > candidateValues,
                                             TreeMap<String, TreeMap<Integer, UnitType> > candidateUnits)
    {
        
    	/*
            Pro memoria 
         
            //FROM INPUTS_TRIAL_DESIGN
            Fasted/Fed,
            Fluid intake with dose,
            Fluid intake with dose CV (%) 
            Start Day/Time,
            End Day/Time,
            Study Duration,
            Start at Gestational Week           
            Sub : Route,
            Sub : Dose,
            Sub : Dosing,              (dosing Type me Single Dose..
            Sub : Dose Interval,
            Sub : NoDoses,
            Sub : Bolus Dose,           (bolus dosing type {Single, Multiple} 
            Sub : Bolus administration duration, 
            Sub : Infusion Dose,
            Sub : Infusion Duration,   
            Sub : Thickness of Applied Formulation Layer,
			Sub	: Area of Application,
			Sub	: Volume Applied Solution,
			Sub	: Place of Application,
			Sub : Inhaled (%)
			Sub : Swallowed (%)
            Inh 1 : Route,
            Inh 1 : Dose,
            Inh 1 : Dosing,
            Inh 1 : Dose Interval,
            Inh 1 : NoDoses
            ...  as for Sub but with different compounds 		"Sub" "Inh 1" "Inh 2" "Inh 3" "Sub Pri Met1" "Sub Pri Met2" "Sub Sec Met" "Inh 1 Met";
            
            //Note there is also regeneration frequency which might affect changes in BWT and BSA and so influence dose in mg
            //Consider this complication later  

            FROM Inputs_<cmpdShortName> eg Inputs_Sub
            .. Other site dosing has  Input Compartment not Route and absolute dose given by profile % max times the Dose from INPUTS_TRIAL_DESIGN 
            Input Compartment,   ("Blood"",Venous Blood","CSF","Spinal CSF","Stomach","Colon","Additional organ","Subcutaneous")
            Input Model,              (Bolus, Zero Order, First Order, Profile)
			Interpolation Method,
			
			- set points encoded here - but individualised smoother onuts are in OtherSiteProfile 
			Profile Value @0.0 hours
			Profile CV @0.0 hours
			Profile Value @0.1 hours
			Profile CV @0.1 hours
			Profile Value @1.2 hours
			..for other-site first order 
			Lag time
			Lag time CV (%)
			fa
			fa CV (%)
			ka
			ka CV (%)                       
			...for othersite  zero order
		    Input Duration 
                          NOTE!: Currently individual parameter values for FO here are not reported  - for interim treat as mean and annotate 0 CV?.
		                         Ideally neede extra sheet for this - like simplified absoprtion sheet but available whenever this doing used.
            ...for more classic admin routes, there is much overlap with 
            Route,                     (oral,iv,infusion,dermal inhaled)
            Dose Units
            Dose
            Sub : Dosing,              (dosing Type me Single Dose..
            Sub : Dose Interval,
            Sub : NoDoses,
            Bolus Dose 
            Infusion Dose,
            Infusion Duration,   

			Amount of Dose Swallowed (%)
			Amount of Dose Inhaled (%)
			Sub : Swallowed (%)
			Thickness of Applied Formulation Layer
	        Area of application
		    Volume Applied Solution
		    Place of application

            //FROM CUSTOM DOSING where there is one table for each file CustomDosing<cmpdShortName> eg CustomDosingSub
            Dose Number
            Day
            Time hh:mm
            Offset
            Dose
            Dose Unit
            Route of Administration
            Duration
            
            //FROM OtherSiteProfile<Compound>
            (Index)
            (Trial)
            Time
            Cumulative Input (%)      
            
            
            FROM DrugPopParams there is also (when output)
            
            Dose (Sub)	             //this is the first dose scaled (where needed from /kg or /m2) to mg (but for basic infusion uses total dose so infusion with any loading bolus) 
            Dose (Inh 1)	
            Dose (Inh 1 Met)	
            Tau (Sub)	
            Tau (Inh 1)	
            Tau (Inh 1 Met)	
            Time (Sub	
            Time (Inh 1	
            Time (Inh 1 Met            
           
            HOWEVER THAT IS NOT VERY USEFUL ...MIGHT AS WELL GET BSA AND BWT AND SCALE EACH DOSE 
             

            Need table with ID, TIME, AMT, TINF, TYPE/ADM, SS, II etc
        */
        
    	//Expect the Inputs_TrialDesign and Custom Dosing, OtherSiteProfile  and Inputs_<Cmpd>  to be loaded as candidate Columns 
    	//with candidate Values
    	
    	///////////////////////////////////////////////////////////////////////////////////////////////////////////////-
    	// Get table info (column ids and values) from loaded candidates for markup for each dosing source table in turn    	
    	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    	
    	
    	//------------------------------------------------------------------------------------------------------------------
    	//  get BWT, BSA (for dose-normalisation) and list of ids  (used at least to confirm ow many subjects in table )    

		Integer        nColWeight     = null;
		Integer        nColBSA        = null;
		Integer        nColDemogIndex = null;
		Vector<String> valsBSA        = null;
		Vector<String> valsBWT        = null;	
		Vector<String> ids            = null;
		
	    TreeMap<String, Integer> demogCols            = candidateColumns.get(xReft.csvNames[TableXReferencer.CsvTables.DEMOGRAPHIC.ordinal()]); //tentative attempt to generalize references to table names ...a bit messsy
	    TreeMap<Integer, Vector<String> > demogValues = candidateValues.get(xReft.csvNames[TableXReferencer.CsvTables.DEMOGRAPHIC.ordinal()]);
    	if(demogCols != null && demogValues != null)
    	{
    		nColWeight      = demogCols.get("Weight");
    		nColBSA         = demogCols.get("BSA");  
    		nColDemogIndex  = demogCols.get("Index"); 
	    	valsBWT         = demogValues.get(nColWeight);
	    	valsBSA         = demogValues.get(nColBSA);
	    	ids             = demogValues.get(nColDemogIndex);
    	}
    	
    	if(ids == null)
    		return;
   	
    	//------------------------------------------------------------------------------------------------------------------
    	// get compound-dependent dosing info from CustomDosing, Inputs_TrialDesign, Inputs_<Cmpd> OtherSiteProfile<Cmpd>   
    	
    	//columns of interest from CustomDosing 
    	Integer nColDoseCustom       =-1;
    	Integer nColTimeCustom       =-1;
    	Integer nColDoseUnitCustom   =-1;
    	Integer nColDurationCustom   =-1; 
    	Integer nColRouteCustom      =-1;
    	
    	//columns of interest from Other Site Input profile <Cmpd>
       	Integer nColIndexOSite       = -1;
    	Integer nColTimeOSite        = -1;
    	Integer nColCumInputPctOSite = -1;
    	
    	//columns of interest in Inputs_TrialDesign, indexed on compound 
    	final int DOSED_COMPOUNDS_SIZE = SimcypConstants.LAST_INDEX_DOSED_CMPD + 1;
    	Integer[] nColRouteTD                  = new Integer[DOSED_COMPOUNDS_SIZE];
    	Integer[] nColDoseTD                   = new Integer[DOSED_COMPOUNDS_SIZE];
    	Integer[] nColDosingRegimenTD          = new Integer[DOSED_COMPOUNDS_SIZE];
    	Integer[] nColDoseIntervalTD           = new Integer[DOSED_COMPOUNDS_SIZE];  	    	    	    	
    	Integer[] nColNoDosesTD                = new Integer[DOSED_COMPOUNDS_SIZE];   
    	Integer[] nColBolusDoseTypeTD          = new Integer[DOSED_COMPOUNDS_SIZE];    	    	
    	Integer[] nColBolusDurationTD          = new Integer[DOSED_COMPOUNDS_SIZE];
    	Integer[] nColInfusionDoseTD           = new Integer[DOSED_COMPOUNDS_SIZE];
    	Integer[] nColInfusionDurationTD       = new Integer[DOSED_COMPOUNDS_SIZE];
    	Integer[] nColDermalAppliedThicknessTD = new Integer[DOSED_COMPOUNDS_SIZE];
    	Integer[] nColDermalAppliedAreaTD      = new Integer[DOSED_COMPOUNDS_SIZE];  
    	Integer[] nColDermalAppliedVolumeTD    = new Integer[DOSED_COMPOUNDS_SIZE];  
    	Integer[] nColDermalAppliedPlaceTD     = new Integer[DOSED_COMPOUNDS_SIZE];    
    	Integer[] nColPctInhaledTD             = new Integer[DOSED_COMPOUNDS_SIZE];           
    	Integer[] nColPctSwallowedTD           = new Integer[DOSED_COMPOUNDS_SIZE];  
    	
    	
    	//columns of interest in Inputs_<Cmpd> 
    	Integer[] nColInputCompartment         = new Integer[DOSED_COMPOUNDS_SIZE]; 
    	Integer[] nColInputModel               = new Integer[DOSED_COMPOUNDS_SIZE]; 
    	Integer[] nColInputInterpolMethod      = new Integer[DOSED_COMPOUNDS_SIZE]; 
    	Integer[] nColInputZODuration          = new Integer[DOSED_COMPOUNDS_SIZE]; 
    	//the FO input values are not yet available as individual parameters, so use mean with CV annotation for now
    	Integer[] nColInput_tLagMean           = new Integer[DOSED_COMPOUNDS_SIZE];
    	Integer[] nColInput_tLagCV             = new Integer[DOSED_COMPOUNDS_SIZE];
    	Integer[] nColInput_faMean             = new Integer[DOSED_COMPOUNDS_SIZE];
    	Integer[] nColInput_faCV               = new Integer[DOSED_COMPOUNDS_SIZE];
    	Integer[] nColInput_kaMean             = new Integer[DOSED_COMPOUNDS_SIZE];
    	Integer[] nColInput_kaCV               = new Integer[DOSED_COMPOUNDS_SIZE]; 
    	
    	//values and units for the columns
    	Vector<TreeMap<Integer, Vector<String> > >  valsTDCmpd                = new Vector<TreeMap<Integer, Vector<String> > >(DOSED_COMPOUNDS_SIZE);  
    	Vector<TreeMap<Integer, UnitType> >         unitsTDCmpd               = new Vector<TreeMap<Integer, UnitType>  >      (DOSED_COMPOUNDS_SIZE);   	
    	Vector<TreeMap<Integer, Vector<String> > >  valsCustomDosingCmpd      = new Vector<TreeMap<Integer, Vector<String> > >(DOSED_COMPOUNDS_SIZE);  	
    	Vector<TreeMap<Integer, Vector<String> > >  valsOtherSiteProfileCmpd  = new Vector<TreeMap<Integer, Vector<String> > >(DOSED_COMPOUNDS_SIZE);
    	Vector<TreeMap<Integer, UnitType> >         unitsOtherSiteProfileCmpd = new Vector<TreeMap<Integer, UnitType>  >      (DOSED_COMPOUNDS_SIZE);
       	Vector<TreeMap<Integer, Vector<String> > >  valsInputsCmpd            = new Vector<TreeMap<Integer, Vector<String> > >(DOSED_COMPOUNDS_SIZE);
    	Vector<TreeMap<Integer, UnitType> >         unitsInputsCmpd           = new Vector<TreeMap<Integer, UnitType>  >      (DOSED_COMPOUNDS_SIZE);
    	valsTDCmpd.setSize                    (DOSED_COMPOUNDS_SIZE);  
    	unitsTDCmpd.setSize                   (DOSED_COMPOUNDS_SIZE);     	
    	valsCustomDosingCmpd.setSize          (DOSED_COMPOUNDS_SIZE);
    	valsOtherSiteProfileCmpd.setSize      (DOSED_COMPOUNDS_SIZE);
    	unitsOtherSiteProfileCmpd.setSize     (DOSED_COMPOUNDS_SIZE); 
    	valsInputsCmpd.setSize                (DOSED_COMPOUNDS_SIZE);
    	unitsInputsCmpd.setSize               (DOSED_COMPOUNDS_SIZE);
    	
    	for(int nCmpd = 0; nCmpd < SimcypConstants.LAST_INDEX_DOSED_CMPD; nCmpd++)
    	{
    	    String cmpd = SimcypConstants.strCompoundShortNames[nCmpd]; 
    		
    		//look up the column numbers and pull through values (and units if any)
    		//..for CustomDosing  (Cpmd specific table)
    		String customDosingCmpd = "CustomDosing" + cmpd;
    		TreeMap<String, Integer> colsCustomDosingCmpdCurrent = candidateColumns.get(customDosingCmpd);
    	    if(colsCustomDosingCmpdCurrent != null)
    	    {    	    	
    	    	nColDoseCustom     = colsCustomDosingCmpdCurrent.get("Dose");
    	    	nColTimeCustom     = colsCustomDosingCmpdCurrent.get("Offset");
    	    	nColDoseUnitCustom = colsCustomDosingCmpdCurrent.get("Dose Units");
    	    	nColDurationCustom = colsCustomDosingCmpdCurrent.get("Duration");
    	    	nColRouteCustom    = colsCustomDosingCmpdCurrent.get("Route of Administration");
    	    	
    	    	valsCustomDosingCmpd.set(nCmpd, candidateValues.get(customDosingCmpd));
    	    }
    	    
    	    //...for OtherSiteProfile  (Cmpd-specific table, also subject-specific) 
    		String otherSiteInputProfileCmpd = "OtherSiteProfile" + cmpd;    	    
    	    TreeMap<String, Integer> colsOSiteInputProfile    = candidateColumns.get(otherSiteInputProfileCmpd);  
    	    if(colsOSiteInputProfile != null)
    	    {
    	         nColIndexOSite       = colsOSiteInputProfile.get("Index");   	
    	         //nColTrial          = colsOSiteInputProfile.get("Trial");
    	         nColTimeOSite        = colsOSiteInputProfile.get("Time");
    	         nColCumInputPctOSite = colsOSiteInputProfile.get("Cumulative Input (%)");
    	      
     	    	 valsOtherSiteProfileCmpd.set(nCmpd, candidateValues.get(otherSiteInputProfileCmpd));
     	    	 unitsOtherSiteProfileCmpd.set(nCmpd, candidateUnits.get(otherSiteInputProfileCmpd));
    	    }

    	    //...for Inputs<Cmpd>  Compound specific table 
    		String inputs = "Inputs_" + cmpd;    	    
    	    TreeMap<String, Integer> colsInputs    = candidateColumns.get(inputs);  
    	    if(colsInputs != null)
    	    {
     	    	nColInputCompartment[nCmpd]          = colsInputs.get("Input Compartment");
    	    	nColInputModel[nCmpd]                = colsInputs.get("Input Model");
    	    	nColInputZODuration[nCmpd]           = colsInputs.get("Input Duration");
     	    	nColInputCompartment[nCmpd]          = colsInputs.get("Input Compartment");  
     	    	nColInputInterpolMethod[nCmpd]       = colsInputs.get("Interpolation Method");
     	    	nColInput_tLagMean[nCmpd]            = colsInputs.get("Lag time");  
     	    	nColInput_tLagCV[nCmpd]              = colsInputs.get("Lag time CV (%)");  
     	    	nColInput_faMean[nCmpd]              = colsInputs.get("fa");  
     	    	nColInput_faCV[nCmpd]                = colsInputs.get("fa CV (%)");
     	    	nColInput_kaMean[nCmpd]              = colsInputs.get("ka");  
     	    	nColInput_kaCV[nCmpd]                = colsInputs.get("ka CV (%)");
    	      
     	    	valsInputsCmpd.set(nCmpd, candidateValues.get(inputs));
     	    	unitsInputsCmpd.set(nCmpd, candidateUnits.get(inputs));
    	    }

    	    //------------------------------------------------------------------------------------------------------------------
    	    //... and for trial design (Cmpd identified within column header string), 
    	    //    reorganise into maps indexed by compound id  
    	    TreeMap<String, Integer> colsTD = candidateColumns.get("Inputs_TrialDesign"); 
    	    if(colsTD != null)
    	    {
    	    	String cmpdRoute                    = cmpd + " : Route";     	    	
    	    	String cmpdDose                     = cmpd + " : Dose";
    	    	String cmpdDosingRegimen            = cmpd + " : Dosing"; //Regimen
    	    	String cmpdDoseInterval             = cmpd + " : Dose Interval";
    	    	String cmpdNoDoses                  = cmpd + " : NoDoses";  	 
    	    	String cmpdBolusDoseType            = cmpd + " : Bolus Dose"; 
    	    	String cmpdBolusDuration            = cmpd + " : Bolus administration duration";   //(make title case?)
    	    	String cmpdInfusionDose             = cmpd + " : Infusion Dose"; 
    	    	String cmpdInfusionDuration         = cmpd + " : Infusion Duration";   //(make title case?)  	
    	    	String cmpdDermalAppliedThickness   = cmpd + " : Thickness of Applied Formulation Layer";
    	    	String cmpdDermalAppliedArea        = cmpd + " : Area of Application";
    	    	String cmpdDermalAppliedVolume      = cmpd + " : Volume Applied Solution";
    	    	String cmpdDermalAppliedPlace       = cmpd + " : Place of Application";
    	    	String cmpdPctInhaled               = cmpd + " : Inhaled (%)";
    	    	String cmpdPctSwallowed             = cmpd + " : Swallowed (%)";
    	    	
    	    	nColRouteTD[nCmpd]                   = colsTD.get(cmpdRoute);    	    	    	    	
    	    	nColDoseTD[nCmpd]                    = colsTD.get(cmpdDose);
    	    	nColDosingRegimenTD[nCmpd]           = colsTD.get(cmpdDosingRegimen);	
    	    	nColDoseIntervalTD[nCmpd]            = colsTD.get(cmpdDoseInterval);    	    	    	    	
    	    	nColNoDosesTD[nCmpd]                 = colsTD.get(cmpdNoDoses);
    	    	nColBolusDoseTypeTD[nCmpd]           = colsTD.get(cmpdBolusDoseType);    	    	
    	    	nColBolusDurationTD[nCmpd]           = colsTD.get(cmpdBolusDuration);
    	    	nColInfusionDoseTD[nCmpd]            = colsTD.get(cmpdInfusionDose);
    	    	nColInfusionDurationTD[nCmpd]        = colsTD.get(cmpdInfusionDuration);
    	    	nColDermalAppliedThicknessTD[nCmpd]  = colsTD.get(cmpdDermalAppliedThickness);
    	    	nColDermalAppliedAreaTD[nCmpd]       = colsTD.get(cmpdDermalAppliedArea);
    	    	nColDermalAppliedVolumeTD[nCmpd]     = colsTD.get(cmpdDermalAppliedVolume);
    	    	nColDermalAppliedPlaceTD[nCmpd]      = colsTD.get(cmpdDermalAppliedPlace);
    	    	nColPctInhaledTD[nCmpd]              = colsTD.get(cmpdPctInhaled);
    	    	nColPctSwallowedTD[nCmpd]            = colsTD.get(cmpdPctSwallowed); 
    	    	
    	    	//put the above pairwise into a column map for this specific compound
    	    	TreeMap<String, Integer> colsTDCurrent = new TreeMap<String, Integer>();
    	    	colsTDCurrent.put(cmpdRoute,                  nColRouteTD[nCmpd]  );
    	    	colsTDCurrent.put(cmpdDose,                   nColDoseTD[nCmpd]  );
    	    	colsTDCurrent.put(cmpdDosingRegimen,          nColDosingRegimenTD[nCmpd]  );
    	    	colsTDCurrent.put(cmpdDoseInterval,           nColDoseIntervalTD[nCmpd]  );         
    	    	colsTDCurrent.put(cmpdNoDoses,                nColNoDosesTD[nCmpd]  );               	 
    	    	colsTDCurrent.put(cmpdBolusDoseType,          nColBolusDoseTypeTD[nCmpd]  );       
    	    	colsTDCurrent.put(cmpdBolusDuration,          nColBolusDurationTD[nCmpd]  );       
    	    	colsTDCurrent.put(cmpdInfusionDose,           nColInfusionDoseTD[nCmpd]  );       
    	    	colsTDCurrent.put(cmpdInfusionDuration,       nColInfusionDurationTD[nCmpd]  );        	
    	    	colsTDCurrent.put(cmpdDermalAppliedThickness, nColDermalAppliedThicknessTD[nCmpd]  ); 
    	    	colsTDCurrent.put(cmpdDermalAppliedArea,      nColDermalAppliedAreaTD[nCmpd]  ); 
    	    	colsTDCurrent.put(cmpdDermalAppliedVolume,    nColDermalAppliedVolumeTD[nCmpd]  ); 
    	    	colsTDCurrent.put(cmpdDermalAppliedPlace,     nColDermalAppliedPlaceTD[nCmpd]  ); 
    	    	colsTDCurrent.put(cmpdPctInhaled,        	  nColPctInhaledTD[nCmpd]  ); 
    	    	colsTDCurrent.put(cmpdPctSwallowed,           nColPctSwallowedTD[nCmpd]); 

        	    //get matching candidate values and units  and map them by compound
        	    TreeMap<Integer, Vector<String> > valsTD = candidateValues.get("Inputs_TrialDesign");
        	    TreeMap<Integer, UnitType> unitsTD = candidateUnits.get("Inputs_TrialDesign"); 
        	    
        	    TreeMap<Integer, Vector<String> > valsTDCurrentCmpd = new TreeMap<Integer, Vector<String> >() ;
        	    TreeMap<Integer, UnitType>       unitsTDCurrentCmpd = new TreeMap<Integer, UnitType >() ;
        	    
        	    for(Integer colNo : colsTDCurrent.values())
        	    {
        	    	if(colNo != null)
        	    	{
        	    	    valsTDCurrentCmpd.put(colNo,  valsTD.get(colNo));
        	    	    unitsTDCurrentCmpd.put(colNo, unitsTD.get(colNo));
        	    	}
        	    }
        	    
        	    //link them to the vectors indexed by compound  
        	    valsTDCmpd.set(nCmpd,  valsTDCurrentCmpd);
        	    unitsTDCmpd.set(nCmpd, unitsTDCurrentCmpd);
    	    }
    	}
    	
    	///////////////////////////////////////////////////////////////////////////////////////////////////////////
    	//Use the values to write the <UPGRADE IN PROGRESS: FIRST!!!!> dosing table 
	    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    	//assuming dosing template has already set up the table 
    	SimulationItem dosingTable = sob.getSimulation().getListOfDosing().get(0);//.add(dosingTable);
        HeaderColumnsDefinition dosingHeaders = dosingTable.getDefinition();
        int nDosingColsExpected = dosingHeaders.getListOfColumn().size();
	    
    	Vector<String> rowVals  = new Vector<String>();
   
    	//rows to be collected sorted on the first four columns before writing to dataset 
    	DosingRowComparator rowComparator = new DosingRowComparator();   //aid to sorting
        SortedSet < RowInfo> rowsToSort = new TreeSet<RowInfo>(rowComparator);
        RowInfoFactory riFac = new RowInfoFactory(0 /* startkey */);  



        
		for(int i = 0; i < ids.size(); i++) 
    	{

         	for(int nCmpd= 0; nCmpd < SimcypConstants.LAST_INDEX_DOSED_CMPD; nCmpd++)
    	   	{
    	 		Double doseInterval  = 0.;
    	 		Double dose          = 0.;
    	 		String inputCompartment = ".";
    	 		String inputModel    = ".";
    	 		String time          = ".";
    	 		String route         = ".";
    	        InputSiteRoute siteRouteCode = InputSiteRoute.UNSET;  //code to disambiguate admin site/route info for unique ADM generation. 
    	        final String SITE_ROUTE_NOT_FOUND = "-1";
    	 		double duration      = 0.;

    	 		SimcypConstants.DoseScalingType doseScaleType = SimcypConstants.DoseScalingType.UNKNOWN; 

    	 		String cmpdShortName = SimcypConstants.strCompoundShortNames[nCmpd].trim().replaceAll("\\s", "_");
    	   		  	 		
    	 		//...first from the custom dosing records   
    	   	

    	   		//CustomDosing table is a table of dosing(event) rows indexed by the Dosing (event) number
    	   		//these are currently standard across ALL subjects: it is only the dose itself which may vary individually 
    	   		//(with dose individualised by BWT if /kg  or BSA /m2)
    	   		int size = 0;
    	   		if(valsCustomDosingCmpd.elementAt(nCmpd) != null)
    	   		{
    	   			if(valsCustomDosingCmpd.elementAt(nCmpd).get(1) != null)
    	   				size = valsCustomDosingCmpd.elementAt(nCmpd).get(1).size();
    	   			
            	 	for(int nDoseNumber  = 0; nDoseNumber < size ; nDoseNumber++)
            	   	{ 	 
            	 		TreeMap<Integer, Vector<String> > valsCustomDosingCurrentCmpd = valsCustomDosingCmpd.elementAt(nCmpd);

	       			    Vector<String> doseValues  = valsCustomDosingCurrentCmpd.get(nColDoseCustom);		    	    
			    	    if(doseValues != null)		    	        	
			    	    {   
			    	      	try
			    	       	{
	    			            dose = Double.parseDouble(doseValues.elementAt(nDoseNumber));
			    	       	}
			    	       	catch(NumberFormatException e)
			    	       	{
			    	       		//leave at zero
			    	       		System.err.println("Error parsing dose interval as Double");
			    	       	}	    			        
			    	    }
			    	    
	            	    Vector<String> doseUnitValues   =  valsCustomDosingCurrentCmpd.get(nColDoseUnitCustom);
			    	    if(doseUnitValues != null)		    	        	
			    	    {
			    	    	String doseUnitVal = doseUnitValues.elementAt(nDoseNumber); 
	    			        if(doseUnitVal.equalsIgnoreCase("mg/kg") && valsBWT != null)
	    			        {
				    	      	try
				    	       	{
		    			            dose *= Double.parseDouble(valsBWT.elementAt(i)); 
		    			            doseScaleType =  SimcypConstants.DoseScalingType.PER_KG_BWT;
				    	       	}
				    	       	catch(Exception e)
				    	       	{
				    	       		//leave at zero
				    	       		System.err.println("Error scaling dose with parsed Double body Weight. " + e.getMessage());
				    	       	}
	    			        }
	    			        else if (doseUnitVal.equalsIgnoreCase("mg/m2") && valsBSA != null)
	    			        {
				    	      	try
				    	       	{
		    			            dose *= Double.parseDouble(valsBWT.elementAt(i)); 
		    			            doseScaleType =  SimcypConstants.DoseScalingType.PER_M2_BSA;
				    	       	}
				    	       	catch(Exception e)
				    	       	{
				    	       		//leave at zero
				    	       		System.err.println("Error scaling dose with parsed Double body surface area. " + e.getMessage());
				    	       	}	
	    			        }
	    			        else
	    			        {
	    			        	doseScaleType =  SimcypConstants.DoseScalingType.UNSCALED;
			    	       	}	    	
	    			        
			    	       	if(doseScaleType == SimcypConstants.DoseScalingType.UNKNOWN)
			    	       	{
			    	       		System.err.println("Failed to find dose scaling for indivdiual + i");
			    	       	}
	    			    }
		    	    
			    	    Vector<String> values =  valsCustomDosingCurrentCmpd.get(nColTimeCustom);
	    	    	    if(values != null)		    	        	
	    	    	    {    			  
	    	    	      	time = values.elementAt(nDoseNumber);
	    			    }

	    	    	    values = valsCustomDosingCmpd.elementAt(nCmpd).get(nColDurationCustom);
	    	    	    if(values != null)		    	        	
	    	    	    {   
	    	    	    	if(values != null)		    	        	
		    		    	{ 
		    	    	    	if(values.elementAt(nDoseNumber).equalsIgnoreCase(SimcypConstants.strNonValues[NonValues.NA.ordinal()]))
		    	    	    	   duration = 0.;	    		
		    	    	    	else
		    	    	    	{
					    	      	try
					    	       	{
			    			            duration = Double.parseDouble(values.elementAt(nDoseNumber)); 
					    	       	}
					    	       	catch(Exception e)
					    	       	{
					    	       		//leave at zero
					    	       		System.err.println("Error parsing custom dosing duration value as Double.");
					    	       	}
		    	    	    	}	    		    	      			  
	        			    }
	    			    }
	    	    	    
	    	    	    values = valsCustomDosingCmpd.elementAt(nCmpd).get(nColRouteCustom);
	    	    	    if(values != null)		    	        	
	    	    	    {    			  
	    			        route = values.elementAt(nDoseNumber);	    			        
	    			        siteRouteCode = inputSiteRouteIndex.get(route); 
	    			    }
            
            	 	
	            	 	rowVals = new Vector<String>(nDosingColsExpected);
		    	    	
	            	 	rowVals.add(ids.elementAt(i));	
		    	       	rowVals.add(time);           	 	
		    	       	rowVals.add(cmpdShortName);	 
		    	    	rowVals.add(Integer.toString(TableXReferencer.CsvTables.CUSTOM_DOSING.ordinal()));	
		    	    	rowVals.add(Double.toString(dose));  //may need to format this   
		    	    	rowVals.add(Double.toString(duration));
		    	    	rowVals.add(siteRouteCode != null? Integer.toString(calculateADMCode(Compound.values()[nCmpd], siteRouteCode, false)) : SITE_ROUTE_NOT_FOUND);
		    	    	rowVals.add(route);
		    	    	assert(nDosingColsExpected == rowVals.size());
		    	    	  
		    	    	RowInfo rowInfo = riFac.createRowInfo(rowVals, null);	    	    	
		    	    	try
		    	    	{	    	    		
		    	    		rowsToSort.add(rowInfo);	    	    		
		    	    	}
		    	    	catch (NumberFormatException e)
		    	    	{
		    	    		System.err.println("Number format exception while trying to sort rows for dosing table based ion integer, double parsing.");
		    	    	}
		    	    	catch( NullPointerException e)
		    	    	{
		    	    		System.err.println("Null pointer exception while extracting dosing table String values for sorting as integer, double");
		    	    	}
            	   	}
            	}

            	//----------------------------------- 	
    	   		//...next from the Inputs_TrialDesign 

            	//This is a table of just one data row (under header and units rows) with dosing information for different compounds side by side with 
    	   		//different header prefixes for different compounds. These are currently standard across ALL subjects: it is only the dose itself which may vary individually 
    	   		//(with dose individualised by BWT if /kg  or BSA /m2 identifiable from the UNITs row for a dose column) 
    	   		int nDoses = 0;   

    	 	    //SimcypConstants.DosingRegimen regimen = SimcypConstants.DosingRegimen.UNSET;
    	 	    Vector<String> values = null;
    	 	    if(valsTDCmpd.elementAt(nCmpd) != null)
       			{
    	 	    	if(nColDosingRegimenTD[nCmpd] != null)
    	 	    	{
	    	 	   		values = valsTDCmpd.elementAt(nCmpd).get(nColDosingRegimenTD[nCmpd]);
				  	    if(values != null)		    	        	
				  	    {    			  
		    		        if(values.elementAt(0).equalsIgnoreCase(SimcypConstants.strDosingRegimen[SimcypConstants.DosingRegimen.SINGLE_DOSE.ordinal()]))
		    		        {
		    		      		//regimen = SimcypConstants.DosingRegimen.SINGLE_DOSE;
		    		      		nDoses = 1;
		    		        }
		    		        //else
		    		      	    //regimen = SimcypConstants.DosingRegimen.MULTIPLE_DOSE;    			            
		    		    } 
    	 	    	}
		    	       
    	 	    	if(nColNoDosesTD[nCmpd] != null)
    	 	    	{
    	 	    		
		       		    values = valsTDCmpd.elementAt(nCmpd).get(nColNoDosesTD[nCmpd]);
				   	    if(values != null)		    	        	
				   	    {
				   	    	if(!values.elementAt(0).equalsIgnoreCase(SimcypConstants.strNonValues[NonValues.NA.ordinal()]))
				   	    	{	
					   	     	try
					   	      	{
					   	     		float fnDoses = Float.parseFloat(values.elementAt(0));					   	     		
			    		            nDoses = (int) fnDoses;
			    		            if(nDoses != fnDoses)
			    		            	throw new NumberFormatException("Floating point value not an integer");
					   	       	}
					   	       	catch(NumberFormatException e)
					   	       	{
					   	       		//leave at zero
					   	       		System.err.println("Error reading number of doses as Integer");
					   	       	}
				   	    	}
		    		    }	
     	      		}   	 	    	
    	 	    	
	    	 	       	
     	       		if(nColDoseTD[nCmpd] != null)
     	       		{
	      			    values = valsTDCmpd.elementAt(nCmpd).get(nColDoseTD[nCmpd]);
	      			    UnitType unitType = unitsTDCmpd.elementAt(nCmpd).get(nColDoseTD[nCmpd]);
			    	    if(values != null)		    	    	
			    	    {   
			    	    	if(unitType != null)
			    	    	{
			    	    		try
			    	    		{
			    	    			dose = Double.parseDouble(values.elementAt(0)); 
				    	    		if(unitType.getUnit() == UnitType.UnitID.UNIT_MG_PER_KG)
				    	    		{
					    	    			double weight = Double.parseDouble(valsBWT.elementAt(i));
					    	    			dose *= weight;
					    	    	}
					    	   		else if(unitType.getUnit() == UnitType.UnitID.UNIT_MG_PER_M2)
					    	   		{
					    	   			double bsa = Double.parseDouble(valsBSA.elementAt(i));
					    	   			dose *= bsa;			    	    			
					    	   		}
				    	    	}
		    	    			catch(Exception e)
		    	    			{
		    	    				System.err.println("Error scaling dose with body scaler parsed as double. Subject " + i + ". Units Type " + unitType.getUnitName());
		    	    			}
			    	    	}
	
			    	    }
     	       		}
		
     	       		if(nColDoseIntervalTD[nCmpd] != null)
     	       		{
	      			    values = valsTDCmpd.elementAt(nCmpd).get(nColDoseIntervalTD[nCmpd]);
			    	    if(values != null)		    	        	
			    	    {    		
			    	    	if(!values.elementAt(0).equalsIgnoreCase(SimcypConstants.strNonValues[NonValues.NA.ordinal()]))
			    	    	{
				    	      	try
				    	       	{
		    			            doseInterval = Double.parseDouble(values.elementAt(0));
				    	       	}
				    	       	catch(NumberFormatException e)
				    	      	{
				    	       		
				    	       		System.err.println("Error parsing dose interval as Double");
				    	       	}
			    	    	}
	    			    }
     	       		}
	      			        
     	       		if(nColRouteTD[nCmpd] != null)
     	       		{
     	       
			    	    values = valsTDCmpd.elementAt(nCmpd).get(nColRouteTD[nCmpd]);
			    	    if(values != null)		    	        	
			    	    {    			  
	    			        route = values.elementAt(0);	    			       
	    			        InputSiteRoute tempSiteRouteCode = inputSiteRouteIndex.get(route);
	    			        //the site/route code should not e applpied in custom dosing cases
	    			        if(tempSiteRouteCode != InputSiteRoute.CUSTOM)
	    			        {
	    			        	siteRouteCode = tempSiteRouteCode;
	    			        }
			    	    }
		    	    }
   	       		
	   	   		}
    	 	    
    	 	    if(siteRouteCode != InputSiteRoute.OTHER_SITE_GENERIC && siteRouteCode != InputSiteRoute.CUSTOM)  //use the TD route unless more specific    
    	 	    {	
    	 	    	try
    	 	    	{
	    	    	    for (int nDose = 0; nDose < nDoses; nDose++)
			    	    {	
			    	    	rowVals = new Vector<String>(nDosingColsExpected);

			    	       	rowVals.add(ids.elementAt(i));	
			    	       	rowVals.add(Double.toString(doseInterval*nDose));                                                                            //TIME           //may need to format this
			    	       	rowVals.add(cmpdShortName);                                                                                                  //CMPD
			    	    	rowVals.add(Integer.toString(TableXReferencer.CsvTables.INPUTS_TRIAL_DESIGN.ordinal()));                                     //SOURCE
			    	       	rowVals.add(Double.toString(dose));                                                                                          //AMT    //may need to format this 
			    	       	rowVals.add(Double.toString(duration));                                                                                      //DURATION
			    	    	rowVals.add(siteRouteCode != null? Integer.toString(calculateADMCode(Compound.values()[nCmpd], siteRouteCode, false)) : SITE_ROUTE_NOT_FOUND); //ADM
			    	       	rowVals.add(route);                                                                                                          //SITE_ROUT(Simcyp)
			    	    	assert(nDosingColsExpected == rowVals.size());
			    	    	
			    	       	RowInfo rowInfo = riFac.createRowInfo(rowVals, "Inputs Trial Design route and compound used to define ADM code");
			    	       	rowsToSort.add(rowInfo);	
	    	    		}
	    	    	}
			    	catch (NumberFormatException e)
			    	{
			    		System.err.println("Number format exception while trying to sort rows for dosing table based in integer, double parsing.");
			    	}
			    	catch( NullPointerException e)
			    	{
			    		System.err.println("Null pointer exception while extracting dosing table String values for sorting as integer, double");
			    	}
    	    	}
	    		
    	    	
    	    	
    	   		//...next from the Inputs_<Cmpd>, cater for other-site dosing inputCompartment and preset profile 
    	    	//(remaining user-input profile is done separately)
    	 	    if(valsInputsCmpd.elementAt(nCmpd) != null)
       			{
     	    		//get this to help with other-site dose
     	    		if(nColInputCompartment[nCmpd] != null)
     	       		{
     	    			values =  valsInputsCmpd.elementAt(nCmpd).get(nColInputCompartment[nCmpd]);
     	    			if(values != null)
     	    			{
     	    				inputCompartment = values.elementAt(0);
     	    		        assert(siteRouteCode == InputSiteRoute.OTHER_SITE_GENERIC || siteRouteCode == InputSiteRoute.UNSET);
                            // make the generic other site indivation more specific 
                            siteRouteCode = inputSiteRouteIndex.get(inputCompartment);
     	    			}
     	       		}  
  
     	    		try
     	    		{	
     	    		    //temporarily use mean values for FO parameters - individualised outputs pending 
	         	    	if(nColInputModel != null && nColInputModel[nCmpd] != null)
	         	       	{
		         	    	values =  valsInputsCmpd.elementAt(nCmpd).get(nColInputModel[nCmpd]);
		         	    	if(values != null)
		         	    	{
	                            inputModel = values.elementAt(0);                        
	         	    		}
	         	       	}
	         	    	
     	    			if(inputModel.equals("First Order"))
     	    			{
     	    				double tLagMeanFOInput = 0.; 
             	    		double faMeanFOInput   = 0.; 
             	    		double kaMeanFOInput   = 0.; 
     	    				
		       	       	    //temporarily use mean values for FO parameters - individualised outputs pending 
		         	    	if(nColInput_tLagMean[nCmpd] != null)
		         	       	{
		         	    		values =  valsInputsCmpd.elementAt(nCmpd).get(nColInput_tLagMean[nCmpd] );
		         	    		if(values != null)
		         	    		{
		         	    			tLagMeanFOInput = Double.parseDouble(values.elementAt(0)); 
		         	    		}
		         	       	}
		 
		       	       	
		         	    	if(nColInput_faMean[nCmpd] != null)
		         	       	{
		         	    		values =  valsInputsCmpd.elementAt(nCmpd).get(nColInput_faMean[nCmpd]);
		         	    		if(values != null)
		         	    		{
		         	    			faMeanFOInput = Double.parseDouble(values.elementAt(0)); 
		         	    		}
		         	       	}
		         	    	
		       	       	    
		         	    	if(nColInput_kaMean[nCmpd] != null)
		         	       	{
		         	    		values =  valsInputsCmpd.elementAt(nCmpd).get(nColInput_kaMean[nCmpd]);
		         	    		if(values != null)
		         	    		{
		         	    			kaMeanFOInput = Double.parseDouble(values.elementAt(0)); 
		         	    		}
		         	       	}
		         	    	
			    	    	rowVals = new Vector<String>(nDosingColsExpected);
				    	    	

			    	    	//NOT SURE BEST WAY TO REPRESENT THIS YET
			    	       	rowVals.add(ids.elementAt(i));
			    	       	rowVals.add(Double.toString(tLagMeanFOInput)); //TIME           //may need to format this  CHECK THIS ALWAYS TIME ZERO BASED OR RELATIVE TO TD DOse time??//ID
			    	       	rowVals.add(cmpdShortName);                                                                                                     //CMPD
			    	    	rowVals.add(Integer.toString(TableXReferencer.CsvTables.INPUTS_.ordinal()));                                                    //SOURCE
			    	       	rowVals.add(Double.toString(dose*faMeanFOInput ));                //may need to format this                                     //AMT 
			    	       	rowVals.add(Double.toString(0));                                                                                                //DURATION   
    					    rowVals.add(siteRouteCode != null? Integer.toString(calculateADMCode(Compound.values()[nCmpd], siteRouteCode, true)): SITE_ROUTE_NOT_FOUND);     //ADM	
			    	       	rowVals.add(inputCompartment + "FO_DEPOT_ka_" + String.valueOf(kaMeanFOInput)); 	       		    	                        //SITE_ROUTE (Simcyp)
			    	    	assert(nDosingColsExpected == rowVals.size());
			    	    	
			    	    	RowInfo rowInfo = riFac.createRowInfo(rowVals, "\'Other site\' first order input flagged depot receiving F*meandose with ka value encoded in ADM string (individualisation not yet implemented)");
			    	       	rowsToSort.add(rowInfo);	
     	    			}
     	    			else if (inputModel.equals("Zero Order") || inputModel.equals("Bolus"))
     	    			{
		         	    	if(nColInputZODuration[nCmpd] != null)
		         	       	{
		         	    		values =  valsInputsCmpd.elementAt(nCmpd).get(nColInputZODuration[nCmpd]);
		         	    		if(values != null)
		         	    		{
		         	    			duration = Double.parseDouble(values.elementAt(0)); 
		         	    		}
     	    				}
		         	    	
		         	    	rowVals = new Vector<String>(nDosingColsExpected);
     	    				
			    	       	rowVals.add(ids.elementAt(i));
			    	       	rowVals.add(Double.toString(0)); //time         //NEED TO ADD THIS TO TIME FROM TRIAL DESIGN?
			    	       	rowVals.add(cmpdShortName);			    	       	
			    	    	rowVals.add(Integer.toString(TableXReferencer.CsvTables.INPUTS_.ordinal()));
			    	       	rowVals.add(Double.toString(dose));                //may need to format this   //TO CHECK no fa scaler needed here 
			    	       	rowVals.add(Double.toString(duration));		
			    	       	rowVals.add(siteRouteCode != null? Integer.toString(calculateADMCode(Compound.values()[nCmpd], siteRouteCode, false)) :  SITE_ROUTE_NOT_FOUND); //ADM
			    	       	rowVals.add(inputCompartment); 	       		    	       
			    	    	assert(nDosingColsExpected == rowVals.size());
			    	    	
			    	    	RowInfo rowInfo = riFac.createRowInfo(rowVals, "\'Other site\' input model " + inputModel);
			    	       	rowsToSort.add(rowInfo);			    	       	
     	    			}
     	    			
     	    		}
		    	    catch(NumberFormatException e)
		    	    {
		    	    	//leave at zero
		    	    	System.err.println("Error parsing Inputs Compound data as numeric");				    	
    	   			}
    		    	catch( NullPointerException e)
    		    	{
    		    		System.err.println("Null pointer exception while extracting Inputs Compound data for dosing table");
    		    	}
         	    	
                    //others we might use 
         	    	//
         	    	//nColInputInterpolMethod[nCmpd]     
         	    	//nColInput_tLagCV[nCmpd]            
         	    	//nColInput_faCV[nCmpd]        
         	    	//nColInput_kaCV[nCmpd]      
       			}

    	 	    //otherSIte Input profile as linear-infusion (approximation to smooth curve when piecwise cubic)
    	    	if(valsOtherSiteProfileCmpd != null)
    	   		{
    	   			
    	   			double totalDose = dose;  //define from trial design and any body-scaling 
    	   			
    	   		    if(valsOtherSiteProfileCmpd.elementAt(nCmpd) != null)
    	   			{
    	   				Vector<String> subjIndices = valsOtherSiteProfileCmpd.elementAt(nCmpd).get(nColIndexOSite);   //one-based
    	   				Vector<String> times       = valsOtherSiteProfileCmpd.elementAt(nCmpd).get(nColTimeOSite );
    	   				Vector<String> cumInputs   = valsOtherSiteProfileCmpd.elementAt(nCmpd).get(nColCumInputPctOSite);
    	   				
    	   				if(subjIndices != null && times != null || cumInputs != null)
    	   				{
	        	   			assert(subjIndices.size() == cumInputs.size());
	        	   			assert(subjIndices.size() == times.size());
	    	   				
	    	   				try
	        	   			{
	        	   				//get a table row for each record with the given subject id except the last, using the difference in times 
	        	   				//and cumulative amounts to create an infusion segment record; assume items are time-ordered I(make more robust later??)
	        	   				for(int ii= 0; ii < subjIndices.size()-1; ii++)
	   	   						{
	        	   					int subjIndexFrom = Integer.parseInt(subjIndices.elementAt(ii));
	        	   					int subjIndexTo   = Integer.parseInt(subjIndices.elementAt(ii +1));
	   	   							if(subjIndexFrom == i+1 && subjIndexTo == i+1)
	   	   							{
	   	   						        double dTime         = Double.parseDouble(times.elementAt(ii));
		       	   					    double inputPct      = Double.parseDouble(cumInputs.elementAt(ii+1)) - Double.parseDouble(cumInputs.elementAt(ii)); 
		       	   					    double inputDuration = Double.parseDouble(times.elementAt(ii+1))     - dTime;
		       	   					  
		    			            	assert(inputDuration >= 0);
		       	   					    assert(ids.elementAt(i) == new Integer(i+1).toString());    			            	
				    			           
		       	   					    rowVals = new Vector<String>(nDosingColsExpected);
		       	   					    
		       	   					    rowVals.add(new Integer(i+1).toString());	                                             //ID
			    					    rowVals.add(String.format("%.4f", dTime));                                               //TIME     
			    					    rowVals.add(cmpdShortName);                                                              //CMPD 
			    					    rowVals.add(Integer.toString(TableXReferencer.CsvTables.OTHER_SITE_PROFILE.ordinal()));  //SOURCE
			    					    rowVals.add(String.format("%.4f", totalDose * inputPct/100.));                           //AMT
			    					    rowVals.add(String.format("%.4f", inputDuration));                                       //DURATION
			    					    rowVals.add(siteRouteCode != null? Integer.toString(calculateADMCode(Compound.values()[nCmpd], siteRouteCode, false) ):  SITE_ROUTE_NOT_FOUND); //ADM
			    					    rowVals.add(inputCompartment);                                                           //SITE_ROUTE
			    					    assert(nDosingColsExpected == rowVals.size());
			    					    
						    	    	RowInfo rowInfo = riFac.createRowInfo(rowVals, "\'Other site\' input model " + inputModel);
						    	       	rowsToSort.add(rowInfo);		
	   	   							}
	       	   					}
	        	   			}
				    	    catch(NumberFormatException e)
				    	    {
				    	    	//leave at zero
				    	    	System.err.println("Error parsing Other Site Input data as numeric");				    	
	        	   			}
	        		    	catch( NullPointerException e)
	        		    	{
	        		    		System.err.println("Null pointer exception while extracting Other Site Inputs for dosing table");
	        		    	}
	        	   			catch(ArrayIndexOutOfBoundsException e)
	        	   			{
	        	   				System.err.println("ArrayIndexOutOfBounds while extracting Other Site Inputs for dosing table");
	        	   			}   
    	   				}
    	   			}    	   		
    	   		    
    	   		}
    	   		
    	        
    	    }
    	}
    	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    	//create the table rows
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    	
		String[] row = new String[nDosingColsExpected];
		for(RowInfo rowInstance : rowsToSort)	
		{
    		//the row
	        DatasetRow newRow = dosingTable.createRow(rowInstance.contents.toArray(row));
	        
	        //find any corresponding row-annotation string and load it
	        String note = rowInstance.annotation;
	        AnnotationType currentAnnotation = new AnnotationType();
	        if(note != null)
	           currentAnnotation.setValue(note); 
	        newRow.setDescription(currentAnnotation);
		}
    }

}
