package com.simcyp.simcypOutputs2SO;

import java.util.ArrayList;

import com.simcyp.simcypOutputs2SO.Constants.Header;
import com.simcyp.simcypOutputs2SO.Constants.SimcypOutputType;

import eu.ddmore.libpharmml.dom.commontypes.FalseBoolean;
import eu.ddmore.libpharmml.dom.commontypes.IntValue;
import eu.ddmore.libpharmml.dom.commontypes.MissingValue;
import eu.ddmore.libpharmml.dom.commontypes.MissingValueSymbol;
import eu.ddmore.libpharmml.dom.commontypes.RealValue;
import eu.ddmore.libpharmml.dom.commontypes.Scalar;
import eu.ddmore.libpharmml.dom.commontypes.StringValue;
import eu.ddmore.libpharmml.dom.commontypes.SymbolType;
import eu.ddmore.libpharmml.dom.commontypes.TrueBoolean;
import eu.ddmore.libpharmml.dom.dataset.ColumnType;

public class SimcypOutputColumn
{
	private String _header;
	private String _unit;
	private ArrayList<String> _rawValues;
	private SymbolType _symbolType;
	private boolean _isValid;
	
	public SimcypOutputColumn(String header, String unit, ArrayList<String> values)
	{
		String cleanHeader = "";
		
		for (int i = 0; i < header.length(); ++i)
		{
			char c = header.charAt(i);
			
			if ((int)c < 128)
			{
				cleanHeader += c;
			}
			else
			{
				final String replacement = "ERRCHAR";
								
				System.err.println(String.format("Illegal character detected in header ('%c' in %s) - substituting '%s' in the Standard Output instead", c, header, replacement));
				cleanHeader += replacement;
			}
		}
		
		_header = cleanHeader;
		_unit = unit;
		_rawValues = new ArrayList<String>(values);
		_symbolType = determineSymbolType();
		_isValid = validate();
	}
	
	private SymbolType determineSymbolType()
	{
		if (isHeader(Header.INDEX) || isHeader(Header.TRIAL) || 
			isHeader(Header.POPULATION) || isHeader(Header.SEX_CODE) || 
			isHeader(Header.STATE_VARIABLE_ID) || isHeader(Header.STATE_VARIABLE_MODEL))
		{
			return SymbolType.INT;
		}
		
		int booleanCount = 0;
		int realCount = 0;
		int stringCount = 0;
		
		for (String value : _rawValues)
		{
			if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false") || 
				value.equalsIgnoreCase("yes") || value.equalsIgnoreCase("no"))
			{
				++booleanCount;
			}
			else
			{
				try
				{
					Double.parseDouble(value);
					++realCount;
				}
				catch (NumberFormatException e)
				{
					if (value.equalsIgnoreCase("n/a"))
					{
						++realCount;
					}
					else
					{
						++stringCount;
					}
				}
			}
		}
		
		if (booleanCount > realCount && booleanCount > stringCount)
		{
			return SymbolType.BOOLEAN;
		}
		
		if (realCount > booleanCount && realCount > stringCount)
		{
			return SymbolType.REAL;
		}
		
		return SymbolType.STRING;
	}

	private boolean validate()
	{
		if (_header.isEmpty())
		{
			System.err.println("Column header cannot be empty");
			return false;
		}
		
		if (_rawValues.isEmpty())
		{
			System.err.println("Column values are required");
			return false;
		}
		
		if (_rawValues.contains(""))
		{
			System.err.println("Column values cannot be empty");
			return false;
		}
		
		return true;
	}
	
	public boolean isValid()
	{
		return _isValid;
	}
	
	public String getHeader()
	{
		return _header;
	}
	
	public void setHeaderSuffix(int suffix)
	{
		final String duplicate = "_DUPLICATE_";
		
		for (int i = suffix; i > 0; --i)
		{
			if (_header.endsWith(duplicate + i))
			{
				_header = _header.substring(0, _header.lastIndexOf(duplicate + i));
				break;
			}
		}
		
		_header += duplicate + suffix;
	}
	
	public String getId()
	{
		return Helpers.toId(_header);
	}

	public ColumnType getColumnType(SimcypOutputType parentOutputType)
	{
		if (parentOutputType == SimcypOutputType.POPULATION_PARAMETERS || 
			parentOutputType == SimcypOutputType.INDETERMINATE)
		{
			return ColumnType.UNDEFINED;
		}
		
		if (isHeader(Header.INDEX))
		{
			return ColumnType.ID;
		}
		
		if (isHeader(Header.TRIAL))
		{
			return parentOutputType == SimcypOutputType.PROFILES ? ColumnType.UNDEFINED : ColumnType.REPLICATE;
		}
		
		if (isHeader(Header.POPULATION))
		{
			return parentOutputType == SimcypOutputType.PROFILES ? ColumnType.UNDEFINED : ColumnType.COVARIATE;
		}
		
		if (isHeader(Header.TIME))
		{
			return ColumnType.TIME;
		}
		
		return ColumnType.COVARIATE;
	}

	public SymbolType getSymbolType()
	{
		return _symbolType;
	}
	
	private boolean hasUnit()
	{
		return _unit.isEmpty() == false;
	}
	
	public String getDescription()
	{
		if (hasUnit())
		{
			return String.format("%s (unit: %s)", _header, _unit);
		}
		
		return _header;
	}
	
	public int getRowCount()
	{
		return _rawValues.size();
	}
	
	public <T> T getValue(int index, Class<T> valueType)
	{
		final String rawValue = _rawValues.get(index);
		
		if (valueType.equals(Scalar.class))
		{
			switch (getSymbolType())
			{
			case BOOLEAN:
				if (rawValue.equalsIgnoreCase("true") || rawValue.equalsIgnoreCase("yes"))
				{
					return valueType.cast(new TrueBoolean());
				}
				
				if (rawValue.equalsIgnoreCase("false") || rawValue.equalsIgnoreCase("no"))
				{
					return valueType.cast(new FalseBoolean());
				}
				
				return valueType.cast(new MissingValue(MissingValueSymbol.NA));
				
			case ID:
			case INT:
				try
				{
					return valueType.cast(new IntValue((int)Double.parseDouble(rawValue)));
				}
				catch (NumberFormatException e)
				{
					return valueType.cast(new MissingValue(MissingValueSymbol.NA));
				}
				
			case REAL:
				try
				{
					return valueType.cast(new RealValue(Double.parseDouble(rawValue)));
				}
				catch (NumberFormatException e)
				{
					return valueType.cast(new MissingValue(MissingValueSymbol.NA));
				}
				
			case STRING:
			default:
				return valueType.cast(new StringValue(rawValue));
			}
		}
		else if (valueType.equals(int.class) || valueType.equals(Integer.class))
		{
			try
			{
				return valueType.cast(Integer.parseInt(_rawValues.get(index)));
			}
			catch (NumberFormatException e)
			{
				return valueType.cast(Constants.INVALID_INTEGER);
			}
		}
		else if (valueType.equals(double.class) || valueType.equals(Double.class))
		{
			try
			{
				return valueType.cast(Double.parseDouble(_rawValues.get(index)));
			}
			catch (NumberFormatException e)
			{
				return valueType.cast(Constants.INVALID_DOUBLE);
			}
		}
		else if (valueType.equals(String.class))
		{
			return valueType.cast(rawValue);
		}
		
		return null;
	}
	
	public <T> T getValue(Class<T> valueType)
	{
		return getValue(0, valueType);
	}
	
	public <T> ArrayList<T> getValues(Class<T> valueType)
	{
		ArrayList<T> values = new ArrayList<T>();
		
		for (int i = 0; i < getRowCount(); ++i)
		{
			values.add(getValue(i, valueType));
		}
		
		return values;
	}
	
	public boolean isHeader(Header header)
	{
		final String headerString = header.toString().replace('_', ' ').toUpperCase();
		
		return 
			_header.equalsIgnoreCase(headerString) || 
			_header.toUpperCase().startsWith(headerString) ||
			_header.toUpperCase().endsWith(headerString);
	}

	@Override
	public int hashCode()
	{
		return 31 + (_header == null ? 0 : _header.hashCode());
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		
		if (obj == null)
		{
			return false;
		}
		
		if (getClass() != obj.getClass())
		{
			return false;
		}
		
		SimcypOutputColumn other = (SimcypOutputColumn)obj;
		
		if (_header == null)
		{
			if (other._header != null)
			{
				return false;
			}
		}
		else if (_header.equals(other._header) == false)
		{
			return false;
		}
		
		return true;
	}
}
