package com.simcyp.simcypOutputs2SO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.simcyp.simcypOutputs2SO.Constants.Compound;
import com.simcyp.simcypOutputs2SO.Constants.DoseUnit;
import com.simcyp.simcypOutputs2SO.Constants.DosingRegimen;
import com.simcyp.simcypOutputs2SO.Constants.DosingRoute;
import com.simcyp.simcypOutputs2SO.Constants.Header;
import com.simcyp.simcypOutputs2SO.Constants.InterpolationMethod;
import com.simcyp.simcypOutputs2SO.Constants.InputCompartment;
import com.simcyp.simcypOutputs2SO.Constants.InputModel;
import com.simcyp.simcypOutputs2SO.Constants.Output;
import com.simcyp.simcypOutputs2SO.Constants.PlaceOfApplication;

public class DosingData
{
	private final HashSet<Integer> _ids = new HashSet<Integer>();
	private final HashSet<Compound> _dosedCompounds = new HashSet<Compound>();
	private final HashMap<Integer, Double> _weights = new HashMap<Integer, Double>();
	private final HashMap<Integer, Double> _bodySurfaceAreas = new HashMap<Integer, Double>();
	private final HashMap<Compound, InputModel> _inputModels = new HashMap<Compound, InputModel>();
	private final HashMap<Compound, DosingRegimen> _dosingRegimens = new HashMap<Compound, DosingRegimen>();
	private final HashMap<Compound, DosingRegimen> _bolusDosingRegimens = new HashMap<Compound, DosingRegimen>();
	private final HashMap<Compound, DosingRoute> _routes = new HashMap<Compound, DosingRoute>();
	private final HashMap<Compound, DoseUnit> _units = new HashMap<Compound, DoseUnit>();
	private final HashMap<Compound, PlaceOfApplication> _places = new HashMap<Compound, PlaceOfApplication>();
	private final HashMap<Compound, InputCompartment> _inputCompartments = new HashMap<Compound, InputCompartment>();
	private final HashMap<Compound, InterpolationMethod> _interpolationMethods = new HashMap<Compound, InterpolationMethod>();
	private final HashMap<Compound, Double> _doses = new HashMap<Compound, Double>();
	private final HashMap<Compound, Double> _bolusDoses = new HashMap<Compound, Double>();
	private final HashMap<Compound, Double> _durations = new HashMap<Compound, Double>();	
	private final HashMap<Compound, Double> _thicknesses = new HashMap<Compound, Double>();
	private final HashMap<Compound, Double> _areas = new HashMap<Compound, Double>();
	private final HashMap<Compound, Double> _volumes = new HashMap<Compound, Double>();	
	private final HashMap<Compound, Double> _lagTimes = new HashMap<Compound, Double>();
	private final HashMap<Compound, Double> _fas = new HashMap<Compound, Double>();
	private final HashMap<Compound, Double> _kas = new HashMap<Compound, Double>();
	private final HashMap<Compound, Double> _intervals = new HashMap<Compound, Double>();
	private final HashMap<Compound, ArrayList<Double>> _customDosingOffsets = new HashMap<Compound, ArrayList<Double>>();
	private final HashMap<Compound, ArrayList<Double>> _customDosingDoses = new HashMap<Compound, ArrayList<Double>>();
	private final HashMap<Compound, ArrayList<Double>> _customDosingDurations = new HashMap<Compound, ArrayList<Double>>();
	private final HashMap<Compound, ArrayList<DoseUnit>> _customDosingUnits = new HashMap<Compound, ArrayList<DoseUnit>>();
	private final HashMap<Compound, ArrayList<DosingRoute>> _customDosingRoutes = new HashMap<Compound, ArrayList<DosingRoute>>();
	private final HashMap<Compound, ArrayList<InputCompartment>> _customDosingCompartments = new HashMap<Compound, ArrayList<InputCompartment>>();
	private final HashMap<Compound, ArrayList<Double>> _customDosingThicknesses = new HashMap<Compound, ArrayList<Double>>();
	private final HashMap<Compound, ArrayList<Double>> _customDosingAreas = new HashMap<Compound, ArrayList<Double>>();
	private final HashMap<Compound, ArrayList<Double>> _customDosingVolumes = new HashMap<Compound, ArrayList<Double>>();
	private final HashMap<Compound, ArrayList<PlaceOfApplication>> _customDosingPlaces = new HashMap<Compound, ArrayList<PlaceOfApplication>>();
	private final HashMap<Compound, ArrayList<Double>> _cumulativeInputTimes = new HashMap<Constants.Compound, ArrayList<Double>>();
	private final HashMap<Compound, HashMap<Integer, ArrayList<Double>>> _cumulativeInputPercents = new HashMap<Constants.Compound, HashMap<Integer,ArrayList<Double>>>();

	public DosingData(ArrayList<SimcypOutput> simcypOutputs)
	{
		for (SimcypOutput simcypOutput : simcypOutputs)
		{
			if (simcypOutput.isOutput(Output.DEMOGRAPHIC))
			{
				extractDataFromDemographic(simcypOutput);				
			}
			else if (simcypOutput.isOutput(Output.CUSTOM_DOSING))
			{
				extractDataFromCustomDosing(simcypOutput);				
			}
			else if (simcypOutput.isOutput(Output.OTHER_SITE_PROFILE))
			{
				extractDataFromOtherSiteProfile(simcypOutput);				
			}
			else if (simcypOutput.isOutput(Output.INPUTS))
			{
				extractDataFromInputs(simcypOutput);				
			}
		}
	}
	
	private void extractDataFromDemographic(SimcypOutput simcypOutput)
	{
		SimcypOutputColumn indexColumn = null;
		SimcypOutputColumn weightColumn = null;
		SimcypOutputColumn bsaColumn = null;
		
		for (SimcypOutputColumn column : simcypOutput.getColumns())
		{
			if (indexColumn == null && column.isHeader(Header.INDEX))
			{
				indexColumn = column;
			}			
			else if (weightColumn == null && column.isHeader(Header.WEIGHT))
			{
				weightColumn = column;
			}			
			else if (bsaColumn == null && column.isHeader(Header.BSA))
			{
				bsaColumn = column;
			}
		}
		
		if (indexColumn != null)
		{
			for (int i = 0; i < indexColumn.getRowCount(); ++i)
			{
				final int individualId = indexColumn.getValue(i, Integer.class);
				
				_ids.add(individualId);
				
				if (weightColumn != null)
				{
					if (_weights.containsKey(individualId) == false)
					{
						_weights.put(individualId, weightColumn.getValue(i, Double.class));
					}
				}
				
				if (bsaColumn != null)
				{
					if (_bodySurfaceAreas.containsKey(individualId) == false)
					{
						_bodySurfaceAreas.put(individualId, bsaColumn.getValue(i, Double.class));
					}
				}
			}
		}
	}
	
	private void extractDataFromCustomDosing(SimcypOutput simcypOutput)
	{
		Compound compound = simcypOutput.getCompound();
		
		if (compound != Compound.NULL)
		{
			_dosedCompounds.add(compound);
			
			for (SimcypOutputColumn column : simcypOutput.getColumns())
			{
				if (column.isHeader(Header.OFFSET))
				{
					if (_customDosingOffsets.containsKey(compound) == false)
					{
						_customDosingOffsets.put(compound, new ArrayList<Double>());
					}		
					
					_customDosingOffsets.get(compound).addAll(column.getValues(Double.class));
				}
				else if (column.isHeader(Header.DOSE))
				{
					if (_customDosingDoses.containsKey(compound) == false)
					{
						_customDosingDoses.put(compound, new ArrayList<Double>());
					}
					
					_customDosingDoses.get(compound).addAll(column.getValues(Double.class));
				}
				else if (column.isHeader(Header.DOSE_UNITS))
				{
					if (_customDosingUnits.containsKey(compound) == false)
					{
						_customDosingUnits.put(compound, new ArrayList<DoseUnit>());
					}
					
					for (String s : column.getValues(String.class))
					{
						_customDosingUnits.get(compound).add(Helpers.toDoseUnit(s));
					}
				}
				else if (column.isHeader(Header.ROUTE_OF_ADMINISTRATION))
				{
					if (_customDosingRoutes.containsKey(compound) == false)
					{
						_customDosingRoutes.put(compound, new ArrayList<DosingRoute>());
					}
					
					for (String s : column.getValues(String.class))
					{
						final DosingRoute route = Helpers.toDosingRoute(s);
						
						_customDosingRoutes.get(compound).add(route);

						if (_customDosingCompartments.containsKey(compound) == false)
						{
							_customDosingCompartments.put(compound, new ArrayList<InputCompartment>());
						}
						
						switch (route)
						{
						case DERMAL:
							_customDosingCompartments.get(compound).add(InputCompartment.SKIN);
							break;
						case BOLUS:
							_customDosingCompartments.get(compound).add(InputCompartment.VENOUS_BLOOD);
							break;
						case INHALED:
							_customDosingCompartments.get(compound).add(InputCompartment.LUNG);
							break;
						case ORAL:
							_customDosingCompartments.get(compound).add(InputCompartment.STOMACH);
							break;
						case SUBCUTANEOUS:
							_customDosingCompartments.get(compound).add(InputCompartment.SUBCUTIS);
							break;
						default:
							break;
						
						}
					}
				}
				else if (column.isHeader(Header.DURATION))
				{
					if (_customDosingDurations.containsKey(compound) == false)
					{
						_customDosingDurations.put(compound, new ArrayList<Double>());
					}
					
					_customDosingDurations.get(compound).addAll(column.getValues(Double.class));
				}
				else if (column.isHeader(Header.PLACE_OF_APPLICATION))
				{
					if (_customDosingPlaces.containsKey(compound) == false)
					{
						_customDosingPlaces.put(compound, new ArrayList<PlaceOfApplication>());
					}
					
					for (String s : column.getValues(String.class))
					{
						_customDosingPlaces.get(compound).add(Helpers.toPlaceOfApplication(s));
					}
				}
			}
		}		
	}

	private void extractDataFromOtherSiteProfile(SimcypOutput simcypOutput)
	{
		Compound compound = simcypOutput.getCompound();
		
		if (compound != Compound.NULL)
		{
			_dosedCompounds.add(compound);
			
			SimcypOutputColumn indexColumn = null;
			SimcypOutputColumn timeColumn = null;
			SimcypOutputColumn cumulativeInputColumn = null;
			
			for (SimcypOutputColumn column : simcypOutput.getColumns())
			{
				if (indexColumn == null && column.isHeader(Header.INDEX))
				{
					indexColumn = column;
				}
				else if (timeColumn == null && column.isHeader(Header.TIME))
				{
					timeColumn = column;
				}
				else if (cumulativeInputColumn == null && column.isHeader(Header.CUMULATIVE_INPUT))
				{
					cumulativeInputColumn = column;
				}
			}
			
			if (indexColumn != null && timeColumn != null && cumulativeInputColumn != null)
			{
				for (int i = 0; i < indexColumn.getRowCount(); ++i)
				{
					final double inputPercent = cumulativeInputColumn.getValue(i, Double.class);
					
					if (inputPercent > 0)
					{
						final double time = timeColumn.getValue(i, Double.class);
						final int individualId = indexColumn.getValue(i, Integer.class);

						if (_cumulativeInputTimes.containsKey(compound) == false)
						{
							_cumulativeInputTimes.put(compound, new ArrayList<Double>());
						}
						
						_cumulativeInputTimes.get(compound).add(time);

						if (_cumulativeInputPercents.containsKey(compound) == false)
						{
							_cumulativeInputPercents.put(compound, new HashMap<Integer, ArrayList<Double>>());
						}
						
						if (_cumulativeInputPercents.get(compound).containsKey(individualId) == false)
						{
							_cumulativeInputPercents.get(compound).put(individualId, new ArrayList<Double>());
						}
						
						_cumulativeInputPercents.get(compound).get(individualId).add(inputPercent);
					}
				}
			}
		}	
	}

	private void extractDataFromInputs(SimcypOutput simcypOutput)
	{
		Compound compound = simcypOutput.getCompound();
		
		if (compound != Compound.NULL)
		{
			_dosedCompounds.add(compound);
			
			for (SimcypOutputColumn column : simcypOutput.getColumns())
			{
				if (column.isHeader(Header.ROUTE))
				{
					DosingRoute route = Helpers.toDosingRoute(column.getValue(String.class));
					
					_routes.put(compound, route);
					
					switch (route)
					{
					case DERMAL:
						_inputCompartments.put(compound, InputCompartment.SKIN);
						break;
					case BOLUS:
					case INFUSION:
						_inputCompartments.put(compound, InputCompartment.VENOUS_BLOOD);
						break;
					case INHALED:
						_inputCompartments.put(compound, InputCompartment.LUNG);
						break;
					case ORAL:
						_inputCompartments.put(compound, InputCompartment.STOMACH);
						break;
					case SUBCUTANEOUS:
						_inputCompartments.put(compound, InputCompartment.SUBCUTIS);
						break;
					default:
						break;
					
					}
				}
				else if (column.isHeader(Header.DOSE_UNITS))
				{
					_units.put(compound, Helpers.toDoseUnit(column.getValue(String.class)));
				}
				else if (column.isHeader(Header.DOSE))
				{
					_doses.put(compound, column.getValue(Double.class));
				}
				else if (column.isHeader(Header.BOLUS_DOSE))
				{
					_bolusDoses.put(compound, column.getValue(Double.class));
				}
				else if (column.isHeader(Header.BOLUS_DOSING_REGIMEN))
				{
					_bolusDosingRegimens.put(compound, Helpers.toDosingRegimen(column.getValue(String.class)));
				}
				else if (column.isHeader(Header.INFUSION_DOSE))
				{
					_doses.put(compound, column.getValue(Double.class));
				}
				else if (column.isHeader(Header.INFUSION_DURATION))
				{
					_durations.put(compound, column.getValue(Double.class));
				}
				else if (column.isHeader(Header.THICKNESS_OF_APPLIED_FORMULATION_LAYER))
				{
					_thicknesses.put(compound, column.getValue(Double.class));
				}
				else if (column.isHeader(Header.AREA_OF_APPLICATION))
				{
					_areas.put(compound, column.getValue(Double.class));
				}
				else if (column.isHeader(Header.VOLUME_APPLIED_SOLUTION))
				{
					_volumes.put(compound, column.getValue(Double.class));
				}
				else if (column.isHeader(Header.PLACE_OF_APPLICATION))
				{
					_places.put(compound, Helpers.toPlaceOfApplication(column.getValue(String.class)));
				}
				else if (column.isHeader(Header.DOSING_REGIMEN))
				{
					_dosingRegimens.put(compound, Helpers.toDosingRegimen(column.getValue(String.class)));
				}
				else if (column.isHeader(Header.INPUT_COMPARTMENT))
				{
					_inputCompartments.put(compound, Helpers.toInputCompartment(column.getValue(String.class)));
				}
				else if (column.isHeader(Header.INPUT_MODEL))
				{
					_inputModels.put(compound, Helpers.toInputModel(column.getValue(String.class)));
				}
				else if (column.isHeader(Header.INPUT_DURATION))
				{
					_durations.put(compound, column.getValue(Double.class));
				}
				else if (column.isHeader(Header.INTERPOLATION_METHOD))
				{
					_interpolationMethods.put(compound, Helpers.toInterpolationMethod(column.getValue(String.class)));
				}
				else if (column.isHeader(Header.LAG_TIME))
				{
					_lagTimes.put(compound, column.getValue(Double.class));
				}
				else if (column.isHeader(Header.FA))
				{
					_fas.put(compound, column.getValue(Double.class));
				}
				else if (column.isHeader(Header.KA))
				{
					_kas.put(compound, column.getValue(Double.class));
				}
				else if (column.isHeader(Header.DOSE_INTERVAL))
				{
					_intervals.put(compound, column.getValue(Double.class));
				}
			}
		}	
	}
	
	public HashSet<Integer> getIndividualIds()
	{
		return _ids;
	}

	public HashSet<Compound> getDosedCompounds()
	{
		return _dosedCompounds;
	}
	
	public double getTime(Compound compound)
	{
		return _lagTimes.containsKey(compound) ? _lagTimes.get(compound) : 0.0;
	}
	
	public DosingRoute getRoute(Compound compound)
	{
		return _routes.containsKey(compound) ? _routes.get(compound) : DosingRoute.NULL;
	}
	
	public double getDose(int individualId, Compound compound)
	{
		double dose = _doses.containsKey(compound) ? _doses.get(compound) : 0.0;
		DoseUnit unit = getUnit(compound);
		
		if (unit == DoseUnit.MILLIGRAMS_PER_SQUARE_METRES)
		{
			dose *= _bodySurfaceAreas.containsKey(individualId) ? _bodySurfaceAreas.get(individualId) : 0.0;
		}
		else if (unit == DoseUnit.MILLIGRAMS_PER_KILOGRAM)
		{
			dose *= _weights.containsKey(individualId) ? _weights.get(individualId) : 0.0;
		}
		
		if (getRoute(compound) == DosingRoute.OTHER_SITE)
		{
			if (getOtherSiteInputModel(compound) == InputModel.FIRST_ORDER)
			{
				dose *= _fas.containsKey(individualId) ? _fas.get(individualId) : 1.0;
			}
		}
		
		return dose;
	}
	
	public double getBolusDose(int individualId, Compound compound)
	{
		double dose = _bolusDoses.containsKey(compound) ? _bolusDoses.get(compound) : 0.0;
		DoseUnit unit = getUnit(compound);
		
		if (unit == DoseUnit.MILLIGRAMS_PER_SQUARE_METRES)
		{
			dose *= _bodySurfaceAreas.containsKey(individualId) ? _bodySurfaceAreas.get(individualId) : 0.0;
		}
		else if (unit == DoseUnit.MILLIGRAMS_PER_KILOGRAM)
		{
			dose *= _weights.containsKey(individualId) ? _weights.get(individualId) : 0.0;
		}
		
		return dose;
	}
	
	public DoseUnit getUnit(Compound compound)
	{
		return _units.containsKey(compound) ? _units.get(compound) : DoseUnit.NULL;
	}
	
	public InputCompartment getCompartment(Compound compound)
	{
		return _inputCompartments.containsKey(compound) ? _inputCompartments.get(compound) : InputCompartment.NULL;
	}
	
	public double getDuration(Compound compound)
	{
		return _durations.containsKey(compound) ? _durations.get(compound) : 0.0;
	}
	
	public double getInterval(Compound compound)
	{
		return _intervals.containsKey(compound) ? _intervals.get(compound) : 0.0;
	}
	
	public double getBolusInterval(Compound compound)
	{
		if (_bolusDosingRegimens.containsKey(compound) && _bolusDosingRegimens.get(compound) == DosingRegimen.MULTIPLE)
		{
			return getInterval(compound);
		}
		
		return 0.0;
	}
	
	public InputModel getOtherSiteInputModel(Compound compound)
	{
		return _inputModels.containsKey(compound) ? _inputModels.get(compound) : InputModel.NULL;
	}
	
	public double getAbsorptionRateConstant(Compound compound)
	{
		return _kas.containsKey(compound) ? _kas.get(compound) : 1.0;
	}
	
	public double getDermalThickness(Compound compound)
	{
		return _thicknesses.containsKey(compound) ? _thicknesses.get(compound) : 0.0;
	}
	
	public double getDermalArea(Compound compound)
	{
		return _areas.containsKey(compound) ? _areas.get(compound) : 0.0;
	}
	
	public double getDermalVolume(Compound compound)
	{
		return _volumes.containsKey(compound) ? _volumes.get(compound) : 0.0;
	}
	
	public PlaceOfApplication getDermalPlace(Compound compound)
	{
		return _places.containsKey(compound) ? _places.get(compound) : PlaceOfApplication.NULL;
	}
	
	public int getCumulativeInputsCount(Compound compound)
	{
		return _cumulativeInputTimes.containsKey(compound) ? _cumulativeInputTimes.get(compound).size() : 0;
	}

	public double getCumulativeInputTime(int index, Compound compound)
	{
		return getValue(_cumulativeInputTimes, index, compound, 0.0);
	}

	public double getCumulativeInputDose(int index, int individualId, Compound compound)
	{
		HashMap<Integer, ArrayList<Double>> compoundPercents = _cumulativeInputPercents.containsKey(compound) ? _cumulativeInputPercents.get(compound) : new HashMap<Integer, ArrayList<Double>>();
		ArrayList<Double> individualPercents = compoundPercents.containsKey(individualId) ? compoundPercents.get(individualId) : new ArrayList<Double>();
		
		if (individualPercents.isEmpty() || index < 0 || index >= individualPercents.size())
		{
			return 0;
		}
		
		if (index > 0)
		{
			return (individualPercents.get(index) - individualPercents.get(index - 1)) * getDose(individualId, compound);
		}
		
		return individualPercents.get(index) * getDose(individualId, compound);
	}

	public int getCustomDoseCount(Compound compound)
	{
		return _customDosingDoses.size();
	}

	public double getCustomDoseTime(int index, Compound compound)
	{
		return getValue(_customDosingOffsets, index, compound, 0.0);
	}

	public DosingRoute getCustomDoseRoute(int index, Compound compound)
	{
		return getValue(_customDosingRoutes, index, compound, DosingRoute.NULL);
	}

	public double getCustomDoseDose(int index, int individualId, Compound compound)
	{
		final DoseUnit unit = getCustomDoseUnit(index, compound);
		double dose = getValue(_customDosingDoses, index, compound, 0.0);
		
		if (unit == DoseUnit.MILLIGRAMS_PER_SQUARE_METRES)
		{
			dose *= _bodySurfaceAreas.containsKey(individualId) ? _bodySurfaceAreas.get(individualId) : 0.0;
		}
		else if (unit == DoseUnit.MILLIGRAMS_PER_KILOGRAM)
		{
			dose *= _weights.containsKey(individualId) ? _weights.get(individualId) : 0.0;
		}
		
		return dose;
	}

	public DoseUnit getCustomDoseUnit(int index, Compound compound)
	{
		return getValue(_customDosingUnits, index, compound, DoseUnit.NULL);
	}

	public InputCompartment getCustomDoseCompartment(int index, Compound compound)
	{
		return getValue(_customDosingCompartments, index, compound, InputCompartment.NULL);
	}

	public double getCustomDoseDuration(int index, Compound compound)
	{
		return getValue(_customDosingDurations, index, compound, 0.0);
	}

	public double getCustomDoseDermalThickness(int index, Compound compound)
	{
		return getValue(_customDosingThicknesses, index, compound, 0.0);
	}

	public double getCustomDoseDermalArea(int index, Compound compound)
	{
		return getValue(_customDosingAreas, index, compound, 0.0);
	}

	public double getCustomDoseDermalVolume(int index, Compound compound)
	{
		return getValue(_customDosingVolumes, index, compound, 0.0);
	}

	public PlaceOfApplication getCustomDoseDermalPlace(int index, Compound compound)
	{
		return getValue(_customDosingPlaces, index, compound, PlaceOfApplication.NULL);
	}
	
	private <T> T getValue(HashMap<Compound, ArrayList<T>> source, int index, Compound compound, T defaultIfNotExists)
	{
		ArrayList<T> values = source.containsKey(compound) ? source.get(compound) : new ArrayList<T>();
		
		if (values.isEmpty() || index < 0 || index >= values.size())
		{
			return defaultIfNotExists;
		}
		
		return values.get(index);
	}
}
