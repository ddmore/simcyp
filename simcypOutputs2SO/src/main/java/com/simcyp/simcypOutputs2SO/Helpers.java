package com.simcyp.simcypOutputs2SO;

import java.util.Arrays;

import com.simcyp.simcypOutputs2SO.Constants.Compound;
import com.simcyp.simcypOutputs2SO.Constants.DoseUnit;
import com.simcyp.simcypOutputs2SO.Constants.DosingRegimen;
import com.simcyp.simcypOutputs2SO.Constants.DosingRoute;
import com.simcyp.simcypOutputs2SO.Constants.InputCompartment;
import com.simcyp.simcypOutputs2SO.Constants.InputModel;
import com.simcyp.simcypOutputs2SO.Constants.InterpolationMethod;
import com.simcyp.simcypOutputs2SO.Constants.PlaceOfApplication;

public class Helpers
{
	public static Compound extractCompound(String text)
	{
		text = text.toUpperCase();
		
		for (String name : Arrays.asList("SUBSTRATE", "SUB"))
			if (text.contains(name))
				return Compound.SUBSTRATE;
		for (String name : Arrays.asList("INHIBITOR1", "INH1"))
			if (text.contains(name))
				return Compound.INHIBITOR_1;
		for (String name : Arrays.asList("INHIBITOR2", "INH2"))
			if (text.contains(name))
				return Compound.INHIBITOR_2;
		for (String name : Arrays.asList("INHIBITOR3", "INH3"))
			if (text.contains(name))
				return Compound.INHIBITOR_3;
		for (String name : Arrays.asList("SUBPRIMETABOLITE1", "SUBPRIMET1", "SUBPM1"))
			if (text.contains(name))
				return Compound.SUBSTRATE_PRIMARY_METABOLITE_1;
		for (String name : Arrays.asList("INH1METABOLITE", "INH1MET", "INH1M"))
			if (text.contains(name))
				return Compound.INHIBITOR_1_METABOLITE;
		for (String name : Arrays.asList("SUBSECMETABOLITE", "SUBSECMET", "SUBSM"))
			if (text.contains(name))
				return Compound.SUBSTRATE_SECONDARY_METABOLITE;
		for (String name : Arrays.asList("SUBPRIMETABOLITE2", "SUBPRIMET2", "SUBPM2"))
			if (text.contains(name))
				return Compound.SUBSTRATE_PRIMARY_METABOLITE_2;
		
		return Compound.NULL;
	}
	
	public static String toOutputString(Object obj)
	{
		if (obj.getClass().equals(DoseUnit.class))
		{
			switch ((DoseUnit)obj)
			{
			case MILLIGRAMS:
				return "mg";
			case MILLIGRAMS_PER_KILOGRAM:
				return "mg/kg";
			case MILLIGRAMS_PER_SQUARE_METRES:
				return "mg/m2";
			default:
				break;
			}
		}
		
		String text = "";
				
		for (String s : obj.toString().toLowerCase().split("_", -1))
		{
			if (s.isEmpty() == false)
			{
				if (text.length() > 0)
				{
					text += " ";
				}
				
				text += s.substring(0, 1).toUpperCase();
				
				if (s.length() > 1)
				{
					text += s.substring(1);
				}
			}
		}
		
		return text;
	}
	
	public static String toId(String text)
	{
		for (char invalidChar : Arrays.asList(' ', '.', ',', '\'', ':', ';', '[', ']', '(', ')', '+', '/', '\\', '%'))
		{
			text = text.replace(invalidChar, '_');
		}		
		
		return text;	
	}
	
	public static Compound toCompound(String text)
	{
		if (text.equalsIgnoreCase("SUBSTRATE"))
			return Compound.SUBSTRATE;
		if (text.equalsIgnoreCase("INHIBITOR 1"))
			return Compound.INHIBITOR_1;
		if (text.equalsIgnoreCase("INHIBITOR 2"))
			return Compound.INHIBITOR_2;
		if (text.equalsIgnoreCase("INHIBITOR 3"))
			return Compound.INHIBITOR_3;
		if (text.equalsIgnoreCase("SUBSTRATE PRIMARY METABOLITE 1"))
			return Compound.SUBSTRATE_PRIMARY_METABOLITE_1;
		if (text.equalsIgnoreCase("INHIBITOR 1 METABOLITE"))
			return Compound.INHIBITOR_1_METABOLITE;
		if (text.equalsIgnoreCase("SUBSTRATE SECONDARY METABOLITE"))
			return Compound.SUBSTRATE_SECONDARY_METABOLITE;
		if (text.equalsIgnoreCase("SUBSTRATE PRIMARY METABOLITE 2"))
			return Compound.SUBSTRATE_PRIMARY_METABOLITE_2;
		
		return Compound.NULL;
	}
	
	public static DosingRoute toDosingRoute(String text)
	{
		if (text.equalsIgnoreCase("ORAL"))
			return DosingRoute.ORAL;
		if (text.equalsIgnoreCase("INHALED"))
			return DosingRoute.INHALED;
		if (text.equalsIgnoreCase("IV"))
			return DosingRoute.BOLUS;
		if (text.equalsIgnoreCase("INFUSION"))
			return DosingRoute.INFUSION;
		if (text.equalsIgnoreCase("DERMAL"))
			return DosingRoute.DERMAL;
		if (text.equalsIgnoreCase("SUBCUTANEOUS"))
			return DosingRoute.SUBCUTANEOUS;
		if (text.equalsIgnoreCase("OTHER SITE"))
			return DosingRoute.OTHER_SITE;
		if (text.equalsIgnoreCase("CUSTOM DOSING"))
			return DosingRoute.CUSTOM;
		
		return DosingRoute.NULL;
	}
	
	public static DosingRegimen toDosingRegimen(String text)
	{
		if (text.equalsIgnoreCase("SINGLE DOSE"))
			return DosingRegimen.SINGLE;
		if (text.equalsIgnoreCase("MULTIPLE DOSE"))
			return DosingRegimen.MULTIPLE;
		
		return DosingRegimen.NULL;
	}
	
	public static DoseUnit toDoseUnit(String text)
	{
		if (text.equalsIgnoreCase("DOSE (MG/M2)"))
			return DoseUnit.MILLIGRAMS_PER_SQUARE_METRES;
		if (text.equalsIgnoreCase("DOSE (MG)"))
			return DoseUnit.MILLIGRAMS;
		if (text.equalsIgnoreCase("DOSE (MG/KG)"))
			return DoseUnit.MILLIGRAMS_PER_KILOGRAM;
		
		return DoseUnit.NULL;
	}
	
	public static PlaceOfApplication toPlaceOfApplication(String text)
	{
		if (text.equalsIgnoreCase("FOREARM"))
			return PlaceOfApplication.FOREARM;
		if (text.equalsIgnoreCase("UPPER ARM"))
			return PlaceOfApplication.UPPER_ARM;
		if (text.equalsIgnoreCase("LOWER LEG"))
			return PlaceOfApplication.LOWER_LEG;
		if (text.equalsIgnoreCase("THIGH"))
			return PlaceOfApplication.THIGH;
		if (text.equalsIgnoreCase("FACE"))
			return PlaceOfApplication.FACE;
		
		return PlaceOfApplication.NULL;
	}
	
	public static InputCompartment toInputCompartment(String text)
	{
		if (text.equalsIgnoreCase("BLOOD"))
			return InputCompartment.BLOOD;
		if (text.equalsIgnoreCase("VENOUS BLOOD"))
			return InputCompartment.VENOUS_BLOOD;
		if (text.equalsIgnoreCase("CSF"))
			return InputCompartment.CSF;
		if (text.equalsIgnoreCase("SPINAL CSF"))
			return InputCompartment.SPINAL_CSF;
		if (text.equalsIgnoreCase("STOMACH"))
			return InputCompartment.STOMACH;
		if (text.equalsIgnoreCase("COLON"))
			return InputCompartment.COLON;
		if (text.equalsIgnoreCase("ADDITIONAL ORGAN"))
			return InputCompartment.ADDITIONAL_ORGAN;
		if (text.equalsIgnoreCase("SUBCUTANEOUS"))
			return InputCompartment.SUBCUTIS;
		
		return InputCompartment.NULL;
	}
	
	public static InputModel toInputModel(String text)
	{
		if (text.equalsIgnoreCase("BOLUS"))
			return InputModel.BOLUS;
		if (text.equalsIgnoreCase("ZERO ORDER"))
			return InputModel.ZERO_ORDER;
		if (text.equalsIgnoreCase("FIRST ORDER"))
			return InputModel.FIRST_ORDER;
		if (text.equalsIgnoreCase("INPUT PROFILE"))
			return InputModel.INPUT_PROFILE;
		
		return InputModel.NULL;
	}
	
	public static InterpolationMethod toInterpolationMethod(String text)
	{
		if (text.equalsIgnoreCase("LNEAR"))
			return InterpolationMethod.LINEAR;
		if (text.equalsIgnoreCase("POLYNOMIAL"))
			return InterpolationMethod.PIECEWISE_CUBIC_POLYNOMIAL;
		
		return InterpolationMethod.NULL;
	}
}
