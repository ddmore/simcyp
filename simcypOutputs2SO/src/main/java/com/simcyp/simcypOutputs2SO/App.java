package com.simcyp.simcypOutputs2SO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.transform.TransformerException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.jdom2.JDOMException;

import com.simcyp.simcypOutputs2SO.Constants.Compound;
import com.simcyp.simcypOutputs2SO.Constants.DosingRoute;
import com.simcyp.simcypOutputs2SO.Constants.DosingTableColumnId;
import com.simcyp.simcypOutputs2SO.Constants.InputModel;

import de.unirostock.sems.cbarchive.CombineArchive;
import de.unirostock.sems.cbarchive.CombineArchiveException;
import eu.ddmore.libpharmml.IValidationError;
import eu.ddmore.libpharmml.IValidationReport;
import eu.ddmore.libpharmml.dom.commontypes.IntValue;
import eu.ddmore.libpharmml.dom.commontypes.MissingDataAttribute;
import eu.ddmore.libpharmml.dom.commontypes.RealValue;
import eu.ddmore.libpharmml.dom.commontypes.Scalar;
import eu.ddmore.libpharmml.dom.commontypes.StringValue;
import eu.ddmore.libpharmml.dom.commontypes.SymbolType;
import eu.ddmore.libpharmml.dom.dataset.ColumnType;
import eu.ddmore.libpharmml.dom.dataset.DataSetTable;
import eu.ddmore.libpharmml.dom.dataset.DatasetRow;
import eu.ddmore.libpharmml.dom.dataset.ExternalFile;
import eu.ddmore.libpharmml.dom.dataset.HeaderColumnsDefinition;
import eu.ddmore.libpharmml.so.SOFactory;
import eu.ddmore.libpharmml.so.StandardisedOutputResource;
import eu.ddmore.libpharmml.so.dom.RawResults;
import eu.ddmore.libpharmml.so.dom.SOBlock;
import eu.ddmore.libpharmml.so.dom.Simulation;
import eu.ddmore.libpharmml.so.dom.SimulationBlock;
import eu.ddmore.libpharmml.so.dom.SimulationItem;
import eu.ddmore.libpharmml.so.dom.StandardisedOutput;
import eu.ddmore.libpharmml.so.dom.TaskInformation;
import eu.ddmore.libpharmml.so.dom.ToolSettings;
import eu.ddmore.libpharmml.so.impl.LibSO;
import eu.ddmore.libpharmml.so.impl.SOValidator;
import eu.ddmore.libpharmml.so.impl.SOVersion;

public class App 
{
	private static boolean EMBED_DATA_IN_SO = false;
	private static String SO_FILE = "simcyp.SO.xml";
	private static String CA_FILE = "simcyp.CA.omex";
	
    public static void main(String[] args) throws IOException
    {
    	System.out.println("");
    	System.out.println("####################");
    	System.out.println("# simcypOutputs2SO #");
    	System.out.println("####################");
    	
    	/***** Parse command line arguments *****/
    	if (args.length > 0)
    	{
    		File configFile = new File(args[0]);
    		
    		if (configFile.exists())
    		{
    			String configFileName = FilenameUtils.getBaseName(configFile.getName());
    			
    			SO_FILE = String.format("%s.SO.xml", configFileName);
    			CA_FILE = String.format("%s.CA.omex", configFileName);
    			
    			for (Object o : FileUtils.readLines(configFile))
    			{
    				String line = (String)o;
    				String[] lineParts = line.split("=");
    				
    				if (lineParts.length == 2)
    				{
    					if (lineParts[0].equals("#embed_data"))
    					{
    						EMBED_DATA_IN_SO = Boolean.parseBoolean(lineParts[1]);
    					}
    				}
    			}
    		}
    	}
    	
    	System.out.println("SO file name: " + SO_FILE);
    	System.out.println("COMBINE archive file name: " + CA_FILE);
    	System.out.println("Embed data in SO: " + EMBED_DATA_IN_SO);
    	
    	/***** Move the Simcyp outputs into their relevant folders *****/
    	try
    	{
	    	for (Object o : FileUtils.listFiles(new File("."), new String[] { "csv", "lua", "txt", "tmp" }, true))
	    	{
	    		File file = (File)o;
	    		String name = file.getName();
				File destinationFolder = new File(FilenameUtils.getExtension(name));
				
				destinationFolder.mkdir();
				
				file.renameTo(new File(destinationFolder.getAbsolutePath(), name));
	    	}
	    }
    	catch (SecurityException e)
    	{
    		System.out.println("simcypOutputs2SO failure - see error log for further details");
    		System.err.println("Failed move Simcyp outputs into their relevant directories - " + e.getMessage());
    		return;
    	}
    	
    	/***** Parse the Simcyp outputs *****/
    	ArrayList<SimcypOutput> simcypOutputs = new ArrayList<SimcypOutput>();
    	boolean anyValidOutputs = false;
    	
    	for (File file : getFiles("csv"))
    	{
    		simcypOutputs.add(new SimcypOutput(file));
    	}
    	
    	for (SimcypOutput output : simcypOutputs)
    	{
    		anyValidOutputs |= output.isValid();
    	}
    	
    	if (anyValidOutputs == false)
    	{
    		System.out.println("simcypOutputs2SO failure - see error log for further details");
    		System.err.println("No Simcyp outputs detected - the Standard Output will not be generated");
    		return;
    	}
    	
    	/***** Create and populate a new SO *****/
    	LibSO libSO = SOFactory.getInstance().createLibSO();
    	StandardisedOutputResource soResource = libSO.createDom(SOVersion.DEFAULT);
    	StandardisedOutput so = soResource.getDom();
    	SOBlock soBlock = so.createSoBlock();
    	
    	System.out.println("Standard Output version: " + so.getWrittenVersion());
    	System.out.print("Populating the Standard Output...");
    	
    	try
    	{
	    	soBlock.setBlkId(FilenameUtils.removeExtension(SO_FILE));
	    	
	    	addToolSettings(soBlock, simcypOutputs);
	    	addRawResults(soBlock);
	    	addTaskInformation(soBlock);
	    	addSimulation(soBlock, simcypOutputs);
	    	
	    	System.out.println(String.format("DONE (%s)", EMBED_DATA_IN_SO ? "Simcyp outputs embedded" : "Simcyp outputs referenced"));
    	}
    	catch (Exception e)
    	{
    		System.out.println("FAILED");
    		System.out.println("simcypOutputs2SO failure - see error log for further details");
    		System.err.println(String.format("Failed to populate the Standard Output: %s", e.getMessage()));
    		return;
    	}
    	
    	/***** Validate the SO *****/
    	System.out.print("Validating the Standard Output... ");
    	
    	SOValidator soValidator = libSO.getValidator();
    	IValidationReport validationReport = soValidator.createValidationReport(soResource);
    	
    	if (validationReport.isValid())
    	{
    		System.out.println("DONE");
    	}
    	else
    	{
    		HashMap<String, Integer> errorCounts = new HashMap<String, Integer>();
	    	Iterator<IValidationError> it = validationReport.errorIterator();
	    	
	    	System.out.println(String.format("WARNING (%d validation errors)", validationReport.numErrors()));
	    	
	    	while (it.hasNext())
	    	{
	    		final String error = it.next().getErrorMsg();
	    		int errorCount = 1;
	    		
	    		if (errorCounts.containsKey(error))
	    		{
	    			errorCount += errorCounts.get(error);
	    		}
	    		
	    		errorCounts.put(error, errorCount);
	    	}
	    	
	    	for (Entry<String, Integer> error : errorCounts.entrySet())
	    	{
	    		System.err.println(String.format("Standard Output validation failure: %s (%d occurrences)", error.getKey(), error.getValue()));
	    	}
    	}
    	
    	/***** Save the SO *****/
		System.out.print("Saving the Standard Output... ");
		
    	try
    	{    		
    		FileOutputStream fos = new FileOutputStream(SO_FILE);
	    	libSO.save(fos, soResource);
	    	fos.close();
	    	
	    	System.out.println("DONE");
    	}
    	catch (Exception e)
    	{
    		System.out.println("FAILED");
    		System.out.println("simcypOutputs2SO failure - see error log for further details");
    		System.err.println("Failed to write Standard Output file to disk: " + e.getMessage());
    		return;
    	}
    	
    	/***** Create the Combine Archive *****/
    	System.out.print("Creating the COMBINE Archive... ");
    	
    	try
    	{
    		File archiveFile = new File(CA_FILE);
    		
    		if (archiveFile.exists())
    		{
    			archiveFile.delete();
    		}
    		
			CombineArchive archive = new CombineArchive(archiveFile);
			
			// root\
			for (File file : new File(".").listFiles())
			{
				if (file.getName().equals(SO_FILE))
				{
					archive.addMainEntry(
						archive.addEntry(
							file, 
							file.getName(), 
							new URI("http://purl.org/NET/mediatypes/application/xml")
						)
					);
				}
				else
				{
					archive.addEntry(
						file, 
						file.getName(), 
						new URI("http://purl.org/NET/mediatypes/text/plain")
					);
				}
			}
			
			// root\csv\			
			for (File file : getFiles("csv"))
			{
				archive.addEntry(
					file, 
					FilenameUtils.concat("csv", file.getName()), 
					new URI("http://purl.org/NET/mediatypes/text/csv")
				);
			}
			
			// root\lua\			
			for (File file : getFiles("lua"))
			{
				archive.addEntry(
					file, 
					FilenameUtils.concat("lua", file.getName()), 
					new URI("http://purl.org/NET/mediatypes/application/x-lua")
				);
			}
			
			// root\txt\			
			for (File file : getFiles("txt"))
			{
				archive.addEntry(
					file, 
					FilenameUtils.concat("txt", file.getName()), 
					new URI("http://purl.org/NET/mediatypes/text/plain")
				);
			}
			
			// root\tmp\			
			for (File file : getFiles("tmp"))
			{
				archive.addEntry(
					file, 
					FilenameUtils.concat("tmp", file.getName()), 
					new URI("http://purl.org/NET/mediatypes/text/plain")
				);
			}
			
			archive.pack();
			archive.close();
			
			System.out.println("DONE");
		}
    	catch (JDOMException | ParseException | CombineArchiveException | URISyntaxException | TransformerException e)
    	{
    		System.out.println("FAILED");
    		System.out.println("simcypOutputs2SO failure - see error log for further details");
			System.err.println("Failed to create the Combine Archive - " + e.getMessage());
			return;
		}
    }
    
    private static List<File> getFiles(String folderName)
    {
    	List<File> files;
    	File folder = new File(folderName);
    	
    	if (folder.exists())
    	{
    		files = Arrays.asList(folder.listFiles());
    	}
    	else
    	{
    		files = Collections.<File>emptyList();
    	}
    	
    	return files;
    }
	
	private static void addToolSettings(SOBlock soBlock, ArrayList<SimcypOutput> simcypOutputs)
	{
		boolean hasToolSettings = false;
	
		for (SimcypOutput simcypOutput : simcypOutputs)
		{
			hasToolSettings |= simcypOutput.isInputsFile();
		}
		
		if (hasToolSettings)
		{
			ToolSettings toolSettings = soBlock.createToolSettings();
			
			for (SimcypOutput simcypOutput : simcypOutputs)
			{
				if (simcypOutput.isInputsFile())
				{
					final String path = FilenameUtils.concat("csv", simcypOutput.getName(true));
					
					toolSettings.createExternalFile(path, null, null, simcypOutput.getId());
				}
			}
		}
	}

	private static void addRawResults(SOBlock soBlock)
	{
		RawResults rawResults = soBlock.createRawResults();
		
		for (File file : getFiles("csv"))
		{
			final String path = FilenameUtils.concat("csv", file.getName());
			final String id = Helpers.toId(FilenameUtils.removeExtension(file.getName()));
			
			rawResults.createDataFile()
				.createExternalFile(path, null, null, id)
				.createMissingData("n/a", MissingDataAttribute.NA);
		}
		
		for (File file : getFiles("lua"))
    	{
			rawResults.createDataFile().createExternalFile(
				FilenameUtils.concat("lua", file.getName()), 
				null, 
				null, 
				Helpers.toId(FilenameUtils.removeExtension(file.getName()))
			);
    	}
	}

	private static void addTaskInformation(SOBlock soBlock)
	{
		File messagesFile = new File("tmp", "messages.tmp");
		File durationFile = new File("tmp", "duration.tmp");
		
		TaskInformation taskInformation = soBlock.createTaskInformation();
		
		taskInformation.setOutputFilePath(
			new ExternalFile(
				"Output", 
				FilenameUtils.concat("txt", "stdout.txt"), 
				null, 
				null
			)
		);
		
		// The messages file would be far better as XML to avoid confusion with the separation character being nested within messages and issues with quotes.
		// Since it's too late to change the Simcyp Console, we'll have to put up with it for now...
		if (messagesFile.exists())
		{			
			try
			{
				for (Object line : FileUtils.readLines(messagesFile))
				{
					ArrayList<String> lineParts = new ArrayList<String>(Arrays.asList(((String)line).split(",", -1)));
					
					if (lineParts.size() >= 5)
					{
						StringBuilder sb = new StringBuilder();
						
						for (int i = 3; i < (lineParts.size() - 1); ++i)
						{
							sb.append(lineParts.get(i));
						}
						
						taskInformation.createMessage(
							lineParts.get(1), 
							lineParts.get(2), 
							sb.toString(), 
							Integer.parseInt(lineParts.get(lineParts.size() - 1)), 
							lineParts.get(0)
						);
					}
				}
			}
			catch (Exception e)
			{
				System.err.println(String.format("Failed to parse messages file (%s) - 'Message'(s) will be omitted from the Standard Output", messagesFile.getName()));
			}
		}
		else
		{
			System.err.println("Messages file not detected - 'Message'(s) will be omitted from the Standard Output");
		}
		
		if (durationFile.exists())
		{
			try
			{
				Date start = null;
				Date end = null;
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				
				for (Object line : FileUtils.readLines(durationFile))
				{
					String[] lineParts = ((String)line).split("=");
					
					if (lineParts.length == 2)
					{
						if (lineParts[0].equalsIgnoreCase("START"))
						{
							start = dateFormat.parse(lineParts[1]);
						}
						else if (lineParts[0].equalsIgnoreCase("END"))
						{
							end = dateFormat.parse(lineParts[1]);
						}
					}
				}
				
				if (start != null && end != null && start.before(end))
				{
					final double seconds = (end.getTime() - start.getTime()) / 1000.0;
					
					taskInformation.createRunTime(seconds);
				}
			}
			catch (IOException | ParseException e)
			{
				System.err.println(String.format("Failed to parse duration file (%s) - 'RunTime' will be omitted from the Standard Output", durationFile.getName()));
			}
		}
		else
		{
			System.err.println("Duration file not detected - 'RunTime' will be omitted from the Standard Output");
		}
	}

	private static void addSimulation(SOBlock soBlock, ArrayList<SimcypOutput> simcypOutputs)
	{
		Simulation simulation = soBlock.createSimulation();
		SimulationBlock simulationBlock = simulation.createSimulationBlock();
		
		for (SimcypOutput simcypOutput : simcypOutputs)
		{
			if (simcypOutput.isValid())
			{
				switch (simcypOutput.getType())
				{
				case POPULATION_PARAMETERS:
					addSimulationItem(simulationBlock.createPopulationParameters(), simcypOutput);
					break;
					
				case INDIVIDUAL_PARAMETERS:
					addSimulationItem(simulationBlock.createIndivParameters(), simcypOutput);
					break;
					
				case PROFILES:
					addSimulationItem(simulationBlock.createSimulatedProfiles(), simcypOutput);
					break;
					
				default:
					break;
				}
			}
		}
		
		boolean shouldAddDosingInformation = false;
		
		for (SimcypOutput simcypOutput : simcypOutputs)
		{
			if (simcypOutput.isInputsFile())
			{
				shouldAddDosingInformation = true;
			}
		}
		
		if (shouldAddDosingInformation)
		{
			addDosing(simulationBlock.createDosing(), simcypOutputs);
		}
	}
	
	private static void addSimulationItem(SimulationItem simulationItem, SimcypOutput simcypOutput)
	{
		simulationItem.setName(simcypOutput.getId());
		
		ArrayList<SimcypOutputColumn> simcypOutputColumns = simcypOutput.getColumns();
		HeaderColumnsDefinition definition = simulationItem.createDefinition();
		int columnNumber = 0;
				
		for (SimcypOutputColumn simcypOutputColumn : simcypOutputColumns)
		{
			definition
				.createColumnDefinition(
					simcypOutputColumn.getId(),
					simcypOutputColumn.getColumnType(simcypOutput.getType()),
					simcypOutputColumn.getSymbolType(),
					++columnNumber
				)
				.createDescription(simcypOutputColumn.getDescription());
		}
		
		if (EMBED_DATA_IN_SO)
		{
			DataSetTable table = simulationItem.createTable();
			
			for (int i = 0; i < simcypOutput.getRowCount(); ++i)
			{
				DatasetRow row = table.createRow();
				
				for (SimcypOutputColumn simcypOutputColumn : simcypOutputColumns)
				{
					row.getListOfValue().add(simcypOutputColumn.getValue(i, Scalar.class));
				}
			}
		}
		else
		{
			simulationItem.createExternalFile(
				FilenameUtils.concat("csv", simcypOutput.getName(true)), 
				null, 
				null, 
				simcypOutput.getId()
			);
		}
	}
	
	private static void addDosing(SimulationItem dosing, ArrayList<SimcypOutput> simcypOutputs)
	{		
		// Gather all dosing information from the Simcyp output files and 
		// format / sort it ready for insertion into the Standard Output
		DosingData dosingData = new DosingData(simcypOutputs);
		ArrayList<HashMap<DosingTableColumnId, Scalar>> dosingEvents = new ArrayList<HashMap<DosingTableColumnId, Scalar>>();
		boolean hasOtherSiteDosing = false;
		boolean hasDermalDosing = false;
		
		try
		{
			for (int individualId : dosingData.getIndividualIds())
			{
				for (Compound compound : dosingData.getDosedCompounds())
				{
					DosingRoute route = dosingData.getRoute(compound);
					InputModel inputModel = dosingData.getOtherSiteInputModel(compound);
					
					if (route == DosingRoute.CUSTOM)
					{
						// Multiple dosing events
						for (int i = 0; i < dosingData.getCustomDoseCount(compound); ++i)
						{
							HashMap<DosingTableColumnId, Scalar> dosingEvent = new HashMap<DosingTableColumnId, Scalar>();
							
							dosingEvent.put(DosingTableColumnId.ID, new IntValue(individualId));
							dosingEvent.put(DosingTableColumnId.TIME, new RealValue(dosingData.getCustomDoseTime(i, compound)));
							dosingEvent.put(DosingTableColumnId.COMPOUND, new StringValue(Helpers.toOutputString(compound)));
							dosingEvent.put(DosingTableColumnId.ADM, new StringValue(Helpers.toOutputString(dosingData.getCustomDoseRoute(i, compound))));
							dosingEvent.put(DosingTableColumnId.DOSE, new RealValue(dosingData.getCustomDoseDose(i, individualId, compound)));
							dosingEvent.put(DosingTableColumnId.UNIT, new StringValue(Helpers.toOutputString(dosingData.getCustomDoseUnit(i, compound))));
							dosingEvent.put(DosingTableColumnId.CMT, new StringValue(Helpers.toOutputString(dosingData.getCustomDoseCompartment(i, compound))));
							dosingEvent.put(DosingTableColumnId.DURATION, new RealValue(dosingData.getCustomDoseDuration(i, compound)));
							dosingEvent.put(DosingTableColumnId.II, new RealValue(0));
							
							if (route == DosingRoute.DERMAL)
							{
								hasDermalDosing = true;
								dosingEvent.put(DosingTableColumnId.THICKNESS, new RealValue(dosingData.getCustomDoseDermalThickness(i, compound)));
								dosingEvent.put(DosingTableColumnId.AREA, new RealValue(dosingData.getCustomDoseDermalArea(i, compound)));
								dosingEvent.put(DosingTableColumnId.VOLUME, new RealValue(dosingData.getCustomDoseDermalVolume(i, compound)));
								dosingEvent.put(DosingTableColumnId.PLACE, new StringValue(Helpers.toOutputString(dosingData.getCustomDoseDermalPlace(i, compound))));
							}
							
							dosingEvents.add(dosingEvent);
						}
					}
					else if (route == DosingRoute.INFUSION)
					{
						// 2 dosing events - one for iv bolus and one for iv infusion
						HashMap<DosingTableColumnId, Scalar> bolusDosingEvent = new HashMap<DosingTableColumnId, Scalar>();
						HashMap<DosingTableColumnId, Scalar> infusionDosingEvent = new HashMap<DosingTableColumnId, Scalar>();
						
						bolusDosingEvent.put(DosingTableColumnId.ID, new IntValue(individualId));
						bolusDosingEvent.put(DosingTableColumnId.TIME, new RealValue(dosingData.getTime(compound)));
						bolusDosingEvent.put(DosingTableColumnId.COMPOUND, new StringValue(Helpers.toOutputString(compound)));
						bolusDosingEvent.put(DosingTableColumnId.ADM, new StringValue(Helpers.toOutputString(DosingRoute.BOLUS)));
						bolusDosingEvent.put(DosingTableColumnId.DOSE, new RealValue(dosingData.getBolusDose(individualId, compound)));
						bolusDosingEvent.put(DosingTableColumnId.UNIT, new StringValue(Helpers.toOutputString(dosingData.getUnit(compound))));
						bolusDosingEvent.put(DosingTableColumnId.CMT, new StringValue(Helpers.toOutputString(dosingData.getCompartment(compound))));
						bolusDosingEvent.put(DosingTableColumnId.DURATION, new RealValue(0));
						bolusDosingEvent.put(DosingTableColumnId.II, new RealValue(dosingData.getBolusInterval(compound)));
						
						infusionDosingEvent.put(DosingTableColumnId.ID, new IntValue(individualId));
						infusionDosingEvent.put(DosingTableColumnId.TIME, new RealValue(dosingData.getTime(compound)));
						infusionDosingEvent.put(DosingTableColumnId.COMPOUND, new StringValue(Helpers.toOutputString(compound)));
						infusionDosingEvent.put(DosingTableColumnId.ADM, new StringValue(Helpers.toOutputString(DosingRoute.INFUSION)));
						infusionDosingEvent.put(DosingTableColumnId.DOSE, new RealValue(dosingData.getDose(individualId, compound)));
						infusionDosingEvent.put(DosingTableColumnId.UNIT, new StringValue(Helpers.toOutputString(dosingData.getUnit(compound))));
						infusionDosingEvent.put(DosingTableColumnId.CMT, new StringValue(Helpers.toOutputString(dosingData.getCompartment(compound))));
						infusionDosingEvent.put(DosingTableColumnId.DURATION, new RealValue(dosingData.getDuration(compound)));
						infusionDosingEvent.put(DosingTableColumnId.II, new RealValue(dosingData.getInterval(compound)));
						
						dosingEvents.add(bolusDosingEvent);
						dosingEvents.add(infusionDosingEvent);
					}
					else if (route == DosingRoute.OTHER_SITE && inputModel == InputModel.INPUT_PROFILE)
					{
						// Multiple dosing events - one for each cumulative input
						hasOtherSiteDosing = true;						
						
						for (int i = 0; i < dosingData.getCumulativeInputsCount(compound); ++i)
						{
							HashMap<DosingTableColumnId, Scalar> dosingEvent = new HashMap<DosingTableColumnId, Scalar>();
							
							dosingEvent.put(DosingTableColumnId.ID, new IntValue(individualId));
							dosingEvent.put(DosingTableColumnId.TIME, new RealValue(dosingData.getCumulativeInputTime(i, compound)));
							dosingEvent.put(DosingTableColumnId.COMPOUND, new StringValue(Helpers.toOutputString(compound)));
							dosingEvent.put(DosingTableColumnId.ADM, new StringValue(Helpers.toOutputString(route)));
							dosingEvent.put(DosingTableColumnId.DOSE, new RealValue(dosingData.getCumulativeInputDose(i, individualId, compound)));
							dosingEvent.put(DosingTableColumnId.UNIT, new StringValue(Helpers.toOutputString(dosingData.getUnit(compound))));
							dosingEvent.put(DosingTableColumnId.CMT, new StringValue(Helpers.toOutputString(dosingData.getCompartment(compound))));
							dosingEvent.put(DosingTableColumnId.DURATION, new RealValue(dosingData.getDuration(compound)));
							dosingEvent.put(DosingTableColumnId.II, new RealValue(dosingData.getInterval(compound)));
							dosingEvent.put(DosingTableColumnId.INPUT_MODEL, new StringValue(Helpers.toOutputString(inputModel)));
													
							dosingEvents.add(dosingEvent);
						}
					}
					else
					{
						// Single dosing event
						HashMap<DosingTableColumnId, Scalar> dosingEvent = new HashMap<DosingTableColumnId, Scalar>();
						
						dosingEvent.put(DosingTableColumnId.ID, new IntValue(individualId));
						dosingEvent.put(DosingTableColumnId.TIME, new RealValue(dosingData.getTime(compound)));
						dosingEvent.put(DosingTableColumnId.COMPOUND, new StringValue(Helpers.toOutputString(compound)));
						dosingEvent.put(DosingTableColumnId.ADM, new StringValue(Helpers.toOutputString(route)));
						dosingEvent.put(DosingTableColumnId.DOSE, new RealValue(dosingData.getDose(individualId, compound)));
						dosingEvent.put(DosingTableColumnId.UNIT, new StringValue(Helpers.toOutputString(dosingData.getUnit(compound))));
						dosingEvent.put(DosingTableColumnId.CMT, new StringValue(Helpers.toOutputString(dosingData.getCompartment(compound))));
						dosingEvent.put(DosingTableColumnId.DURATION, new RealValue(dosingData.getDuration(compound)));
						dosingEvent.put(DosingTableColumnId.II, new RealValue(dosingData.getInterval(compound)));
						
						if (route == DosingRoute.OTHER_SITE)
						{
							hasOtherSiteDosing = true;							
							dosingEvent.put(DosingTableColumnId.INPUT_MODEL, new StringValue(Helpers.toOutputString(inputModel)));
						}
						
						if (route == DosingRoute.DERMAL)
						{
							hasDermalDosing = true;
							dosingEvent.put(DosingTableColumnId.THICKNESS, new RealValue(dosingData.getDermalThickness(compound)));
							dosingEvent.put(DosingTableColumnId.AREA, new RealValue(dosingData.getDermalArea(compound)));
							dosingEvent.put(DosingTableColumnId.VOLUME, new RealValue(dosingData.getDermalVolume(compound)));
							dosingEvent.put(DosingTableColumnId.PLACE, new StringValue(Helpers.toOutputString(dosingData.getDermalPlace(compound))));
						}
						
						dosingEvents.add(dosingEvent);
					}
				}
			}
			
			Collections.sort(dosingEvents, new DosingEventComparator());
						
			// Populate the Standard Output
			HeaderColumnsDefinition definition = dosing.createDefinition();
			DataSetTable table = dosing.createTable();
			int columnNumber = 0;
			
			definition
				.createColumnDefinition(DosingTableColumnId.ID.toString(), ColumnType.ID, SymbolType.INT, ++columnNumber)
				.createDescription("Index corresponding to an individual");
			definition
				.createColumnDefinition(DosingTableColumnId.TIME.toString(), ColumnType.TIME, SymbolType.REAL, ++columnNumber)
				.createDescription("Time of dose (time elapsed since simulation start) (unit: h)");
			definition
				.createColumnDefinition(DosingTableColumnId.COMPOUND.toString(), ColumnType.UNDEFINED, SymbolType.STRING, ++columnNumber)
				.createDescription("Compound position");
			definition
				.createColumnDefinition(DosingTableColumnId.ADM.toString(), ColumnType.ADM, SymbolType.STRING, ++columnNumber)
				.createDescription("Route of administration for a dose");
			definition
				.createColumnDefinition(DosingTableColumnId.DOSE.toString(), ColumnType.DOSE, SymbolType.REAL, ++columnNumber)
				.createDescription("Dose amount");
			definition
				.createColumnDefinition(DosingTableColumnId.UNIT.toString(), ColumnType.UNDEFINED, SymbolType.STRING, ++columnNumber)
				.createDescription("Dose unit");
			definition
				.createColumnDefinition(DosingTableColumnId.CMT.toString(), ColumnType.CMT, SymbolType.STRING, ++columnNumber)
				.createDescription("Compartment into which the dose is introduced");
			definition
				.createColumnDefinition(DosingTableColumnId.DURATION.toString(), ColumnType.DURATION, SymbolType.REAL, ++columnNumber)
				.createDescription("Dose duration (relevant only when the route of administration is 'Infusion' or the route of administration is 'Other Site' and the input model is 'Zero Order') (unit: h)");
			definition
				.createColumnDefinition(DosingTableColumnId.II.toString(), ColumnType.II, SymbolType.REAL, ++columnNumber)
				.createDescription("Time between doses (unit: h)");
			
			if (hasOtherSiteDosing)
			{
				definition
					.createColumnDefinition(DosingTableColumnId.INPUT_MODEL.toString(), ColumnType.UNDEFINED, SymbolType.STRING, ++columnNumber)
					.createDescription("Dose input model; Bolus, Zero Order, First Order, or Input Profile (relevant only when the route of administration is 'Other Site')");
			}
			
			if (hasDermalDosing)
			{
				definition
					.createColumnDefinition(DosingTableColumnId.THICKNESS.toString(), ColumnType.UNDEFINED, SymbolType.REAL, ++columnNumber)
					.createDescription("Thickness of applied formulation layer (relevant only when the route of administration is 'Dermal') (unit: cm)");
				definition
					.createColumnDefinition(DosingTableColumnId.AREA.toString(), ColumnType.UNDEFINED, SymbolType.REAL, ++columnNumber)
					.createDescription("Area of application (relevant only when the route of administration is 'Dermal') (unit: cm2)");
				definition
					.createColumnDefinition(DosingTableColumnId.VOLUME.toString(), ColumnType.UNDEFINED, SymbolType.REAL, ++columnNumber)
					.createDescription("Volume of applied solution (relevant only when the route of administration is 'Dermal') (unit: ml)");
				definition
					.createColumnDefinition(DosingTableColumnId.PLACE.toString(), ColumnType.UNDEFINED, SymbolType.STRING, ++columnNumber)
					.createDescription("Place of application (relevant only when the route of administration is 'Dermal')");
			}
			
			for (int i = 0; i < dosingEvents.size(); ++i)
			{
				HashMap<DosingTableColumnId, Scalar> dosingEvent = dosingEvents.get(i);
				List<Scalar> rowValues = table.createRow().getListOfValue();
				
				for (DosingTableColumnId id : DosingTableColumnId.values())
				{
					if (dosingEvent.containsKey(id))
					{
						rowValues.add(dosingEvent.get(id));
					}
				}
			}
		}
		catch (Exception e)
		{
			System.err.println("Failed to extract dosing information from the Simcyp output files - 'Dosing' will be omitted from the Standard Output");
		}
	}
}
