package com.simcyp.simcypOutputs2SO;

import java.util.Comparator;
import java.util.HashMap;

import com.simcyp.simcypOutputs2SO.Constants.DosingTableColumnId;

import eu.ddmore.libpharmml.dom.commontypes.Scalar;

public class DosingEventComparator implements Comparator<HashMap<DosingTableColumnId, Scalar>>
{
	public int compare(HashMap<DosingTableColumnId, Scalar> eventA, HashMap<DosingTableColumnId, Scalar> eventB)
	{
		double individualA = Integer.parseInt(eventA.get(DosingTableColumnId.ID).valueToString());
		double individualB = Integer.parseInt(eventB.get(DosingTableColumnId.ID).valueToString());
		
		if (individualA < individualB)
		{
			return -1;
		}
		else if (individualA > individualB)
		{
			return 1;
		}
		else
		{
			double timeA = Double.parseDouble(eventA.get(DosingTableColumnId.TIME).valueToString());
			double timeB = Double.parseDouble(eventB.get(DosingTableColumnId.TIME).valueToString());
			
			if (timeA < timeB)
			{
				return -1;
			}
			else if (timeA > timeB)
			{
				return 1;
			}
			else
			{
				int compoundA = Helpers.toCompound(eventA.get(DosingTableColumnId.COMPOUND).valueToString()).ordinal();
				int compoundB = Helpers.toCompound(eventB.get(DosingTableColumnId.COMPOUND).valueToString()).ordinal();
				
				if (compoundA < compoundB)
				{
					return -1;
				}
				else if (compoundA > compoundB)
				{
					return 1;
				}
				else
				{
					double durationA = Double.parseDouble(eventA.get(DosingTableColumnId.DURATION).valueToString());
					double durationB = Double.parseDouble(eventB.get(DosingTableColumnId.DURATION).valueToString());
					
					if (durationA < durationB)
					{
						return -1;
					}
					else if (durationA > durationB)
					{
						return 1;
					}
					else
					{
						return 0;
					}
				}
			}
		}
	}
}
