package com.simcyp.simcypOutputs2SO;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.simcyp.simcypOutputs2SO.Constants.Compound;
import com.simcyp.simcypOutputs2SO.Constants.Header;
import com.simcyp.simcypOutputs2SO.Constants.Output;
import com.simcyp.simcypOutputs2SO.Constants.SimcypOutputType;

public class SimcypOutput
{
	private ArrayList<SimcypOutputColumn> _columns = new ArrayList<SimcypOutputColumn>();
	private String _path;
	private SimcypOutputType _type;
	private boolean _isValid;
	
	public SimcypOutput(File file)
	{
		final String SEPARATOR = ",";
		
		try
		{
			_path = file.getCanonicalPath();
		}
		catch (IOException e)
		{
			_path = file.getPath();
		}
		
		System.out.print(String.format("Parsing Simcyp output file: %s... ", _path));
		
		boolean success = true;
		
		try
		{
			ArrayList<String> headers = null;
			ArrayList<String> units = null;
			ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();
			
			for (Object line : FileUtils.readLines(file, "UTF-8"))
			{
				if (headers == null)
				{
					headers = new ArrayList<String>(Arrays.asList(((String)line).split(SEPARATOR, -1)));
					continue;
				}
				
				if (units == null)
				{
					units = new ArrayList<String>(Arrays.asList(((String)line).split(SEPARATOR, -1)));
					continue;
				}
				
				values.add(new ArrayList<String>(Arrays.asList(((String)line).split(SEPARATOR, -1))));
			}
			
			if (values.isEmpty())
			{
				success = false;
				System.err.println(String.format("Incomplete data detected in Simcyp output (%s) - file will be omitted from the Standard Output", _path));
			}
			else
			{
				try
				{
					for (int col = 0; col < headers.size(); ++col)
					{
						String header = headers.get(col);
						String unit = units.get(col);
						ArrayList<String> columnValues = new ArrayList<String>();
						
						for (int row = 0; row < values.size(); ++row)
						{
							columnValues.add(values.get(row).get(col));
						}
						
						// Rename headers where necessary to avoid duplicates
						SimcypOutputColumn newColumn = new SimcypOutputColumn(header, unit, columnValues);
						int headerSuffix = 1;
						
						while (_columns.contains(newColumn))
						{
							newColumn.setHeaderSuffix(headerSuffix++);
						}
						
						_columns.add(newColumn);
					}
				}
				catch (IndexOutOfBoundsException e)
				{
					success = false;
					System.err.println(String.format("Inconsistent number of columns detected in Simcyp output (%s) - file will be omitted from the Standard Output", _path));
				}
			}
		}
		catch (IOException e)
		{
			success = false;
			System.err.println(String.format("Error parsing Simcyp output (%s) - file will be omitted from the Standard Output", _path));
		}
		
		_type = determineType();
		_isValid = validate();
		
		if (_isValid == false)
		{
			success = false;
			System.err.println(String.format("Invalid Simcyp output (%s) - file will be omitted from the Standard Output", _path));
		}
		
		System.out.println(success ? "DONE" : "FAILED");
	}
	
	private SimcypOutputType determineType()
	{
		if (_columns.isEmpty() == false)
		{
			boolean hasIndexColumn = false;
			
			for (SimcypOutputColumn column : _columns)
			{
				if (column.isHeader(Header.TIME))
				{
					return SimcypOutputType.PROFILES;
				}
				
				if (column.isHeader(Header.DVID))
				{
					return SimcypOutputType.INDETERMINATE;
				}
				
				if (column.isHeader(Header.STATE_VARIABLE_ID))
				{
					return SimcypOutputType.POPULATION_PARAMETERS;
				}
				
				if (column.isHeader(Header.DOSE_NUMBER))
				{
					return SimcypOutputType.CUSTOM_DOSING;
				}
				
				if (column.isHeader(Header.INDEX))
				{
					hasIndexColumn = true;
				}
			}
			
			return hasIndexColumn ? SimcypOutputType.INDIVIDUAL_PARAMETERS : SimcypOutputType.POPULATION_PARAMETERS;
		}
		
		return SimcypOutputType.INDETERMINATE;
	}
	
	private boolean validate()
	{
		if (_type == SimcypOutputType.INDETERMINATE)
		{
			System.err.println("Indeterminate Simcyp output type");
			return false;
		}
		
		if (_columns.isEmpty())
		{
			System.err.println("No columns detected");
			return false;
		}
		
		int rowCount = _columns.get(0).getRowCount();
		
		for (SimcypOutputColumn column : _columns)
		{
			if (column.isValid() == false)
			{
				System.err.println(String.format("Invalid Simcyp output column ('%s' in %s) - column will be omitted from the Standard Output", column.getHeader(), _path));
			}
			
			if (column.getRowCount() != rowCount)
			{
				System.err.println("Inconsistent number of rows per column");
				return false;
			}
		}
		
		return true;
	}
	
	public boolean isValid()
	{
		return _isValid;
	}
	
	public boolean isInputsFile()
	{
		return FilenameUtils.removeExtension(FilenameUtils.getName(_path)).toUpperCase().startsWith("INPUT");
	}
	
	public String getId()
	{
		return Helpers.toId(getName(false));
	}
	
	public String getName(boolean includeExtension)
	{
		if (includeExtension)
		{
			return FilenameUtils.getName(_path);
		}
		
		return FilenameUtils.removeExtension(FilenameUtils.getName(_path));
	}
	
	public String getPath()
	{
		return _path;
	}

	public int getColumnCount()
	{
		return _columns.size();
	}
	
	public int getRowCount()
	{
		return _columns.get(0).getRowCount();
	}
	
	public SimcypOutputType getType()
	{
		return _type;
	}
	
	public ArrayList<SimcypOutputColumn> getColumns()
	{
		return _columns;
	}
	
	public boolean isOutput(Output output)
	{
		final String outputString = output.toString().replace("_", "").toUpperCase();
		final String name = getName(false);
		
		return 
			name.equalsIgnoreCase(outputString) || 
			name.toUpperCase().startsWith(outputString) ||
			name.toUpperCase().endsWith(outputString);
	}

	public Compound getCompound()
	{
		return Helpers.extractCompound(getName(false));
	}
}
